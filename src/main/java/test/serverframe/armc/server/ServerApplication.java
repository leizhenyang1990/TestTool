/********************************************
 * 服务端启动主类
 *
 * @author zwq
 * @create 2018-07-26 22:23
 *********************************************/

package test.serverframe.armc.server;


import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.client.RestTemplate;


@MapperScan({"test.serverframe.armc.server.manager.dao.mapper", "com.microcore.dao.mapper"})
@ComponentScan({"test.serverframe.armc.server.manager", "com.microcore"})
@SpringBootApplication
@ServletComponentScan
public class ServerApplication {

    private static final Logger logger = LoggerFactory.getLogger(ServerApplication.class);

    public static void main(String[] args) {
        // 启动管理端服务
        try {
            // 启动管理端服务
            SpringApplication.run(ServerApplication.class);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    @Bean
    @LoadBalanced
        //负载均衡
    RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
