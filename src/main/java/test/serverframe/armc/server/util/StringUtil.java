package test.serverframe.armc.server.util;

import org.apache.commons.lang.StringUtils;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by lp on 2018/8/31.
 */
public class StringUtil {
    /**
     * 字符串转list（以逗号分隔）
     *
     * @param str string类型
     * @return
     */
    public static List<String> stringToList(String str) {
        List<String> strList = null;
        try {
            String[] s = str.split(",");
            if (s.length == 0) throw new Exception("数组为空，请检查！");
            strList = Arrays.asList(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strList;
    }

    /**
     * 检查是否包含% 或 _
     *
     * @param
     * @return
     */
    public static String checkParam(String str) {
        if (StringUtils.isEmpty(str)) {
            return "";
        }
        StringBuffer sbf = new StringBuffer();
        for (char c : str.toCharArray()) {
            if (c == '%' || c == '_' || c == '.') {
                sbf.append("\\" + c);
            } else {
                sbf.append(c);
            }
        }
        return sbf.toString();
    }


    /*public static void doCompress(File file, ZipOutputStream out, String path) throws IOException {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (files != null && files.length > 0) {
                for (File fileOne : files) {
                    String name = file.name();
                    if (!"".equals(path)) {
                        name = path + "/" + name;
                    }
                    doCompress(fileOne, out, name);
                }
            }
        } else {
            doZip(file, out, path);
        }
    }

    private static void doZip(File fileOne, ZipOutputStream out, String name) throws IOException {
        String entryName = "";
        ZipEntry zipEntry = null;
        FileInputStream fis = null;
        int let = 0;
        if (!"".equals(name)) {
            entryName = name + File.separator + fileOne.name();
        } else {
            entryName = fileOne.name();
        }
        zipEntry = new ZipEntry(entryName);
        out.putNextEntry(zipEntry);
        byte[] bytes = new byte[2048];
        fis = new FileInputStream(fileOne);
        while ((let = fis.read(bytes)) > 0) {
            out.write(bytes, 0, let);
            out.finish();
        }
        out.closeEntry();
        fis.close();


    }

    public static void main(String[] args) throws IOException {
        File zip = new File("F:\\Li\\");
        File file = new File("F:\\Li\\项目\\upload.zip");
        ZipOutputStream out = null;
        try {
            out = new ZipOutputStream(new FileOutputStream(file));
            doCompress(zip, out, "");
            //doZip(zip, out, "");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            out.close();
        }
    }*/
}
