package test.serverframe.armc.server.util;


import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import test.serverframe.armc.server.manager.config.FtpConfiguration;

import java.io.*;
import java.net.MalformedURLException;

/**
 * Created by Fanyx on 2018-07-30.
 */
public class FTPUtil {
    private String host;
    private Integer port;
    private String username;
    private String password;

    public FTPClient ftpClient = null;

    /**
     * 初始化ftp服务器
     */
    public void initFtpClient() {
        this.host = FtpConfiguration.getHost();
        this.port = Integer.parseInt(FtpConfiguration.getPort());
        this.username = FtpConfiguration.getUsername();
        this.password = FtpConfiguration.getPassword();
        ftpClient = new FTPClient();
        ftpClient.setControlEncoding("utf-8");
        System.out.println("connecting...ftpServer：" + this.host + ":" + this.port);
        try {
            //连接ftp服务器
            ftpClient.connect(host, port);
            //登陆ftp服务器
            ftpClient.login(username, password);
            //是否成功登陆
            int replyCode = ftpClient.getReplyCode();
            if (!FTPReply.isPositiveCompletion(replyCode)) {
                System.out.println("connect success...ftpServer：" + this.host + ":" + this.port);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 上传文件
     *
     * @param pathName       ftp服务保存地址
     * @param fileName       上传到ftp的文件名
     * @param originFileName 待上传文件的名称(绝对地址)
     * @return
     */
    public Boolean uploadFile(String pathName, String fileName, String originFileName) {
        boolean flag = false;
        try {
            InputStream inputStream = new FileInputStream(new File(originFileName));
            flag = this.uploadFile(pathName, fileName, inputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ;
        return flag;
    }

    /**
     * 上传文件
     *
     * @param pathName    ftp服务保存地址
     * @param fileName    上传到ftp的文件名
     * @param inputStream 输入文件流
     * @return
     */
    @SuppressWarnings("static-access")
    public boolean uploadFile(String pathName, String fileName, InputStream inputStream) {
        boolean flag = false;
        try {
            System.out.println("start upload file ...");
            initFtpClient();
            ftpClient.setFileType(ftpClient.BINARY_FILE_TYPE);
            ftpClient.makeDirectory(pathName);
            ftpClient.changeWorkingDirectory(pathName);
            ftpClient.storeFile(fileName, inputStream);
            inputStream.close();
            ftpClient.logout();
            flag = true;
            System.out.println("file upload success");
        } catch (IOException e) {
            System.out.println("文件上传失败");
            e.printStackTrace();
        } finally {
            if (ftpClient.isConnected()) {
                try {
                    ftpClient.disconnect();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
            if (null != inputStream) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }
        return flag;
    }

    /**
     * 改变目录路径
     *
     * @param directory
     * @return
     */
    public boolean changeWorkingDirectory(String directory) {
        boolean flag = true;
        try {
            flag = ftpClient.changeWorkingDirectory(directory);
            if (flag) {
                System.out.println("进入文件夹" + directory + "成功");
            } else {
                System.out.println("进入文件夹" + directory + "失败！开始创建文件夹");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * 创建多层目录文件，如果有ftp服务器已存在该文件，则不创建，如果无，则创建
     *
     * @param remote
     * @return
     * @throws IOException
     */
    public boolean createDirecroty(String remote) throws IOException {
        boolean success = true;
        String directory = remote + "/";
        //如果远程目录不存在，则递归创建远程服务器目录
        if (!directory.equalsIgnoreCase("/") && !changeWorkingDirectory(new String(directory))) {
            int start = 0;
            int end = 0;
            if (directory.startsWith("/")) {
                start = 1;
            } else {
                start = 0;
            }
            end = directory.indexOf("/", start);
            String path = "";
            String paths = "";
            while (true) {
                String subDirectory = new String(remote.substring(start, end).getBytes("GBK"), "iso-8859-1");
                path = path + "/" + subDirectory;
                if (!existFile(path)) {
                    if (makeDirectory(subDirectory)) {
                        changeWorkingDirectory(subDirectory);
                    } else {
                        System.out.println("创建目录[" + subDirectory + "]失败");
                        changeWorkingDirectory(subDirectory);
                    }
                } else {
                    changeWorkingDirectory(subDirectory);
                }
                paths = paths + "/" + subDirectory;
                start = end + 1;
                end = directory.indexOf("/", start);
                //检查所有目录是否创建完毕
                if (end <= start) {
                    break;
                }
            }
        }

        return success;
    }

    /**
     * 判断ftp服务器文件是否存在
     *
     * @param path
     * @return
     * @throws IOException
     */
    public boolean existFile(String path) throws IOException {
        boolean flag = false;
        FTPFile[] ftpFileArr = ftpClient.listFiles(path);
        if (ftpFileArr.length > 0) {
            flag = true;
        }
        return flag;
    }

    /**
     * 创建目录
     *
     * @return
     */
    public boolean makeDirectory(String dir) {
        boolean flag = false;
        try {
            flag = ftpClient.makeDirectory(dir);
            if (flag) {
                System.out.println("创建文件夹" + dir + "成功！");
            } else {
                System.out.println("创建文件夹" + dir + "失败！");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * 下载文件
     *
     * @param pathName  FTP服务器文件目录
     * @param fileName  文件名称
     * @param localPath 下载后的文件路径
     * @return
     */
    public boolean downLoadFile(String pathName, String fileName, String localPath) {
        boolean flag = false;
        OutputStream os = null;
        initFtpClient();
        try {
            System.out.println("开始下载文件");
            //切换FTP目录
            ftpClient.changeWorkingDirectory(pathName);
            FTPFile[] ftpFiles = ftpClient.listFiles();
            for (FTPFile file : ftpFiles) {
                if (fileName.equalsIgnoreCase(file.getName())) {
                    File localFile = new File(localPath + "/" + file.getName());
                    os = new FileOutputStream(localFile);
                    ftpClient.retrieveFile(file.getName(), os);
                    os.close();
                }
            }
            ftpClient.logout();
            flag = true;
            System.out.println("下载文件成功");
        } catch (IOException e) {
            System.out.println("下载文件失败");
            e.printStackTrace();
        } finally {
            if (ftpClient.isConnected()) {
                try {
                    ftpClient.disconnect();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != os) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return flag;
    }

    /**
     * 删除文件
     *
     * @param pathName FTP服务器保存目录
     * @param fileName 要删除的文件名称
     * @return
     */
    public boolean deleteFile(String pathName, String fileName) {
        boolean flag = false;
        try {
            System.out.println("开始删除文件");
            initFtpClient();
            //切换FTP目录
            ftpClient.changeWorkingDirectory(pathName);
            ftpClient.dele(pathName);
            ftpClient.logout();
            flag = true;
            System.out.println("删除文件成功");
        } catch (IOException e) {
            System.out.println("删除文件失败");
            e.printStackTrace();
        } finally {
            if (ftpClient.isConnected()) {
                try {
                    ftpClient.disconnect();
                } catch (IOException e2) {
                }
            }
        }
        return flag;
    }

    public static void main(String[] args) {
        FTPUtil ftp = new FTPUtil();
        ftp.uploadFile("appfile/data", "TEST.txt", "C:/Users/Administrator/Desktop/TEST.txt");
//		 ftp.downLoadFile("appfile/data", "public.sql", "D:/ay_fanyuanxin/fyx");
//        System.out.println(ftp.deleteFile("ftpFile/data", "sigar.rar"));
        System.out.println("ok");
    }
}
