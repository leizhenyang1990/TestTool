package test.serverframe.armc.server.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class DateUtil {

	static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	static SimpleDateFormat onlyDateformat = new SimpleDateFormat("yyyy-MM-dd");
	static SimpleDateFormat onlyTimeFormat = new SimpleDateFormat("HH:mm:ss");

	/**
     * 转换日期格式为yyyy-MM-dd
     *
     * @param date 日期
     * @return yyyy-MM-dd字符串
     */
    public static String formatDate(Date date) {
        if (date != null) {
            /******yegui修改，将异常捕获try catch语句去掉*********/
            return format.format(date);
        }
        return "";
    }

    /**
     * @throws ParseException
     * @Descripttion 时分秒字符串转日期格式
     * @Author LSY
     * @Date 2018/10/12 17:50
     * @Param [time 时分秒字符串]
     * @Return Date日期
     **/
    public static Date formatStr(String time) throws Exception {
        if (time != null) {
            /******yegui修改，将异常捕获去掉try catch语句，抛出去*********/
        	return format.parse(time);
        }
        return null;
    }

    public static Date onlyDateformat(String date) throws ParseException {
    	if (StringUtils.isNotEmpty(date)){
    		return onlyDateformat.parse(date);
    	}
    	return null;
    }

    public static String onlyDateformat(Date date) {
        if (date != null) {
            return onlyDateformat.format(date);
        }
        return "";
    }

    public static Date onlyTimeFormat(String date) throws ParseException {
    	if (StringUtils.isNotEmpty(date)){
    		return onlyTimeFormat.parse(date);
    	}
    	return null;
    }

    public static String onlyTimeFormat(Date date) {
        if (date != null) {
            return onlyTimeFormat.format(date);
        }
        return "";
    }

    /**
     * 计划日期与执行开始进行对比，获得真正的执行计划时间
     * @param date 总体日期
     * @param onlyTimeStr 子计划时间
     * @param isStart 是否为开始计划时间
     * @return
     * @throws Exception
     */
    public static Date getCompareTime(String date, String onlyTimeStr, boolean isStart) throws Exception {
    	String[] dateStr = date.trim().split(" ");

    	Date subTime = onlyTimeFormat.parse(dateStr[1]);
    	Date onlyTime = onlyTimeFormat.parse(onlyTimeStr.trim());
		String lastDateStr = null;
		Date lastDate = null;
		//判断是否为计划开始时间
    	if (isStart) {
    		//如果计划开始时间大于子计划时间，则后一天的日期加上子计划的时间，否则当前日期加上子计划时间
    		if (subTime.getTime() > onlyTime.getTime()) {
	    		lastDateStr = getPreviousDay(onlyDateformat(dateStr[0]), isStart) + " " + onlyTimeStr.trim();
	    		lastDate = formatStr(lastDateStr);
	    	} else {
	    		lastDate = formatStr(dateStr[0] + " " + onlyTimeStr.trim());
	    	}
		} else {
			//整个计划结束时间大于子计划时间，则当前日期加上子计划时间，否则后一天的日期加上子计划的时间
			if (subTime.getTime() >= onlyTime.getTime()) {
				lastDate = formatStr(dateStr[0] + " " + onlyTimeStr.trim());
	    	} else {
	    		lastDateStr = getPreviousDay(onlyDateformat(dateStr[0]), isStart) + " " + onlyTimeStr.trim();
	    		lastDate = formatStr(lastDateStr);
	    	}
		}
    	return lastDate;
    }

    /*
     * 根据是否为开始日期获取计划日期的前一天或者后一天日期，格式：yyyy-MM-dd
     */
    public static String getPreviousDay(Date date, Boolean isStart) {
    	Calendar c = Calendar.getInstance();
    	c.setTime(date);
    	int day = c.get(Calendar.DATE);
    	//判断是否是计划开始时间，获取当前日期的前一天或者后一天日期
    	if (isStart) {
    		c.set(Calendar.DATE, day + 1);
    	} else {
    		c.set(Calendar.DATE, day - 1);
    	}
    	String dayBefore = onlyDateformat.format(c.getTime());
    	return dayBefore;
    }
	/**
	 * 得到几天前的时间
	 * 
	 * @param d
	 * @param day
	 * @return
	 */
	public static Date getDateBefore(Date d, int day) {
		Calendar no = Calendar.getInstance();
		no.setTime(d);
		no.set(Calendar.DATE, no.get(Calendar.DATE) - day);
		return no.getTime();
	}

	public static List<String> findDaysStr(String begintTime, String endTime) throws ParseException {

		Date dBegin = onlyDateformat(begintTime);
		Date dEnd = onlyDateformat(endTime);
		//存放每一天日期String对象的daysStrList
		List<String> daysStrList = new ArrayList<String>();
                //放入开始的那一天日期String
		daysStrList.add(onlyDateformat(dBegin));

		Calendar calBegin = Calendar.getInstance();
		// 使用给定的 Date 设置此 Calendar 的时间
		calBegin.setTime(dBegin);

		Calendar calEnd = Calendar.getInstance();
		// 使用给定的 Date 设置此 Calendar 的时间
		calEnd.setTime(dEnd);

		// 判断循环此日期是否在指定日期之后
		while (dEnd.after(calBegin.getTime())) {
	    	// 根据日历的规则，给定的日历字段增加或减去指定的时间量
		    calBegin.add(Calendar.DAY_OF_MONTH, 1);
	        String dayStr = onlyDateformat(calBegin.getTime());
	    	daysStrList.add(dayStr);
		}

		return daysStrList;
	}

	public static void main(String[] args) {
		//System.out.println(findDaysStr("2019-01-01", "2019-01-31"));

		testPoints(new Object[]{"1234adsfdgd554",1,4});

	}

	public static void testPoints(Object[] itgr){
		if(itgr.length==0){
		System.out.println("没有Integer参数传入！");
		}else if(itgr.length==1){
		System.out.println("1个Integer参数传入！");
		}else{
		System.out.println("the input string is-->");
	for(int i=0;i<itgr.length;i++){
		System.out.println("第"+(i+1)+"个Integer参数是"+itgr[i]+"");
	}}}


}
