package test.serverframe.armc.server.manager.dao.mapper;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import test.serverframe.armc.server.manager.controller.params.TestUseCasePageParams;
import test.serverframe.armc.server.manager.controller.vo.InterfaceTestUseCaseTestStatusVO;
import test.serverframe.armc.server.manager.domain.IntegrationTestInterfaceUseCaseRecordAsso;
import test.serverframe.armc.server.manager.domain.TestUseCase;

import java.util.List;
import java.util.Map;

public interface TestUseCaseMapper {
    @Delete({
            "delete from tb_ssgp_csgj_test_use_case",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String id);

    @Insert({
            "insert into tb_ssgp_csgj_test_use_case (id, interface_id, ",
            "interface_name, use_case_name, ",
            "remark, source, ",
            "delete_flag, in_header, ",
            "in_body, in_query, request_method, request_url)",
            "values (#{id,jdbcType=VARCHAR}, #{interfaceId,jdbcType=VARCHAR}, ",
            "#{interfaceName,jdbcType=VARCHAR}, #{useCaseName,jdbcType=VARCHAR}, ",
            "#{remark,jdbcType=VARCHAR}, #{source,jdbcType=VARCHAR}, ",
            "#{createTime,jdbcType=TIMESTAMP}, #{updateTime,jdbcType=TIMESTAMP},",
            "#{deleteFlag,jdbcType=VARCHAR}, #{inHeader,jdbcType=VARCHAR}, ",
            "#{inBody,jdbcType=VARCHAR}, #{inQuery,jdbcType=VARCHAR},",
            "#{requestMethod,jdbcType=VARCHAR}, ##{requestUrl,jdbcType=VARCHAR}"
    })
    int insert(TestUseCase record);

    @InsertProvider(type = TestUseCaseSqlProvider.class, method = "insertSelective")
    @Options
    int insertSelective(TestUseCase record);

    @Select({
            "select",
            "id, interface_id, interface_name, request_method, request_url, use_case_name, remark, source, delete_flag, ",
            "in_header, in_body, in_query, create_time, update_time",
            "from tb_ssgp_csgj_test_use_case",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "interface_id", property = "interfaceId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "interface_name", property = "interfaceName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "use_case_name", property = "useCaseName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "request_method", property = "requestMethod", jdbcType = JdbcType.VARCHAR),
            @Result(column = "request_url", property = "requestUrl", jdbcType = JdbcType.VARCHAR),
            @Result(column = "remark", property = "remark", jdbcType = JdbcType.VARCHAR),
            @Result(column = "source", property = "source", jdbcType = JdbcType.VARCHAR),
            @Result(column = "delete_flag", property = "deleteFlag", jdbcType = JdbcType.VARCHAR),
            @Result(column = "in_header", property = "inHeader", jdbcType = JdbcType.VARCHAR),
            @Result(column = "in_body", property = "inBody", jdbcType = JdbcType.VARCHAR),
            @Result(column = "in_query", property = "inQuery", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    TestUseCase selectByPrimaryKey(String id);

    @UpdateProvider(type = TestUseCaseSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(TestUseCase record);

    @Update({
            "update tb_ssgp_csgj_test_use_case",
            "set interface_id = #{interfaceId,jdbcType=VARCHAR},",
            "interface_name = #{interfaceName,jdbcType=VARCHAR},",
            "use_case_name = #{useCaseName,jdbcType=VARCHAR},",
            "request_method = #{requestMethod,jdbcType=VARCHAR},",
            "request_url = #{requestUrl,jdbcType=VARCHAR},",
            "remark = #{remark,jdbcType=VARCHAR},",
            "create_time = #{createTime,jdbcType=TIMESTAMP},",
            "update_time = #{updateTime,jdbcType=TIMESTAMP}",
            "source = #{source,jdbcType=VARCHAR},",
            "delete_flag = #{deleteFlag,jdbcType=VARCHAR},",
            "in_header = #{inHeader,jdbcType=VARCHAR},",
            "in_body = #{inBody,jdbcType=VARCHAR},",
            "in_query = #{inQuery,jdbcType=VARCHAR}",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKeyWithBLOBs(TestUseCase record);

    @Update({
            "update tb_ssgp_csgj_test_use_case",
            "set interface_id = #{interfaceId,jdbcType=VARCHAR},",
            "interface_name = #{interfaceName,jdbcType=VARCHAR},",
            "use_case_name = #{useCaseName,jdbcType=VARCHAR},",
            "request_method = #{requestMethod,jdbcType=VARCHAR},",
            "request_url = #{requestUrl,jdbcType=VARCHAR},",
            "remark = #{remark,jdbcType=VARCHAR},",
            "create_time = #{createTime,jdbcType=TIMESTAMP},",
            "update_time = #{updateTime,jdbcType=TIMESTAMP}",
            "source = #{source,jdbcType=VARCHAR},",
            "delete_flag = #{deleteFlag,jdbcType=VARCHAR}",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(TestUseCase record);

    @SelectProvider(type = TestUseCaseSqlProvider.class, method = "selectByPage")
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "interface_id", property = "interfaceId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "interface_name", property = "interfaceName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "use_case_name", property = "useCaseName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "request_method", property = "requestMethod", jdbcType = JdbcType.VARCHAR),
            @Result(column = "request_url", property = "requestUrl", jdbcType = JdbcType.VARCHAR),
            @Result(column = "remark", property = "remark", jdbcType = JdbcType.VARCHAR),
            @Result(column = "source", property = "source", jdbcType = JdbcType.VARCHAR),
            @Result(column = "delete_flag", property = "deleteFlag", jdbcType = JdbcType.VARCHAR),
            @Result(column = "in_header", property = "inHeader", jdbcType = JdbcType.VARCHAR),
            @Result(column = "in_body", property = "inBody", jdbcType = JdbcType.VARCHAR),
            @Result(column = "in_query", property = "inQuery", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    List<TestUseCase> selectByPage(TestUseCasePageParams params);

    @SelectProvider(type = TestUseCaseSqlProvider.class, method = "selectByInterfaceId")
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "interface_id", property = "interfaceId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "interface_name", property = "interfaceName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "use_case_name", property = "useCaseName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "request_method", property = "requestMethod", jdbcType = JdbcType.VARCHAR),
            @Result(column = "request_url", property = "requestUrl", jdbcType = JdbcType.VARCHAR),
            @Result(column = "remark", property = "remark", jdbcType = JdbcType.VARCHAR),
            @Result(column = "source", property = "source", jdbcType = JdbcType.VARCHAR),
            @Result(column = "delete_flag", property = "deleteFlag", jdbcType = JdbcType.VARCHAR),
            @Result(column = "in_header", property = "inHeader", jdbcType = JdbcType.VARCHAR),
            @Result(column = "in_body", property = "inBody", jdbcType = JdbcType.VARCHAR),
            @Result(column = "in_query", property = "inQuery", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP)
    })
    List<TestUseCase> selectByInterfaceId(Map<String, Object> params);

    @Select({
            "select uc.*,inter_record.status,inter_record.out_value,inter_record.assert_value,inter_record.start_time,inter_record.end_time,inter_record.status",
            "from tb_ssgp_csgj_integration_test_interface_use_case_record_asso asso",
            "inner join tb_ssgp_csgj_test_use_case uc on asso.tuc_id = uc.id",
            "inner join tb_ssgp_csgj_interface_test_record inter_record on asso.itr_id = inter_record.id",
            "where asso.itir_id = #{itirId,jdbcType=VARCHAR} and asso.tuc_id = #{tucId,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "interface_id", property = "interfaceId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "interface_name", property = "interfaceName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "use_case_name", property = "useCaseName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "request_method", property = "requestMethod", jdbcType = JdbcType.VARCHAR),
            @Result(column = "request_url", property = "requestUrl", jdbcType = JdbcType.VARCHAR),
            @Result(column = "remark", property = "remark", jdbcType = JdbcType.VARCHAR),
            @Result(column = "source", property = "source", jdbcType = JdbcType.VARCHAR),
            @Result(column = "delete_flag", property = "deleteFlag", jdbcType = JdbcType.VARCHAR),
            @Result(column = "in_header", property = "inHeader", jdbcType = JdbcType.VARCHAR),
            @Result(column = "in_body", property = "inBody", jdbcType = JdbcType.VARCHAR),
            @Result(column = "in_query", property = "inQuery", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "out_value", property = "outValue", jdbcType = JdbcType.VARCHAR),
            @Result(column = "assert_value", property = "assertValue", jdbcType = JdbcType.VARCHAR),
            @Result(column = "start_time", property = "startTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "end_time", property = "endTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "status", property = "status", jdbcType = JdbcType.VARCHAR)
    })
    List<InterfaceTestUseCaseTestStatusVO> selectByItirId(IntegrationTestInterfaceUseCaseRecordAsso asso);
}
