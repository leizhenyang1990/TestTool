package test.serverframe.armc.server.manager.service;

import com.microcore.entity.AssertCondition;
import com.microcore.service.AssertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import test.serverframe.armc.server.manager.controller.vo.InterfaceTestUseCaseTestStatusVO;
import test.serverframe.armc.server.manager.dao.mapper.TestUseCaseMapper;
import test.serverframe.armc.server.manager.domain.IntegrationTestInterfaceUseCaseRecordAsso;
import test.serverframe.armc.server.manager.domain.InterfaceTestRecord;
import test.serverframe.armc.server.manager.domain.TestUseCase;
import test.serverframe.armc.server.manager.service.params.TestUseCaseByInterfaceParams;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 测试用例
 *
 * @author LeiZhenYang
 * @date 2018.11.29
 */
@Service
public class TestUseCaseService extends BaseService<TestUseCase, String> {

    @Autowired
    private TestUseCaseMapper mapper;

    @Autowired
    private InterfaceTestService interfaceTestService;

    @Autowired
    private AssertService assertService;

    @Override
    protected String getMapperName() {
        return TestUseCaseMapper.class.getName();
    }

    /**
     * 新增测试用例
     *
     * @param testUseCase
     * @return
     */
    @Transactional
    @Override
    public int insertSelective(TestUseCase testUseCase) {
        if (StringUtils.isEmpty(testUseCase.getId())) {
            testUseCase.setId(UUID.randomUUID().toString());
        }
        testUseCase.setCreateTime(new Date());
        testUseCase.setUpdateTime(new Date());
        if (testUseCase.getAssertConditions() != null) {
        	for (AssertCondition assertCondition : testUseCase.getAssertConditions()) {
            assertCondition.setAssertId(UUID.randomUUID().toString());
            assertCondition.setFkId(testUseCase.getId());
            if (assertService.isJsonArray(assertCondition.getConditions())) {
                assertService.addAssert(assertCondition);
            }
        }
        }
        
        return mapper.insertSelective(testUseCase);
    }

    @Override
    public int updateByPrimaryKeySelective(TestUseCase testUseCase) {
        testUseCase.setCreateTime(null);
        testUseCase.setUpdateTime(new Date());
        for (AssertCondition assertCondition : testUseCase.getAssertConditions()) {
            if (assertService.isJsonArray(assertCondition.getConditions())) {
                //删除原来的断言判断
                assertService.deleteAssert(testUseCase.getId());
                //重新增加此用例的断言判断
                assertCondition.setAssertId(UUID.randomUUID().toString());
                assertService.addAssert(assertCondition);
            }
        }
        return super.updateByPrimaryKeySelective(testUseCase);
    }

    @Override
    public TestUseCase selectByPrimaryKey(String id) {
        List<AssertCondition> assertConditions = assertService.getAssert(id);
        TestUseCase testUseCase = super.selectByPrimaryKey(id);
        testUseCase.setAssertConditions(assertConditions);
        return testUseCase;
    }

    /**
     * 修改测试用例
     *
     * @param testUseCase
     * @return
     */
    @Transactional
    public int updateSelective(TestUseCase testUseCase) {
        testUseCase.setCreateTime(null);
        testUseCase.setUpdateTime(new Date());
        return super.updateByPrimaryKeySelective(testUseCase);
    }

    /**
     * 执行测试用例并记录执行结果
     *
     * @param testUseCase
     * @return
     */
    @Transactional
    public InterfaceTestRecord executeTestUseCaseAndRecode(TestUseCase testUseCase) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        testUseCase.setUseCaseName("接口测试的用例");
        testUseCase.setRemark("系统生成");
        testUseCase.setSource(TestUseCase.Source.InterfaceTest);
        TestUseCase sameTestUseCase = getSameTestUseCase(testUseCase);
        if (sameTestUseCase != null) {
            // 已存在相同的测试用例，则使用原用例
            sameTestUseCase.setAssertConditions(testUseCase.getAssertConditions());
            testUseCase = sameTestUseCase;
        } else {
            // 不存在相同的测试用例，则新建一个用例
            testUseCase.setId(UUID.randomUUID().toString());
            insertSelective(testUseCase);
        }
        return interfaceTestService.callAndRecord(testUseCase);
    }

    /**
     * 根据接口ID获取测试用例，判断是否有相同的用例
     *
     * @param testUseCase
     * @return
     */
    private TestUseCase getSameTestUseCase(TestUseCase testUseCase) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        String interfaceId = testUseCase.getInterfaceId();
        List<TestUseCase> testUseCases = mapper.selectByInterfaceId(getBaseDao().getParamsFromObj(new TestUseCaseByInterfaceParams(interfaceId)));
        for (TestUseCase newTUC : testUseCases) {
            if (newTUC.equals(testUseCase)) {
                return newTUC;
            }
        }
        return null;
    }

    /**
     * 获取集成测试用例接口执行状态
     *
     * @param asso
     * @return
     */
    public List<InterfaceTestUseCaseTestStatusVO> getUseCaseExecuteStatusByItirId(IntegrationTestInterfaceUseCaseRecordAsso asso) {
        return mapper.selectByItirId(asso);
    }
}
