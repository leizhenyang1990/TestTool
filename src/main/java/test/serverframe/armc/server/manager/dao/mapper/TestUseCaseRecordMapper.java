package test.serverframe.armc.server.manager.dao.mapper;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import test.serverframe.armc.server.manager.domain.TestUseCaseRecord;

import java.util.List;

public interface TestUseCaseRecordMapper {
    @Delete({
            "delete from tb_ssgp_csgj_test_use_case_record",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String id);

    @Insert({
            "insert into tb_ssgp_csgj_test_use_case_record (id, itir_id, ",
            "status, start_time, ",
            "end_time, total, ",
            "success_total, test_use_case_name, test_use_case_id)",
            "values (#{id,jdbcType=VARCHAR}, #{itirId,jdbcType=VARCHAR}, ",
            "#{status,jdbcType=VARCHAR}, #{startTime,jdbcType=TIMESTAMP}, ",
            "#{endTime,jdbcType=TIMESTAMP}, #{total,jdbcType=INTEGER}, ",
            "#{successTotal,jdbcType=INTEGER}, #{testUseCaseName,jdbcType=VARCHAR}, #{testUseCaseId,jdbcType=VARCHAR})"
    })
    int insert(TestUseCaseRecord record);

    @InsertProvider(type = TestUseCaseRecordSqlProvider.class, method = "insertSelective")
    int insertSelective(TestUseCaseRecord record);

    @Select({
            "select",
            "id, itir_id, status, start_time, end_time, total, success_total, test_use_case_name, test_use_case_id",
            "from tb_ssgp_csgj_test_use_case_record",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "itir_id", property = "itirId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "status", property = "status", jdbcType = JdbcType.VARCHAR),
            @Result(column = "start_time", property = "startTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "end_time", property = "endTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "total", property = "total", jdbcType = JdbcType.INTEGER),
            @Result(column = "success_total", property = "successTotal", jdbcType = JdbcType.INTEGER),
            @Result(column = "test_use_case_name", property = "testUseCaseName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "test_use_case_id", property = "testUseCaseId", jdbcType = JdbcType.VARCHAR)
    })
    TestUseCaseRecord selectByPrimaryKey(String id);

    @UpdateProvider(type = TestUseCaseRecordSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(TestUseCaseRecord record);

    @Update({
            "update tb_ssgp_csgj_test_use_case_record",
            "set itir_id = #{itirId,jdbcType=VARCHAR},",
            "status = #{status,jdbcType=VARCHAR},",
            "start_time = #{startTime,jdbcType=TIMESTAMP},",
            "end_time = #{endTime,jdbcType=TIMESTAMP},",
            "total = #{total,jdbcType=INTEGER},",
            "success_total = #{successTotal,jdbcType=INTEGER}",
            "test_use_case_name = #{testUseCaseName,jdbcType=VARCHAR},",
            "test_use_case_id = #{testUseCaseId,jdbcType=VARCHAR},",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(TestUseCaseRecord record);

    @Select({
            "select",
            "id, itir_id, status, start_time, end_time, total, success_total, test_use_case_name, test_use_case_id",
            "from tb_ssgp_csgj_test_use_case_record",
            "where itir_id = #{itirId,jdbcType=VARCHAR} order by start_time DESC"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "itir_id", property = "itirId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "status", property = "status", jdbcType = JdbcType.VARCHAR),
            @Result(column = "start_time", property = "startTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "end_time", property = "endTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "total", property = "total", jdbcType = JdbcType.INTEGER),
            @Result(column = "success_total", property = "successTotal", jdbcType = JdbcType.INTEGER),
            @Result(column = "test_use_case_name", property = "testUseCaseName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "test_use_case_id", property = "testUseCaseId", jdbcType = JdbcType.VARCHAR)
    })
    List<TestUseCaseRecord> selectByItirId(String itirId);
}