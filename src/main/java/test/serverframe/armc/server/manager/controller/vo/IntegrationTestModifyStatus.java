package test.serverframe.armc.server.manager.controller.vo;

import io.swagger.annotations.ApiModelProperty;

/**
 * @author LeiZhenYang
 * @date 2019.01.10
 */
public class IntegrationTestModifyStatus {

    @ApiModelProperty(value = "集成测试主键", required = true)
    private String id;

    @ApiModelProperty("状态（启用、禁用）")
    private Integer status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
