package test.serverframe.armc.server.manager.service.exec.caller;

/**
 * @author LeiZhenYang
 * @date 2019.01.03
 */
public class TestUseCaseRPCCaller implements Caller {

    @Override
    public void asyncCall() {

    }

    @Override
    public Object syncCall() {
        return null;
    }

}
