package test.serverframe.armc.server.manager.model;

import java.io.Serializable;

/**
 * 注册中心实例
 *
 * @author LeiZhenYang
 * @date 2018.11.24
 */
public class CenterInstance implements Serializable{

    /**
     * 服务id
     */
    private String sid;

    /**
     * 实例id
     */
    private String siId;

    /**
     * 注册中心id
     */
    private String siEurekaId;
    /**
     * 实例名称
     */
    private String siName;
    /**
     * 实例Host
     */
    private String siHostName;
    /**
     * 应用
     */
    private String siApp;
    /**
     * 实例IP
     */
    private String siIpAddress;
    /**
     * 实例状态
     */
    private String siStatus;
    /**
     * 实例端口
     */
    private String siPort;

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getSiId() {
        return siId;
    }

    public void setSiId(String siId) {
        this.siId = siId;
    }

    public String getSiEurekaId() {
        return siEurekaId;
    }

    public void setSiEurekaId(String siEurekaId) {
        this.siEurekaId = siEurekaId;
    }

    public String getSiName() {
        return siName;
    }

    public void setSiName(String siName) {
        this.siName = siName;
    }

    public String getSiHostName() {
        return siHostName;
    }

    public void setSiHostName(String siHostName) {
        this.siHostName = siHostName;
    }

    public String getSiApp() {
        return siApp;
    }

    public void setSiApp(String siApp) {
        this.siApp = siApp;
    }

    public String getSiIpAddress() {
        return siIpAddress;
    }

    public void setSiIpAddress(String siIpAddress) {
        this.siIpAddress = siIpAddress;
    }

    public String getSiStatus() {
        return siStatus;
    }

    public void setSiStatus(String siStatus) {
        this.siStatus = siStatus;
    }

    public String getSiPort() {
        return siPort;
    }

    public void setSiPort(String siPort) {
        this.siPort = siPort;
    }
}
