package test.serverframe.armc.server.manager.dao.mapper;

import org.apache.ibatis.jdbc.SQL;
import test.serverframe.armc.server.manager.domain.IntegrationTestInterfaceUseCaseRecordAsso;

public class IntegrationTestInterfaceUseCaseRecordAssoSqlProvider {

    public String insertSelective(IntegrationTestInterfaceUseCaseRecordAsso record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("tb_ssgp_csgj_integration_test_interface_use_case_record_asso");

        if (record.getItirId() != null) {
            sql.VALUES("itir_id", "#{itirId,jdbcType=VARCHAR}");
        }

        if (record.getTucId() != null) {
            sql.VALUES("tuc_id", "#{tucId,jdbcType=VARCHAR}");
        }

        if (record.getItrId() != null) {
            sql.VALUES("itr_id", "#{itrId,jdbcType=VARCHAR}");
        }

        return sql.toString();
    }
}
