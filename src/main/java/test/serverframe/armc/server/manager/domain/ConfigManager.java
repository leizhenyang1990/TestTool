package test.serverframe.armc.server.manager.domain;

public class ConfigManager {
	
	private String id;
	
    private String systemName;

    private Integer httpConnectTimeout;

    private Integer httpReadTimeout;

    private Integer recordTime;
    
    private Integer httpConnectionRequestTimeout;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName == null ? null : systemName.trim();
    }

    public Integer getHttpConnectTimeout() {
        return httpConnectTimeout;
    }

    public void setHttpConnectTimeout(Integer httpConnectTimeout) {
        this.httpConnectTimeout = httpConnectTimeout;
    }

    public Integer getHttpReadTimeout() {
        return httpReadTimeout;
    }

    public void setHttpReadTimeout(Integer httpReadTimeout) {
        this.httpReadTimeout = httpReadTimeout;
    }

    public Integer getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Integer recordTime) {
        this.recordTime = recordTime;
    }

	public Integer getHttpConnectionRequestTimeout() {
		return httpConnectionRequestTimeout;
	}

	public void setHttpConnectionRequestTimeout(Integer httpConnectionRequestTimeout) {
		this.httpConnectionRequestTimeout = httpConnectionRequestTimeout;
	}
}