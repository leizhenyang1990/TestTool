package test.serverframe.armc.server.manager.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import test.serverframe.armc.server.manager.common.ResultDtoUtil;
import test.serverframe.armc.server.manager.controller.vo.TestUseCaseCallInterface;
import test.serverframe.armc.server.manager.domain.InterfaceTestRecord;
import test.serverframe.armc.server.manager.domain.TestUseCase;
import test.serverframe.armc.server.manager.dto.ResultDto;
import test.serverframe.armc.server.manager.service.TestUseCaseService;

import javax.validation.Valid;

/**
 * @author LeiZhenYang
 * @date 2018.11.29
 */
@RestController
@RequestMapping("interface/test")
@CrossOrigin(origins = "*")
@Api("接口测试")
public class InterfaceTestController extends BaseController {

    /**
     * 用例测试服务
     */
    @Autowired
    private TestUseCaseService testUseCaseService;

    /**
     * 接口测试
     *
     * @param model
     * @return
     */
    @PostMapping("/call")
    @ApiOperation(notes = "接口测试", value = "接口测试")
    public ResultDto<InterfaceTestRecord> call(@ApiParam("接口测试模型") @RequestBody @Valid TestUseCaseCallInterface model) {
        if (model == null) {
            return ResultDtoUtil.error("用例测试实体不能为空");
        }
        try {
            return ResultDtoUtil.success(testUseCaseService.executeTestUseCaseAndRecode(new TestUseCase(model)));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return ResultDtoUtil.error("接口测试失败");
    }
}
