package test.serverframe.armc.server.manager.controller.params;

import io.swagger.annotations.ApiModelProperty;

/**
 * @Author: Uaena
 * @Date: 2019/1/7 0007 19:48
 * @Version 1.0
 */
public class ConfigManager {

    @ApiModelProperty(value = "banner名称")
    private String name;

    @ApiModelProperty(value = "记录保留天数")
    private Integer time;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }
}
