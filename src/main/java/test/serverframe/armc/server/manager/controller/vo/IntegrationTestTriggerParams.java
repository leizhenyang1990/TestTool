package test.serverframe.armc.server.manager.controller.vo;

import com.microcore.entity.param.TriggerParams;

/**
 * @author LeiZhenYang
 * @date 2019.01.09
 */
public class IntegrationTestTriggerParams extends TriggerParams {

    private int executeCount;

    private int concurrentCount;

    public int getExecuteCount() {
        return executeCount;
    }

    public void setExecuteCount(int executeCount) {
        this.executeCount = executeCount;
    }

    public int getConcurrentCount() {
        return concurrentCount;
    }

    public void setConcurrentCount(int concurrentCount) {
        this.concurrentCount = concurrentCount;
    }
}
