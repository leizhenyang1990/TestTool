package test.serverframe.armc.server.manager.controller.params;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Min;

/**
 * @author LeiZhenYang
 * @date 2018.11.29
 */
public class PageParams {

    @Min(value = 1, message = "pageNum必须大于0")
    @ApiModelProperty(value = "页码", required = true, example = "1")
    private int pageNum;

    @Min(value = 1, message = "pageSize必须大于0")
    @ApiModelProperty(value = "数量", required = true, example = "10")
    private int pageSize;

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
