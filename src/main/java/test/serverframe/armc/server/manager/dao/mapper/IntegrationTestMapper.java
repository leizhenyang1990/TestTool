package test.serverframe.armc.server.manager.dao.mapper;

import com.microcore.dao.mapper.InvokeTriggerMapper;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import test.serverframe.armc.server.manager.controller.params.IntegrationTestPageParams;
import test.serverframe.armc.server.manager.controller.vo.IntegrationTestModify;
import test.serverframe.armc.server.manager.controller.vo.IntegrationTestPage;
import test.serverframe.armc.server.manager.domain.IntegrationTest;

import java.util.List;

public interface IntegrationTestMapper {
    @Update({
            "update tb_ssgp_csgj_integration_test",
            "set is_used = 0 ",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String id);



    @Insert({
            "insert into tb_ssgp_csgj_integration_test (id, name, ",
            "remark, create_time, ",
            "update_time, class_name, method_name, params, start_time, end_time, is_concurrent, status, execute_plan_data, is_used)",
            "values (#{id,jdbcType=VARCHAR}, #{name,jdbcType=VARCHAR}, ",
            "#{remark,jdbcType=VARCHAR}, #{createTime,jdbcType=TIMESTAMP}, ",
            "#{updateTime,jdbcType=TIMESTAMP}, #{className,jdbcType=VARCHAR},",
            "#{methodName,jdbcType=VARCHAR},#{params,jdbcType=VARCHAR},",
            "#{startTime,jdbcType=TIMESTAMP},#{endTime,jdbcType=TIMESTAMP},",
            "#{status,jdbcType=INTEGER}, #{isConcurrent,jdbcType=BIT}, #{executePlanData,jdbcType=VARCHAR}, #{isUsed,jdbcType=BIT})"

    })
    int insert(IntegrationTest record);

    @InsertProvider(type = IntegrationTestSqlProvider.class, method = "insertSelective")
    int insertSelective(IntegrationTest record);

    @Select({
            "select",
            "id, name, remark, create_time, update_time, class_name, method_name, params, status, is_concurrent, start_time, end_time, execute_plan_data ",
            "from tb_ssgp_csgj_integration_test",
            "where id = #{id,jdbcType=VARCHAR} and is_used =1"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "name", property = "name", jdbcType = JdbcType.VARCHAR),
            @Result(column = "remark", property = "remark", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "class_name", property = "className", jdbcType = JdbcType.VARCHAR),
            @Result(column = "method_name", property = "methodName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "params", property = "params", jdbcType = JdbcType.VARCHAR),
            @Result(column = "status", property = "status", jdbcType = JdbcType.INTEGER),
            @Result(column = "is_concurrent", property = "isConcurrent", jdbcType = JdbcType.BIT),
            @Result(column = "start_time", property = "startTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "end_time", property = "endTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "execute_plan_data", property = "executePlanData", jdbcType = JdbcType.VARCHAR),
            @Result(column = "id",property = "integrationTestTriggerParams",many = @Many(
                    select = "com.microcore.dao.mapper.InvokeTriggerMapper.getTriggersByInvokeId"
            )),
            @Result(column = "id", property = "interfaces",many = @Many(
                    select = "test.serverframe.armc.server.manager.dao.mapper.IntegrationTestInterfaceMapper.selectByItId"
            ))
    })
    IntegrationTest selectByPrimaryKey(String id);

    @UpdateProvider(type = IntegrationTestSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(IntegrationTest record);

    @Update({
            "update tb_ssgp_csgj_integration_test",
            "set name = #{name,jdbcType=VARCHAR},",
            "remark = #{remark,jdbcType=VARCHAR},",
            "create_time = #{createTime,jdbcType=TIMESTAMP},",
            "update_time = #{updateTime,jdbcType=TIMESTAMP}",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(IntegrationTest record);


    @SelectProvider(type = IntegrationTestSqlProvider.class, method = "selectByPage")
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "name", property = "name", jdbcType = JdbcType.VARCHAR),
            @Result(column = "remark", property = "remark", jdbcType = JdbcType.VARCHAR),
            @Result(column = "create_time", property = "createTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "update_time", property = "updateTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "status", property = "status", jdbcType = JdbcType.INTEGER),
            @Result(column = "last_test_time", property = "lastTestTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "last_test_status", property = "lastTestStatus", jdbcType = JdbcType.INTEGER),
            @Result(column = "last_test_interface_total", property = "lastTestInterfaceTotal", jdbcType = JdbcType.VARCHAR),
            @Result(column = "last_test_success_interface_total", property = "lastTestSuccessInterfaceTotal", jdbcType = JdbcType.VARCHAR),
            @Result(column = "last_test_record_id", property = "lastTestRecordId", jdbcType = JdbcType.VARCHAR),
    })
    List<IntegrationTestPage> selectByPage(IntegrationTestPageParams params);
}
