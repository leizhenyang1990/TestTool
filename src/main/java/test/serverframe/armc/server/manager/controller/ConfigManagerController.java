package test.serverframe.armc.server.manager.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import test.serverframe.armc.server.manager.common.ResultDtoUtil;
import test.serverframe.armc.server.manager.controller.vo.ConfigManagerVo;
import test.serverframe.armc.server.manager.domain.ConfigManager;
import test.serverframe.armc.server.manager.dto.ResultDto;
import test.serverframe.armc.server.manager.service.ConfigManagerService;

/**
 * @Author: Uaena
 * @Date: 2019/1/7 0007 19:41
 * @Version 1.0
 */
@CrossOrigin(origins = "*")
@Api("配置管理")
@Validated
@RequestMapping("/config")
@RestController
public class ConfigManagerController {
	
	@Autowired
	public ConfigManagerService configManagerService;
    
    @PostMapping
    @ApiOperation(notes = "修改配置", value = "修改配置")
    public ResultDto<Integer> UpdateConfig(@RequestBody ConfigManagerVo param) {
        ResultDto<Integer> result;
        try {
            result = ResultDtoUtil.success(configManagerService.updateConfig(param));
        } catch (Exception e) {
            e.printStackTrace();
            result = ResultDtoUtil.error(500, "更改配置错误");
        }

        return result;

    }

    @GetMapping
    @ApiOperation(notes = "获取配置", value = "获取配置")
    public ResultDto<ConfigManager> getConfig() {
    	ResultDto<ConfigManager> result;
    	try {
            result = ResultDtoUtil.success(configManagerService.selectConfig());
        } catch (Exception e) {
            e.printStackTrace();
            result = ResultDtoUtil.error(500, "查询配置出错");
        }
        return result;


    }
    
    /*@PostMapping
    @ApiOperation(notes = "修改配置", value = "修改配置")
    public ResultDto<Boolean> setConfig(@RequestParam(value = "name", required = false) String name,
                                        @RequestParam(value = "time", required = false) Integer time) {
         Initialize data. 
        ResultDto<Boolean> result;
        ConfigManager configManager = new ConfigManager();
        configManager.setName(name);
        configManager.setTime(time);
         Export data to a YAML file. 
        URL resource = ConfigManagerController.class.getClassLoader().getResource("config-manager.yml");
        String path = System.getProperty("user.dir") + "\\src\\main\\resources\\config-manager.yml";
        File file = new File(path);
        File dumpFile = new File(resource.getPath());
        try {
            Yaml.dump(configManager, file);
            Yaml.dump(configManager, dumpFile);
            result = ResultDtoUtil.success();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            result = ResultDtoUtil.error(500, "更改配置错误");
        }

        return result;

    }
    @GetMapping
    @ApiOperation(notes = "获取配置", value = "获取配置")
    public ResultDto<ConfigManager> getConfig() throws FileNotFoundException {
        URL resource = ConfigManagerController.class.getClassLoader().getResource("config-manager.yml");
        File dumpFile = new File(resource.getPath());
        ConfigManager configManager = (ConfigManager) Yaml.loadType(dumpFile, ConfigManager.class);
        return ResultDtoUtil.success(configManager);


    }*/

}
