package test.serverframe.armc.server.manager.dao.mapper;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import test.serverframe.armc.server.manager.domain.IntegrationTestInterfaceRecord;
import test.serverframe.armc.server.manager.domain.extend.RecordCounter;

import java.util.List;

public interface IntegrationTestInterfaceRecordMapper {
    @Delete({
            "delete from tb_ssgp_csgj_integration_test_interface_record",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String id);

    @Insert({
            "insert into tb_ssgp_csgj_integration_test_interface_record (id, itr_id, ",
            "interface_id, interface_name, ",
            "status, total, ",
            "success_total, start_time, end_time)",
            "values (#{id,jdbcType=VARCHAR}, #{itrId,jdbcType=VARCHAR}, ",
            "#{interfaceId,jdbcType=VARCHAR}, #{interfaceName,jdbcType=VARCHAR}, ",
            "#{status,jdbcType=VARCHAR}, #{total,jdbcType=INTEGER}, ",
            "#{successTotal,jdbcType=INTEGER}, #{startTime,jdbcType=TIMESTAMP}, #{endTime,jdbcType=TIMESTAMP} )"
    })
    int insert(IntegrationTestInterfaceRecord record);

    @InsertProvider(type = IntegrationTestInterfaceRecordSqlProvider.class, method = "insertSelective")
    int insertSelective(IntegrationTestInterfaceRecord record);

    @Select({
            "select",
            "id, itr_id, interface_id, interface_name, status, total, success_total, start_time, end_time",
            "from tb_ssgp_csgj_integration_test_interface_record",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "itr_id", property = "itrId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "interface_id", property = "interfaceId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "interface_name", property = "interfaceName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "status", property = "status", jdbcType = JdbcType.VARCHAR),
            @Result(column = "total", property = "total", jdbcType = JdbcType.INTEGER),
            @Result(column = "success_total", property = "successTotal", jdbcType = JdbcType.INTEGER),
            @Result(column = "start_time", property = "startTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "end_time", property = "endTime", jdbcType = JdbcType.TIMESTAMP)
    })
    IntegrationTestInterfaceRecord selectByPrimaryKey(String id);

    @UpdateProvider(type = IntegrationTestInterfaceRecordSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(IntegrationTestInterfaceRecord record);

    @Update({
            "update tb_ssgp_csgj_integration_test_interface_record",
            "set itr_id = #{itrId,jdbcType=VARCHAR},",
            "interface_id = #{interfaceId,jdbcType=VARCHAR},",
            "interface_name = #{interfaceName,jdbcType=VARCHAR},",
            "status = #{status,jdbcType=VARCHAR},",
            "total = #{total,jdbcType=INTEGER},",
            "success_total = #{successTotal,jdbcType=INTEGER}",
            "start_time = #{startTime,jdbcType=TIMESTAMP},",
            "end_time = #{endTime,jdbcType=TIMESTAMP},",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(IntegrationTestInterfaceRecord record);


    @Select({"select",
            "id, itr_id, interface_id, interface_name, status, total, success_total, start_time, end_time",
            "from tb_ssgp_csgj_integration_test_interface_record",
            "where itr_id = #{itrId,jdbcType=VARCHAR}"})
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "itr_id", property = "itrId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "interface_id", property = "interfaceId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "interface_name", property = "interfaceName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "status", property = "status", jdbcType = JdbcType.VARCHAR),
            @Result(column = "total", property = "total", jdbcType = JdbcType.INTEGER),
            @Result(column = "success_total", property = "successTotal", jdbcType = JdbcType.INTEGER),
            @Result(column = "start_time", property = "startTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "end_time", property = "endTime", jdbcType = JdbcType.TIMESTAMP)
    })
    List<IntegrationTestInterfaceRecord> selectByItrId(String itrId);


    @Select({
            "select interface.itr_id,interface_total,test_use_case_total from",
            "(",
            "SELECT itr_id,count(id) AS interface_total",
            "FROM tb_ssgp_csgj_integration_test_interface_record",
            "WHERE itr_id = #{itrId,jdbcType=VARCHAR}",
            "GROUP BY itr_id",
            ") interface",
            "inner join",
            "(",
            "SELECT #{itrId,jdbcType=VARCHAR} as itr_id,sum(t.count) AS test_use_case_total",
            "FROM (",
            "SELECT",
            "itir_id,",
            "count(id) AS 'count'",
            "FROM tb_ssgp_csgj_test_use_case_record",
            "WHERE itir_id IN (",
            "SELECT id",
            "FROM tb_ssgp_csgj_integration_test_interface_record",
            "WHERE itr_id = #{itrId,jdbcType=VARCHAR}",
            ")",
            "GROUP BY itir_id",
            ") t",
            ") test_use_case on interface.itr_id = test_use_case.itr_id"
    })
    @Results({
            @Result(column = "itr_id", property = "itrId", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "interface_total", property = "interfaceTotal", jdbcType = JdbcType.INTEGER),
            @Result(column = "test_use_case_total", property = "testUseCaseTotal", jdbcType = JdbcType.INTEGER)
    })
    RecordCounter countByItrId(String itrId);
}
