package test.serverframe.armc.server.manager.controller.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author LeiZhenYang
 * @date 2018.11.30
 */
@ApiModel("集成测试修改模型")
public class IntegrationTestModify extends IntegrationTestAdd {

    @ApiModelProperty(value = "集成测试主键", required = true)
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
