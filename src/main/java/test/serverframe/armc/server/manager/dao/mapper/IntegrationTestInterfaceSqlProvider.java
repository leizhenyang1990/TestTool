package test.serverframe.armc.server.manager.dao.mapper;

import org.apache.ibatis.jdbc.SQL;
import test.serverframe.armc.server.manager.domain.IntegrationTestInterface;

public class IntegrationTestInterfaceSqlProvider {

    public String insertSelective(IntegrationTestInterface record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("tb_ssgp_csgj_integration_test_interface");
        
        if (record.getId() != null) {
            sql.VALUES("id", "#{id,jdbcType=VARCHAR}");
        }
        
        if (record.getItId() != null) {
            sql.VALUES("it_id", "#{itId,jdbcType=VARCHAR}");
        }
        
        if (record.getInterfaceId() != null) {
            sql.VALUES("interface_id", "#{interfaceId,jdbcType=VARCHAR}");
        }
        
        if (record.getInterfaceName() != null) {
            sql.VALUES("interface_name", "#{interfaceName,jdbcType=VARCHAR}");
        }
        
        return sql.toString();
    }

    public String updateByPrimaryKeySelective(IntegrationTestInterface record) {
        SQL sql = new SQL();
        sql.UPDATE("tb_ssgp_csgj_integration_test_interface");
        
        if (record.getItId() != null) {
            sql.SET("it_id = #{itId,jdbcType=VARCHAR}");
        }
        
        if (record.getInterfaceId() != null) {
            sql.SET("interface_id = #{interfaceId,jdbcType=VARCHAR}");
        }
        
        if (record.getInterfaceName() != null) {
            sql.SET("interface_name = #{interfaceName,jdbcType=VARCHAR}");
        }
        
        sql.WHERE("id = #{id,jdbcType=VARCHAR}");
        
        return sql.toString();
    }
}