package test.serverframe.armc.server.manager.controller.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
public class IntegrationTestEnterUseCaseAssoAdd {

    @ApiModelProperty(name = "集成测试接口id", hidden = true)
    private String itiId;

    @ApiModelProperty(name = "测试用例ID")
    private String tucId;

    public String getItiId() {
        return itiId;
    }

    public void setItiId(String itiId) {
        this.itiId = itiId;
    }

    public String getTucId() {
        return tucId;
    }

    public void setTucId(String tucId) {
        this.tucId = tucId == null ? null : tucId.trim();
    }
}
