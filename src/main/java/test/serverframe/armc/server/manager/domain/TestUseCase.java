package test.serverframe.armc.server.manager.domain;

import com.microcore.entity.AssertCondition;
import test.serverframe.armc.server.manager.controller.vo.TestUseCaseCallInterface;

import java.util.Date;
import java.util.List;
import java.util.Objects;

public class TestUseCase {

    private String id;

    private String interfaceId;

    private String interfaceName;

    private String requestMethod;

    private String requestUrl;

    private String useCaseName;

    private String remark;

    private Date createTime;

    private Date updateTime;

    private String source = Source.TestUseCase;

    private String deleteFlag = DeleteFlag.False;

    private String inHeader;

    private String inBody;

    private String inQuery;

    private List<AssertCondition> assertConditions;

    private List<InterfaceTestRecord> interfaceTestRecords;

    public List<AssertCondition> getAssertConditions() {
        return assertConditions;
    }

    public void setAssertConditions(List<AssertCondition> assertConditions) {
        this.assertConditions = assertConditions;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getInterfaceId() {
        return interfaceId;
    }

    public void setInterfaceId(String interfaceId) {
        this.interfaceId = interfaceId == null ? null : interfaceId.trim();
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName == null ? null : interfaceName.trim();
    }

    public String getUseCaseName() {
        return useCaseName;
    }

    public void setUseCaseName(String useCaseName) {
        this.useCaseName = useCaseName == null ? null : useCaseName.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source == null ? null : source.trim();
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag == null ? null : deleteFlag.trim();
    }

    public String getInHeader() {
        return inHeader;
    }

    public void setInHeader(String inHeader) {
        this.inHeader = inHeader == null ? null : inHeader.trim();
    }

    public String getInBody() {
        return inBody;
    }

    public void setInBody(String inBody) {
        this.inBody = inBody == null ? null : inBody.trim();
    }

    public String getInQuery() {
        return inQuery;
    }

    public void setInQuery(String inQuery) {
        this.inQuery = inQuery == null ? null : inQuery.trim();
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public List<InterfaceTestRecord> getInterfaceTestRecords() {
        return interfaceTestRecords;
    }

    public void setInterfaceTestRecords(List<InterfaceTestRecord> interfaceTestRecords) {
        this.interfaceTestRecords = interfaceTestRecords;
    }

    /**
     * 测试用例来源
     */
    public static class Source {

        /**
         * 测试用例
         */
        public static final String TestUseCase = "0";

        /**
         * 集成测试
         */
        public static final String IntegrationTest = "1";

        /**
         * 接口测试
         */
        public static final String InterfaceTest = "2";

    }


    /**
     * 测试用例来源
     */
    public static class DeleteFlag {

        /**
         * 测试用例
         */
        public static final String True = "0";

        /**
         * 集成测试
         */
        public static final String False = "1";


    }

    public TestUseCase() {

    }

    /**
     * 构建用于接口测试的测试用例对象
     *
     * @param testUseCase
     */
    public TestUseCase(TestUseCaseCallInterface testUseCase) {
        this.interfaceId = testUseCase.getInterfaceId();
        this.interfaceName = testUseCase.getInterfaceName();
        this.requestMethod = testUseCase.getRequestMethod();
        this.requestUrl = testUseCase.getRequestUrl();
        this.inHeader = testUseCase.getInHeader();
        this.inBody = testUseCase.getInBody();
        this.inQuery = testUseCase.getInQuery();
        this.assertConditions = testUseCase.getAssertConditions();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestUseCase that = (TestUseCase) o;
        // 以下参数相等时，认定为同一个测试用例
        return Objects.equals(interfaceId, that.interfaceId) &&
                Objects.equals(requestMethod, that.requestMethod) &&
                Objects.equals(requestUrl, that.requestUrl) &&
                Objects.equals(inHeader, that.inHeader) &&
                Objects.equals(inBody, that.inBody) &&
                Objects.equals(inQuery, that.inQuery);
    }

    @Override
    public int hashCode() {
        return Objects.hash(interfaceId, requestMethod, requestUrl, inHeader, inBody, inQuery);
    }

}
