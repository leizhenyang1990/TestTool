package test.serverframe.armc.server.manager.domain;

import io.swagger.annotations.ApiModelProperty;
import test.serverframe.armc.server.manager.controller.vo.IntegrationTestAdd;
import test.serverframe.armc.server.manager.controller.vo.IntegrationTestInterfaceAdd;
import test.serverframe.armc.server.manager.controller.vo.IntegrationTestTriggerParams;

import java.util.Date;
import java.util.List;

public class IntegrationTest {

    private String id;

    private String name;

    private String remark;

    private Date createTime;

    private Date updateTime;

    private String className;

    private String methodName;

    private String params;

    private Integer status;

    private Boolean isConcurrent;

    private Date startTime;

    private Date endTime;

    private String executePlanData;

    private Boolean isUsed;

    @ApiModelProperty(value = "集成测试的调度的trigger集合（不用填trigger属性）")
    private List<IntegrationTestTriggerParams> integrationTestTriggerParams;

    public List<IntegrationTestTriggerParams> getIntegrationTestTriggerParams() {
        return integrationTestTriggerParams;
    }

    public void setIntegrationTestTriggerParams(List<IntegrationTestTriggerParams> integrationTestTriggerParams) {
        this.integrationTestTriggerParams = integrationTestTriggerParams;
    }

    public Boolean getUsed() {
        return isUsed;
    }

    public void setUsed(Boolean used) {
        isUsed = used;
    }

    public String getExecutePlanData() {
        return executePlanData;
    }

    public void setExecutePlanData(String executePlanData) {
        this.executePlanData = executePlanData;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className == null ? null : className.trim();
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName == null ? null : methodName.trim();
    }

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params == null ? null : params.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean getConcurrent() {
        return isConcurrent;
    }

    public void setConcurrent(Boolean concurrent) {
        isConcurrent = concurrent;
    }

    private List<IntegrationTestInterface> interfaces;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public List<IntegrationTestInterface> getInterfaces() {
        return interfaces;
    }

    public void setInterfaces(List<IntegrationTestInterface> interfaces) {
        this.interfaces = interfaces;
    }

    public IntegrationTest() {
    }

    public IntegrationTest(IntegrationTestAdd record) {
        this.name = record.getName();
        this.remark = record.getRemark();
        //this.className = record.getClassName();
        //this.methodName = record.getMethodName();
        //this.params = record.getParams();
        this.status = record.getStatus();
        this.isConcurrent = record.getConcurrent();
        this.startTime = record.getStartTime();
        this.endTime = record.getEndTime();
        this.executePlanData=record.getExecutePlanData();
    }
}
