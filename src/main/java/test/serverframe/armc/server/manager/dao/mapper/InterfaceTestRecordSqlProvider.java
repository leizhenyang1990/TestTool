package test.serverframe.armc.server.manager.dao.mapper;

import org.apache.ibatis.jdbc.SQL;
import test.serverframe.armc.server.manager.domain.InterfaceTestRecord;

public class InterfaceTestRecordSqlProvider {

    public String insertSelective(InterfaceTestRecord record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("tb_ssgp_csgj_interface_test_record");
        
        if (record.getId() != null) {
            sql.VALUES("id", "#{id,jdbcType=VARCHAR}");
        }
        
        if (record.getUcId() != null) {
            sql.VALUES("uc_id", "#{ucId,jdbcType=VARCHAR}");
        }
        
        if (record.getOutValue() != null) {
            sql.VALUES("out_value", "#{outValue,jdbcType=VARCHAR}");
        }
        
        if (record.getStartTime() != null) {
            sql.VALUES("start_time", "#{startTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getEndTime() != null) {
            sql.VALUES("end_time", "#{endTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getStatus() != null) {
            sql.VALUES("status", "#{status,jdbcType=VARCHAR}");
        }

        if (record.getAssertValue() != null) {
            sql.VALUES("assert_value", "#{assertValue,jdbcType=VARCHAR}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(InterfaceTestRecord record) {
        SQL sql = new SQL();
        sql.UPDATE("tb_ssgp_csgj_interface_test_record");
        
        if (record.getUcId() != null) {
            sql.SET("uc_id = #{ucId,jdbcType=VARCHAR}");
        }
        
        if (record.getOutValue() != null) {
            sql.SET("out_value = #{outValue,jdbcType=VARCHAR}");
        }
        
        if (record.getStartTime() != null) {
            sql.SET("start_time = #{startTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getEndTime() != null) {
            sql.SET("end_time = #{endTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getStatus() != null) {
            sql.SET("status = #{status,jdbcType=VARCHAR}");
        }

        if (record.getAssertValue() != null) {
            sql.SET("assert_value = #{assertValue,jdbcType=VARCHAR}");
        }

        sql.WHERE("id = #{id,jdbcType=VARCHAR}");
        
        return sql.toString();
    }
}