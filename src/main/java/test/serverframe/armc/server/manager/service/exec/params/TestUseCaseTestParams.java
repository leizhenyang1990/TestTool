package test.serverframe.armc.server.manager.service.exec.params;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 测试用例异步请求模型
 *
 * @author LeiZhenYang
 * @date 2019.01.03
 */
public class TestUseCaseTestParams extends InterfaceTestParams {

    /**
     * 集成测试测试用例ID
     */
    private String ucId;

    /**
     * 集成测试接口记录id
     */
    private String itirId;

    /**
     * 测试用例并发总数
     */
    private AtomicInteger concurrentTestUseCaseCallbackTotalCounter;

    /**
     * 测试用例并发总数
     */
    private AtomicInteger concurrentTestUseCaseSuccessTotalCounter;

    public TestUseCaseTestParams(InterfaceTestParams params) {
        super(params);
        setTestInterface(params.getTestInterface());
        setInterfaceTestTotalCount(params.getInterfaceTestTotalCount());
        setConcurrentInterfaceTestCallbackTotalCounter(params.getConcurrentInterfaceTestCallbackTotalCounter());
        setConcurrentInterfaceTestSuccessTotalCounter(params.getConcurrentInterfaceTestSuccessTotalCounter());
    }


    public String getUcId() {
        return ucId;
    }

    public void setUcId(String ucId) {
        this.ucId = ucId;
    }

    public String getItirId() {
        return itirId;
    }

    public void setItirId(String itirId) {
        this.itirId = itirId;
    }

    public AtomicInteger getConcurrentTestUseCaseCallbackTotalCounter() {
        return concurrentTestUseCaseCallbackTotalCounter;
    }

    public void setConcurrentTestUseCaseCallbackTotalCounter(AtomicInteger concurrentTestUseCaseCallbackTotalCounter) {
        this.concurrentTestUseCaseCallbackTotalCounter = concurrentTestUseCaseCallbackTotalCounter;
    }

    public AtomicInteger getConcurrentTestUseCaseSuccessTotalCounter() {
        return concurrentTestUseCaseSuccessTotalCounter;
    }

    public void setConcurrentTestUseCaseSuccessTotalCounter(AtomicInteger concurrentTestUseCaseSuccessTotalCounter) {
        this.concurrentTestUseCaseSuccessTotalCounter = concurrentTestUseCaseSuccessTotalCounter;
    }
}
