package test.serverframe.armc.server.manager.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.springframework.stereotype.Service;
import test.serverframe.armc.server.manager.model.*;
import test.serverframe.armc.server.util.FileUtil;

import java.util.List;

/**
 * @author LeiZhenYang
 * @date 2018.11.24
 */
@Service
public class CenterAPIService {

    /**
     * sId 服务Id
     * siId 实例Id
     * siaoId 类别Id
     * siaId 接口I'd
     */

    /**
     * 获取服务集合
     *
     * @param pageNum
     * @param pageSize
     * @param sName    服务名称
     * @return
     */
    public Page<CenterServer> listServer(int pageNum, int pageSize, String sName) {
        String json = FileUtil.readResource("server/list.json");
        return JSON.parseObject(json, new TypeReference<Page<CenterServer>>() {
        });
    }

    /**
     * 通过服务id获取实例集合
     *
     * @param sId 服务id
     * @return
     */
    public List<CenterInstance> listInstance(String sId) {
        String json = FileUtil.readResource("instance/list.json");
        // 不分页
        return JSON.parseArray(json, CenterInstance.class);
    }

    /**
     * 获取类别集合
     *
     * @param pageNum
     * @param pageSize
     * @param siId     实例id
     * @param siaoName 类别名称
     * @return
     */
    public Page<CenterOwner> listOwner(int pageNum, int pageSize, String siId, String siaoName) {
        String json = FileUtil.readResource("owner/list.json");
        return JSON.parseObject(json, new TypeReference<Page<CenterOwner>>() {
        });
    }

    /**
     * 获取接口集合
     *
     * @param siId
     * @param siaoId
     * @return
     */
    public Page<CenterInterface> listInterface(String siId, String siaoId) {
        String json = FileUtil.readResource("interface/list.json");
        return JSON.parseObject(json, new TypeReference<Page<CenterInterface>>() {
        });
    }

    /**
     * 获取接口
     *
     * @param siaId 接口id
     * @return
     */
    public CenterInterface getInterface(String siaId) {
        String json = FileUtil.readResource("interface/" + siaId + ".json");
        return JSON.parseObject(json, new TypeReference<CenterInterface>() {
        });
    }

	/**
	 * @author yegui  
	 * @date 2019年2月26日  
	 * @param pageNum
	 * @param pageSize
	 * @param sName
	 * @return
	 * Page<CenterServer>
	 */  
	public Page<CenterServer> listRPCServer(int pageNum, int pageSize, String sName) {
		String str = "";
		return null;
	}
}
