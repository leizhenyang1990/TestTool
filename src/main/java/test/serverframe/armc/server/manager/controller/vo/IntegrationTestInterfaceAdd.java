package test.serverframe.armc.server.manager.controller.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel
public class IntegrationTestInterfaceAdd {

    @ApiModelProperty(value = "集成测试ID",hidden = true)
    private String itId;

    @ApiModelProperty("接口ID")
    private String interfaceId;

    @ApiModelProperty("接口名称")
    private String interfaceName;

    @ApiModelProperty("测试用例集合")
    private List<IntegrationTestEnterUseCaseAssoAdd> assos;

    public String getItId() {
        return itId;
    }

    public void setItId(String itId) {
        this.itId = itId == null ? null : itId.trim();
    }

    public String getInterfaceId() {
        return interfaceId;
    }

    public void setInterfaceId(String interfaceId) {
        this.interfaceId = interfaceId == null ? null : interfaceId.trim();
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName == null ? null : interfaceName.trim();
    }

    public List<IntegrationTestEnterUseCaseAssoAdd> getAssos() {
        return assos;
    }

    public void setAssos(List<IntegrationTestEnterUseCaseAssoAdd> assos) {
        this.assos = assos;
    }
}
