package test.serverframe.armc.server.manager.domain;

import test.serverframe.armc.server.manager.controller.vo.IntegrationTestEnterUseCaseAssoAdd;

public class IntegrationTestInterfaceUseCaseAsso {
    private String itiId;

    private String tucId;

    private TestUseCase testUseCase;

    public String getItiId() {
        return itiId;
    }

    public void setItiId(String itiId) {
        this.itiId = itiId == null ? null : itiId.trim();
    }

    public String getTucId() {
        return tucId;
    }

    public void setTucId(String tucId) {
        this.tucId = tucId == null ? null : tucId.trim();
    }

    public TestUseCase getTestUseCase() {
        return testUseCase;
    }

    public void setTestUseCase(TestUseCase testUseCase) {
        this.testUseCase = testUseCase;
    }

    public IntegrationTestInterfaceUseCaseAsso() {

    }

    public IntegrationTestInterfaceUseCaseAsso(IntegrationTestEnterUseCaseAssoAdd record) {
        this.itiId = record.getItiId();
        this.tucId = record.getTucId();
    }
}
