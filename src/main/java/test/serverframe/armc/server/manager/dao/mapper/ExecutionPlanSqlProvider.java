package test.serverframe.armc.server.manager.dao.mapper;

import org.apache.ibatis.jdbc.SQL;
import test.serverframe.armc.server.manager.domain.ExecutionPlan;

public class ExecutionPlanSqlProvider {

    public String insertSelective(ExecutionPlan record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("tb_ssgp_execution_plan");
        
        if (record.getId() != null) {
            sql.VALUES("id", "#{id,jdbcType=VARCHAR}");
        }
        
        if (record.getTriggerId() != null) {
            sql.VALUES("trigger_id", "#{triggerId,jdbcType=VARCHAR}");
        }
        
        if (record.getExecuteCount() != null) {
            sql.VALUES("execute_count", "#{executeCount,jdbcType=INTEGER}");
        }
        
        if (record.getConcurrentCount() != null) {
            sql.VALUES("concurrent_count", "#{concurrentCount,jdbcType=INTEGER}");
        }
        
        return sql.toString();
    }

    public String updateByPrimaryKeySelective(ExecutionPlan record) {
        SQL sql = new SQL();
        sql.UPDATE("tb_ssgp_execution_plan");
        
        if (record.getTriggerId() != null) {
            sql.SET("trigger_id = #{triggerId,jdbcType=VARCHAR}");
        }
        
        if (record.getExecuteCount() != null) {
            sql.SET("execute_count = #{executeCount,jdbcType=INTEGER}");
        }
        
        if (record.getConcurrentCount() != null) {
            sql.SET("concurrent_count = #{concurrentCount,jdbcType=INTEGER}");
        }
        
        sql.WHERE("id = #{id,jdbcType=VARCHAR}");
        
        return sql.toString();
    }
}