package test.serverframe.armc.server.manager.controller.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import test.serverframe.armc.server.manager.domain.TestUseCaseRecord;

import java.util.Date;
import java.util.List;

/**
 * @author LeiZhenYang
 * @date 2018.11.30
 */
@ApiModel("集成测试分页模型")
public class IntegrationTestPage {

    @ApiModelProperty(value = "集成测试名称")
    private String id;

    @ApiModelProperty(value = "集成测试名称")
    private String name;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("修改时间")
    private Date updateTime;

    @ApiModelProperty("启用禁用状态")
    private  Integer status;

    /**
     * 最后一次测试的状态
     */
    @ApiModelProperty(value = "最后测试状态")
    private String lastTestStatus;

    /**
     * 最后一次测试的时间
     */
    @ApiModelProperty(value = "最后测试时间")
    private Date lastTestTime;

    /**
     * 最后一次测试的接口总数
     */
    @ApiModelProperty(value = "最后测试接口总数")
    private Integer lastTestInterfaceTotal;

    /**
     * 最后一次测试的接口成功总数
     */
    @ApiModelProperty(value = "最后测试接口成功总数")
    private Integer lastTestSuccessInterfaceTotal;

    @ApiModelProperty(value = "最后测试集成测试ID")
    private String lastTestRecordId;

    @ApiModelProperty("测试用例记录集合")
    List<TestUseCaseRecord> testUseCaseRecords;

    public List<TestUseCaseRecord> getTestUseCaseRecords() {
        return testUseCaseRecords;
    }

    public void setTestUseCaseRecords(List<TestUseCaseRecord> testUseCaseRecords) {
        this.testUseCaseRecords = testUseCaseRecords;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getLastTestStatus() {
        return lastTestStatus;
    }

    public void setLastTestStatus(String lastTestStatus) {
        this.lastTestStatus = lastTestStatus;
    }

    public Date getLastTestTime() {
        return lastTestTime;
    }

    public void setLastTestTime(Date lastTestTime) {
        this.lastTestTime = lastTestTime;
    }

    public Integer getLastTestInterfaceTotal() {
        return lastTestInterfaceTotal;
    }

    public void setLastTestInterfaceTotal(Integer lastTestInterfaceTotal) {
        this.lastTestInterfaceTotal = lastTestInterfaceTotal;
    }

    public Integer getLastTestSuccessInterfaceTotal() {
        return lastTestSuccessInterfaceTotal;
    }

    public void setLastTestSuccessInterfaceTotal(Integer lastTestSuccessInterfaceTotal) {
        this.lastTestSuccessInterfaceTotal = lastTestSuccessInterfaceTotal;
    }

    public String getLastTestRecordId() {
        return lastTestRecordId;
    }

    public void setLastTestRecordId(String lastTestRecordId) {
        this.lastTestRecordId = lastTestRecordId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
