package test.serverframe.armc.server.manager.controller.vo;

import io.swagger.annotations.ApiModel;

import java.util.Date;

@ApiModel("集成测试接口测试用例执行状态")
public class InterfaceTestUseCaseTestStatusVO {

    private String id;

    private String interfaceId;

    private String interfaceName;

    private String requestMethod;

    private String requestUrl;

    private String useCaseName;

    private String remark;

    private Date createTime;

    private Date updateTime;

    private String source;

    private String deleteFlag;

    private String inHeader;

    private String inBody;

    private String inQuery;

    private String outValue;

    private Date startTime;

    private Date endTime;

    private String status;

    private String assertValue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getInterfaceId() {
        return interfaceId;
    }

    public void setInterfaceId(String interfaceId) {
        this.interfaceId = interfaceId == null ? null : interfaceId.trim();
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName == null ? null : interfaceName.trim();
    }

    public String getUseCaseName() {
        return useCaseName;
    }

    public void setUseCaseName(String useCaseName) {
        this.useCaseName = useCaseName == null ? null : useCaseName.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source == null ? null : source.trim();
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag == null ? null : deleteFlag.trim();
    }

    public String getInHeader() {
        return inHeader;
    }

    public void setInHeader(String inHeader) {
        this.inHeader = inHeader == null ? null : inHeader.trim();
    }

    public String getInBody() {
        return inBody;
    }

    public void setInBody(String inBody) {
        this.inBody = inBody == null ? null : inBody.trim();
    }

    public String getInQuery() {
        return inQuery;
    }

    public void setInQuery(String inQuery) {
        this.inQuery = inQuery == null ? null : inQuery.trim();
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOutValue() {
        return outValue;
    }

    public void setOutValue(String outValue) {
        this.outValue = outValue;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getAssertValue() {
        return assertValue;
    }

    public void setAssertValue(String assertValue) {
        this.assertValue = assertValue;
    }
}
