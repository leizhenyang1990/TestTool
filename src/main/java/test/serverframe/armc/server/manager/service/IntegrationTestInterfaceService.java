package test.serverframe.armc.server.manager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import test.serverframe.armc.server.manager.controller.vo.IntegrationTestInterfaceAdd;
import test.serverframe.armc.server.manager.dao.mapper.IntegrationTestInterfaceMapper;
import test.serverframe.armc.server.manager.dao.mapper.IntegrationTestRecordMapper;
import test.serverframe.armc.server.manager.domain.IntegrationTestInterface;

import java.util.List;
import java.util.UUID;

/**
 * @author LeiZhenYang
 * @date 2018.12.05
 */
@Service
public class IntegrationTestInterfaceService extends BaseService<IntegrationTestInterface, String> {

    @Autowired
    private IntegrationTestInterfaceMapper mapper;

    @Override
    protected String getMapperName() {
        return IntegrationTestRecordMapper.class.getName();
    }

    @Autowired
    private IntegrationTestInterfaceUseCaseAssoService assoService;

    /**
     * 新增集成测试接口
     *
     * @param record
     * @return
     */
    @Transactional
    public boolean addIntegrationTestInterface(IntegrationTestInterfaceAdd record) {
        IntegrationTestInterface iti = new IntegrationTestInterface(record);
        String id = UUID.randomUUID().toString();
        iti.setId(id);
        insertSelective(iti);
        if (record.getAssos() != null) {
            record.getAssos().forEach(asso -> {
                asso.setItiId(id);
                assoService.addIntegrationTestEnterUseCaseAsso(asso);
            });
        }
        return true;
    }

    /**
     * 通过集成接口ID删除集成测试接口
     *
     * @param itId
     */
    public void deleteIntegrationTestInterfaceByItId(String itId) {
        List<IntegrationTestInterface> list = mapper.selectByItId(itId);
        if (list != null) {
            list.forEach(iti -> {
                assoService.deleteIntegrationTestEnterUseCaseAssoByItiId(iti.getId());
            });
        }
        mapper.deleteByiItId(itId);
    }
}
