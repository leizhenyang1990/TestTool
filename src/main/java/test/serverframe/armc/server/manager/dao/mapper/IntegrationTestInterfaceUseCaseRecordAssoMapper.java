package test.serverframe.armc.server.manager.dao.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import test.serverframe.armc.server.manager.domain.IntegrationTestInterfaceUseCaseRecordAsso;

public interface IntegrationTestInterfaceUseCaseRecordAssoMapper {
    @Insert({
            "insert into tb_ssgp_csgj_integration_test_interface_use_case_record_asso (itir_id, tuc_id, ",
            "itr_id)",
            "values (#{itirId,jdbcType=VARCHAR}, #{tucId,jdbcType=VARCHAR}, ",
            "#{itrId,jdbcType=VARCHAR})"
    })
    int insert(IntegrationTestInterfaceUseCaseRecordAsso record);

    @InsertProvider(type = IntegrationTestInterfaceUseCaseRecordAssoSqlProvider.class, method = "insertSelective")
    int insertSelective(IntegrationTestInterfaceUseCaseRecordAsso record);

}
