package test.serverframe.armc.server.manager.dao.mapper;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import test.serverframe.armc.server.manager.controller.params.IntegrationTestPageParams;
import test.serverframe.armc.server.manager.controller.vo.IntegrationTestRecordPage;
import test.serverframe.armc.server.manager.domain.IntegrationTestRecord;

import java.util.List;

public interface IntegrationTestRecordMapper {
    @Delete({
            "delete from tb_ssgp_csgj_integration_test_record",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String id);

    @Insert({
            "insert into tb_ssgp_csgj_integration_test_record (id, it_id, ",
            "status, start_time, ",
            "end_time, total, ",
            "success_total, concurrent_count)",
            "values (#{id,jdbcType=VARCHAR}, #{itId,jdbcType=VARCHAR}, ",
            "#{status,jdbcType=VARCHAR}, #{startTime,jdbcType=TIMESTAMP}, ",
            "#{endTime,jdbcType=TIMESTAMP}, #{total,jdbcType=INTEGER}, ",
            "#{successTotal,jdbcType=INTEGER}, #{concurrentCount,jdbcType=INTEGER})"
    })
    int insert(IntegrationTestRecord record);

    @InsertProvider(type = IntegrationTestRecordSqlProvider.class, method = "insertSelective")
    int insertSelective(IntegrationTestRecord record);

    @Select({
            "select",
            "id, it_id, status, start_time, end_time, total, success_total, concurrent_count",
            "from tb_ssgp_csgj_integration_test_record",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "it_id", property = "itId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "status", property = "status", jdbcType = JdbcType.VARCHAR),
            @Result(column = "start_time", property = "startTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "end_time", property = "endTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "total", property = "total", jdbcType = JdbcType.INTEGER),
            @Result(column = "success_total", property = "successTotal", jdbcType = JdbcType.INTEGER),
            @Result(column = "concurrent_count", property = "concurrentCount", jdbcType = JdbcType.INTEGER)
    })
    IntegrationTestRecord selectByPrimaryKey(String id);

    @UpdateProvider(type = IntegrationTestRecordSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(IntegrationTestRecord record);

    @Update({
            "update tb_ssgp_csgj_integration_test_record",
            "set it_id = #{itId,jdbcType=VARCHAR},",
            "status = #{status,jdbcType=VARCHAR},",
            "start_time = #{startTime,jdbcType=TIMESTAMP},",
            "end_time = #{endTime,jdbcType=TIMESTAMP},",
            "total = #{total,jdbcType=INTEGER},",
            "success_total = #{successTotal,jdbcType=INTEGER}",
            "concurrent_count = #{concurrentCount,jdbcType=INTEGER}",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(IntegrationTestRecord record);

    @SelectProvider(type = IntegrationTestRecordSqlProvider.class, method = "selectByPage")
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "it_id", property = "itId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "status", property = "status", jdbcType = JdbcType.VARCHAR),
            @Result(column = "start_time", property = "startTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "end_time", property = "endTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "total", property = "total", jdbcType = JdbcType.INTEGER),
            @Result(column = "success_total", property = "successTotal", jdbcType = JdbcType.INTEGER),
            @Result(column = "concurrent_count", property = "concurrentCount", jdbcType = JdbcType.INTEGER),
            @Result(column = "id", property = "counter", one = @One(
                    select = "test.serverframe.armc.server.manager.dao.mapper.IntegrationTestInterfaceRecordMapper.countByItrId"
            ))
    })
    List<IntegrationTestRecordPage> selectByPage(IntegrationTestPageParams params);

    @Select({"select count(*) from tb_ssgp_csgj_integration_test_record where it_id = #{itId,jdbcType=VARCHAR}"})
    int countByItId(String itId);
}
