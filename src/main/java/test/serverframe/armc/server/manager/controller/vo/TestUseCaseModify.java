package test.serverframe.armc.server.manager.controller.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author LeiZhenYang
 * @date 2018.12.02
 */
@ApiModel("测试用例修改模型")
public class TestUseCaseModify extends TestUseCaseAdd {

    @ApiModelProperty(value = "用例测试主键", required = true)
    private String id;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
