package test.serverframe.armc.server.manager.dao.mapper;

import org.apache.ibatis.jdbc.SQL;
import test.serverframe.armc.server.manager.domain.IntegrationTestInterfaceUseCaseAsso;

public class IntegrationTestInterfaceUseCaseAssoSqlProvider {

    public String insertSelective(IntegrationTestInterfaceUseCaseAsso record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("tb_ssgp_csgj_integration_test_interface_use_case_asso");

        if (record.getItiId() != null) {
            sql.VALUES("iti_id", "#{itiId,jdbcType=VARCHAR}");
        }

        if (record.getTucId() != null) {
            sql.VALUES("tuc_id", "#{tucId,jdbcType=VARCHAR}");
        }

        return sql.toString();
    }
}
