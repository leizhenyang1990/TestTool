package test.serverframe.armc.server.manager.controller;

import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import test.serverframe.armc.server.manager.common.ResultDtoUtil;
import test.serverframe.armc.server.manager.common.exception.ExceptionHandle;
import test.serverframe.armc.server.manager.controller.params.TestUseCasePageParams;
import test.serverframe.armc.server.manager.controller.vo.TestUseCaseAdd;
import test.serverframe.armc.server.manager.controller.vo.TestUseCaseModify;
import test.serverframe.armc.server.manager.domain.TestUseCase;
import test.serverframe.armc.server.manager.dto.ResultDto;
import test.serverframe.armc.server.manager.service.TestUseCaseService;

import javax.validation.Valid;
import java.util.List;

/**
 * 测试用例
 *
 * @author LeiZhenYang
 * @date 2018.11.29
 */
@RestController
@RequestMapping("test/use/case")
@CrossOrigin(origins = "*")
@Api("用例测试")
public class TestUseCaseController extends BaseController {

    /**
     * 用例测试服务
     */
    @Autowired
    private TestUseCaseService service;

    /**
     * 任务分页自定义异常
     */
    @Autowired
    private ExceptionHandle<PageInfo<TestUseCase>> exceptionHandle;

    /**
     * 测试用例分页
     *
     * @param data
     * @return
     */
    @GetMapping("/page")
    @ApiOperation(notes = "测试用例分页", value = "测试用例分页")
    public ResultDto<PageInfo<TestUseCase>> page(@ModelAttribute @Valid TestUseCasePageParams data) {
        ResultDto<PageInfo<TestUseCase>> result;
        try {
            PageInfo<TestUseCase> mbrValues = service.pageFind(data.getPageNum(), data.getPageSize(), data);
            result = ResultDtoUtil.success(mbrValues);
        } catch (Exception ex) {
            result = exceptionHandle.exceptionGet(ex);
        }
        return result;
    }

    /**
     * 批量删除测试用例
     *
     * @param ids
     * @return
     */
    @DeleteMapping("/delete")
    @ApiOperation(notes = "批量删除测试用例", value = "批量删除测试用例")
    public ResultDto<PageInfo<TestUseCase>> delete(@ApiParam("测试用例ID集合") @RequestBody List<String> ids) {
        if (ids != null) {
            try {
                ids.forEach(id -> {
                    TestUseCase testUseCase = new TestUseCase();
                    testUseCase.setId(id);
                    testUseCase.setDeleteFlag(TestUseCase.DeleteFlag.True);
                    service.updateSelective(testUseCase);
                });
                return ResultDtoUtil.success();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return ResultDtoUtil.error("批量删除失败");
    }

    /**
     * 新增测试用例
     *
     * @param model
     * @return
     */
    @PostMapping("/add")
    @ApiOperation(notes = "新增测试用例", value = "新增测试用例")
    public ResultDto<TestUseCase> add(@ApiParam("用例测试") @RequestBody @Valid TestUseCaseAdd model) {
        try {
            TestUseCase testUseCase = new TestUseCase(model);
            testUseCase.setSource(TestUseCase.Source.TestUseCase);
            testUseCase.setUseCaseName(model.getUseCaseName());
            testUseCase.setRemark(model.getRemark());
            int i = service.insertSelective(testUseCase);
            if (i > 0) {
                return ResultDtoUtil.success();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResultDtoUtil.error(ex.getMessage());
        }
        return ResultDtoUtil.error("新增失败");

    }

    /**
     * 修改测试用例
     *
     * @param model
     * @return
     */
    @PutMapping("/modify")
    @ApiOperation(notes = "修改测试用例", value = "修改测试用例")
    public ResultDto<TestUseCase> modify(@ApiParam("用例测试") @RequestBody @Valid TestUseCaseModify model) {
        if (model != null) {
            try {
                TestUseCase testUseCase = new TestUseCase(model);
                testUseCase.setId(model.getId());
                testUseCase.setSource(TestUseCase.Source.TestUseCase);
                testUseCase.setUseCaseName(model.getUseCaseName());
                testUseCase.setRemark(model.getRemark());
                int i = service.updateByPrimaryKeySelective(testUseCase);
                if (i > 0) {
                    return ResultDtoUtil.success();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                return ResultDtoUtil.error(ex.getMessage());
            }
        }
        return ResultDtoUtil.error("修改失败");

    }

    @GetMapping("/{id}")
    @ApiModelProperty(notes = "根据ID获取测试用例", value = "根据ID获取测试用例")
    public ResultDto<TestUseCase> get(@ApiParam("测试用例ID") @PathVariable String id) {
        return ResultDtoUtil.success(service.selectByPrimaryKey(id));
    }

}
