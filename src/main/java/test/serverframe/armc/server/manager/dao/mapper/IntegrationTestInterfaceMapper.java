package test.serverframe.armc.server.manager.dao.mapper;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import test.serverframe.armc.server.manager.domain.IntegrationTestInterface;

import java.util.List;

public interface IntegrationTestInterfaceMapper {
    @Delete({
            "delete from tb_ssgp_csgj_integration_test_interface",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String id);

    @Insert({
            "insert into tb_ssgp_csgj_integration_test_interface (id, it_id, ",
            "interface_id, interface_name)",
            "values (#{id,jdbcType=VARCHAR}, #{itId,jdbcType=VARCHAR}, ",
            "#{interfaceId,jdbcType=VARCHAR}, #{interfaceName,jdbcType=VARCHAR})"
    })
    int insert(IntegrationTestInterface record);

    @InsertProvider(type = IntegrationTestInterfaceSqlProvider.class, method = "insertSelective")
    int insertSelective(IntegrationTestInterface record);

    @Select({
            "select",
            "id, it_id, interface_id, interface_name",
            "from tb_ssgp_csgj_integration_test_interface",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "it_id", property = "itId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "interface_id", property = "interfaceId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "interface_name", property = "interfaceName", jdbcType = JdbcType.VARCHAR)
    })
    IntegrationTestInterface selectByPrimaryKey(String id);

    @UpdateProvider(type = IntegrationTestInterfaceSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(IntegrationTestInterface record);

    @Update({
            "update tb_ssgp_csgj_integration_test_interface",
            "set it_id = #{itId,jdbcType=VARCHAR},",
            "interface_id = #{interfaceId,jdbcType=VARCHAR},",
            "interface_name = #{interfaceName,jdbcType=VARCHAR}",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(IntegrationTestInterface record);

    @Delete({
            "delete from tb_ssgp_csgj_integration_test_interface where it_id = #{itId,jdbcType=VARCHAR}"
    })
    int deleteByiItId(String itId);

    @Select({
            "select",
            "id, it_id, interface_id, interface_name",
            "from tb_ssgp_csgj_integration_test_interface",
            "where it_id = #{itId,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "it_id", property = "itId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "interface_id", property = "interfaceId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "interface_name", property = "interfaceName", jdbcType = JdbcType.VARCHAR),
            @Result(column = "id", property = "assos", many = @Many(
                    select = "test.serverframe.armc.server.manager.dao.mapper.IntegrationTestInterfaceUseCaseAssoMapper.selectByItiId"
            ))
    })
    List<IntegrationTestInterface> selectByItId(String itId);

}
