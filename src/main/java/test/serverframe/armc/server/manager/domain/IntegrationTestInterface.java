package test.serverframe.armc.server.manager.domain;

import test.serverframe.armc.server.manager.controller.vo.IntegrationTestInterfaceAdd;

import java.util.List;

public class IntegrationTestInterface {
    private String id;

    private String itId;

    private String interfaceId;

    private String interfaceName;

    private List<IntegrationTestInterfaceUseCaseAsso> assos;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getItId() {
        return itId;
    }

    public void setItId(String itId) {
        this.itId = itId == null ? null : itId.trim();
    }

    public String getInterfaceId() {
        return interfaceId;
    }

    public void setInterfaceId(String interfaceId) {
        this.interfaceId = interfaceId == null ? null : interfaceId.trim();
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName == null ? null : interfaceName.trim();
    }

    public List<IntegrationTestInterfaceUseCaseAsso> getAssos() {
        return assos;
    }

    public void setAssos(List<IntegrationTestInterfaceUseCaseAsso> assos) {
        this.assos = assos;
    }

    public IntegrationTestInterface() {

    }

    public IntegrationTestInterface(IntegrationTestInterfaceAdd record) {
        this.itId = record.getItId();
        this.interfaceId = record.getInterfaceId();
        this.interfaceName = record.getInterfaceName();
    }


}
