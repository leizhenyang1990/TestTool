package test.serverframe.armc.server.manager.controller.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * @author LeiZhenYang
 * @date 2018.11.29
 */
@ApiModel("集成测试记录分页查询条件")
public class IntegrationTestRecordPageParams extends PageParams {

    @ApiModelProperty(value = "集成测试ID", required = true)
    private String itId;
    @ApiModelProperty(value = "集成测试历史数据时间段:开始时间")
    private Date startTime;
    @ApiModelProperty(value = "集成测试历史数据时间段:结束时间")
    private Date endTime;
    @ApiModelProperty(value = "今日 = true 历史 = false 全部 = null", hidden = true)
    private Boolean flag;
    @ApiModelProperty(value = "集成测试历史数据记录天数")
    private Integer recordDay;

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @ApiModelProperty(hidden = true)


    public String getItId() {
        return itId;
    }

    public void setItId(String itId) {
        this.itId = itId;
    }

	public Integer getRecordDay() {
		return recordDay;
	}

	public void setRecordDay(Integer recordDay) {
		this.recordDay = recordDay;
	}

}
