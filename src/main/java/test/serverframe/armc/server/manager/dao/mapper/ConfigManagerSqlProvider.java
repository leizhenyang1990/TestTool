package test.serverframe.armc.server.manager.dao.mapper;

import org.apache.ibatis.jdbc.SQL;

import test.serverframe.armc.server.manager.controller.vo.ConfigManagerVo;
import test.serverframe.armc.server.manager.domain.ConfigManager;

public class ConfigManagerSqlProvider {

    public String insertSelective(ConfigManager record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("tb_ssgp_config");
        
        if (record.getId() != null) {
        	sql.VALUES("id", "#{id,jdbcType=VARCHAR}");
        }
        
        if (record.getSystemName() != null) {
            sql.VALUES("system_name", "#{systemName,jdbcType=VARCHAR}");
        }
        
        if (record.getHttpConnectTimeout() != null) {
            sql.VALUES("http_connect_timeout", "#{httpConnectTimeout,jdbcType=INTEGER}");
        }
        
        if (record.getHttpReadTimeout() != null) {
            sql.VALUES("http_read_timeout", "#{httpReadTimeout,jdbcType=INTEGER}");
        }
        
        if (record.getRecordTime() != null) {
            sql.VALUES("record_time", "#{recordTime,jdbcType=INTEGER}");
        }
        
        if (record.getHttpConnectionRequestTimeout() != null) {
        	sql.VALUES("http_connection_request_timeout", "#{httpConnectionRequestTimeout,jdbcType=INTEGER}");
        }
        
        return sql.toString();
    }
    
    public String updateSelective(ConfigManagerVo record) {
        SQL sql = new SQL();
        sql.UPDATE("tb_ssgp_config");
        
        if (record.getSystemName() != null) {
            sql.SET("system_name = #{systemName,jdbcType=VARCHAR}");
        }
        
        if (record.getHttpConnectTimeout() != null) {
            sql.SET("http_connect_timeout = #{httpConnectTimeout,jdbcType=INTEGER}");
        }
        
        if (record.getHttpReadTimeout() != null) {
            sql.SET("http_read_timeout = #{httpReadTimeout,jdbcType=INTEGER}");
        }
        
        if (record.getRecordTime() != null) {
            sql.SET("record_time = #{recordTime,jdbcType=INTEGER}");
        }
        
        if (record.getHttpConnectionRequestTimeout() != null) {
        	sql.SET("http_connection_request_timeout = #{httpConnectionRequestTimeout,jdbcType=INTEGER}");
        }
        
        sql.WHERE("id = #{id,jdbcType=VARCHAR}");
        
        return sql.toString();
    }
}