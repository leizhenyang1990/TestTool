package test.serverframe.armc.server.manager.domain.extend;

/**
 * 用于关联的Count
 *
 * @author LiuChunfu
 * @date 2019.01.14
 */
public class RecordCounter {

    /**
     * 集成测试记录id
     */
    private String itrId;

    /**
     * 接口总数
     */
    private int interfaceTotal;

    /**
     * 测试用例总数
     */
    private int testUseCaseTotal;

    public String getItrId() {
        return itrId;
    }

    public void setItrId(String itrId) {
        this.itrId = itrId;
    }

    public int getInterfaceTotal() {
        return interfaceTotal;
    }

    public void setInterfaceTotal(int interfaceTotal) {
        this.interfaceTotal = interfaceTotal;
    }

    public int getTestUseCaseTotal() {
        return testUseCaseTotal;
    }

    public void setTestUseCaseTotal(int testUseCaseTotal) {
        this.testUseCaseTotal = testUseCaseTotal;
    }
}
