package test.serverframe.armc.server.manager.model;

import java.io.Serializable;

/**
 * 注册中心服务
 *
 * @author LeiZhenYang
 * @date 2018.11.24
 */
public class CenterServer implements Serializable {
    /**
     * 服务id
     */
    private String sid;
    /**
     * 服务名称
     */
    private String sName;
    /**
     * 服务中文名
     */
    private String sChinaName;
    private String sModelType;
    private String sStatus;

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public String getsChinaName() {
        return sChinaName;
    }

    public void setsChinaName(String sChinaName) {
        this.sChinaName = sChinaName;
    }

    public String getsModelType() {
        return sModelType;
    }

    public void setsModelType(String sModelType) {
        this.sModelType = sModelType;
    }

    public String getsStatus() {
        return sStatus;
    }

    public void setsStatus(String sStatus) {
        this.sStatus = sStatus;
    }

}

