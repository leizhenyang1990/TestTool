package test.serverframe.armc.server.manager.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import test.serverframe.armc.server.manager.model.*;
import test.serverframe.armc.server.manager.service.CenterAPIService;

import java.util.List;

/**
 * @author LeiZhenYang
 * @date 2018.11.24
 */
@RestController
@RequestMapping("center")
@CrossOrigin(origins = "*")
@Api("注册中心服务")
public class CenterAPIController {

    @Autowired
    private CenterAPIService service;

    /**
     * 获取服务集合
     *
     * @param pageNum
     * @param pageSize
     * @param sName    服务名称
     * @return
     */
    @GetMapping("servers")
    @ApiOperation(notes = "获取服务集合", value = "获取服务集合")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "int", name = "pageNum", value = "页码", required = true),
            @ApiImplicitParam(paramType = "query", dataType = "int", name = "pageSize", value = "分页大小", required = true),
            @ApiImplicitParam(paramType = "query", dataType = "String", name = "sName", value = "服务名称")
    })
    public Page<CenterServer> listServer(int pageNum, int pageSize, String sName) {
        return service.listServer(pageNum, pageSize, sName);
    }

    /**
     * 通过服务id获取实例集合
     *
     * @param sId 服务id
     * @return
     */
    @GetMapping("instances")
    @ApiOperation(notes = "获取实例集合", value = "获取实例集合")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "String", name = "sId", value = "服务id", required = true)
    })
    public List<CenterInstance> listInstance(String sId) {
        return service.listInstance(sId);
    }

    /**
     * @param pageNum
     * @param pageSize
     * @param siId     实例id
     * @param siaoName 类别名称
     * @return
     */
    @GetMapping("owners")
    @ApiOperation(notes = "获取类别集合", value = "获取类别集合")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "int", name = "pageNum", value = "页码", required = true),
            @ApiImplicitParam(paramType = "query", dataType = "int", name = "pageSize", value = "分页大小", required = true),
            @ApiImplicitParam(paramType = "query", dataType = "String", name = "siId", value = "实例ID", required = true),
            @ApiImplicitParam(paramType = "query", dataType = "String", name = "siaoName", value = "类别名称")
    })
    public Page<CenterOwner> listOwner(int pageNum, int pageSize, String siId, String siaoName) {
        return service.listOwner(pageNum, pageSize, siId, siaoName);
    }

    /**
     * 获取接口集合
     *
     * @param siId
     * @param siaoId
     * @return
     */
    @GetMapping("interfaces")
    @ApiOperation(notes = "获取接口集合", value = "获取接口集合")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "String", name = "siId", value = "实例ID", required = true),
            @ApiImplicitParam(paramType = "query", dataType = "String", name = "siaoId", value = "类别ID")
    })
    public Page<CenterInterface> listInterface(String siId, String siaoId) {
        return service.listInterface(siId, siaoId);
    }

    /**
     * 获取接口
     *
     * @param siaId 接口id
     * @return
     */
    @GetMapping("interface")
    @ApiOperation(notes = "根据接口id获取接口", value = "根据接口id获取接口")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "String", name = "siaId", value = "接口ID", required = true),
    })
    public CenterInterface getInterface(String siaId) {
        return service.getInterface(siaId);
    }
    
    
    @GetMapping("RPC-servers")
    @ApiOperation(notes = "获取RPC服务集合", value = "获取RPC服务集合")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", dataType = "int", name = "pageNum", value = "页码", required = true),
            @ApiImplicitParam(paramType = "query", dataType = "int", name = "pageSize", value = "分页大小", required = true),
            @ApiImplicitParam(paramType = "query", dataType = "String", name = "sName", value = "RPC服务名称")
    })
    public Page<CenterServer> listRPCServer(int pageNum, int pageSize, String sName) {
        return service.listRPCServer(pageNum, pageSize, sName);
    }
}
