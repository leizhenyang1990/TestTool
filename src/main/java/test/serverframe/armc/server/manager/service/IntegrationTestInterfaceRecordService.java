package test.serverframe.armc.server.manager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import test.serverframe.armc.server.manager.dao.mapper.IntegrationTestInterfaceRecordMapper;
import test.serverframe.armc.server.manager.dao.mapper.IntegrationTestRecordMapper;
import test.serverframe.armc.server.manager.domain.IntegrationTestInterfaceRecord;

import java.util.List;

/**
 * @author LeiZhenYang
 * @date 2018.12.05
 */
@Service
public class IntegrationTestInterfaceRecordService extends BaseService<IntegrationTestInterfaceRecord, String> {

    @Autowired
    private IntegrationTestInterfaceRecordMapper mapper;

    @Override
    protected String getMapperName() {
        return IntegrationTestRecordMapper.class.getName();
    }

    public List<IntegrationTestInterfaceRecord> getRecordByItrId(String itrId) {
        return mapper.selectByItrId(itrId);
    }
}
