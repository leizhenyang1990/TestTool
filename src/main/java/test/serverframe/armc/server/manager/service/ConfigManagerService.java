package test.serverframe.armc.server.manager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import test.serverframe.armc.server.manager.controller.vo.ConfigManagerVo;
import test.serverframe.armc.server.manager.dao.mapper.ConfigManagerMapper;
import test.serverframe.armc.server.manager.domain.ConfigManager;

/**  
* 
* @author yegui  
* @date 2019年2月27日  
*/
@Service
public class ConfigManagerService {
	
	@Autowired
	private ConfigManagerMapper configManagerMapper;
	
	public int updateConfig(ConfigManagerVo configManager) {
		return configManagerMapper.updateSelective(configManager);
	}
	
	public ConfigManager selectConfig() {
		return configManagerMapper.selectConfig();
	}
}
