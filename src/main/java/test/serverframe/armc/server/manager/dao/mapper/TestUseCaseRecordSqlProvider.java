package test.serverframe.armc.server.manager.dao.mapper;

import org.apache.ibatis.jdbc.SQL;
import test.serverframe.armc.server.manager.domain.TestUseCaseRecord;

public class TestUseCaseRecordSqlProvider {

    public String insertSelective(TestUseCaseRecord record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("tb_ssgp_csgj_test_use_case_record");
        
        if (record.getId() != null) {
            sql.VALUES("id", "#{id,jdbcType=VARCHAR}");
        }
        
        if (record.getItirId() != null) {
            sql.VALUES("itir_id", "#{itirId,jdbcType=VARCHAR}");
        }
        
        if (record.getStatus() != null) {
            sql.VALUES("status", "#{status,jdbcType=VARCHAR}");
        }
        
        if (record.getStartTime() != null) {
            sql.VALUES("start_time", "#{startTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getEndTime() != null) {
            sql.VALUES("end_time", "#{endTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getTotal() != null) {
            sql.VALUES("total", "#{total,jdbcType=INTEGER}");
        }
        
        if (record.getSuccessTotal() != null) {
            sql.VALUES("success_total", "#{successTotal,jdbcType=INTEGER}");
        }
        if (record.getTestUseCaseName() != null) {
            sql.VALUES("test_use_case_name", "#{testUseCaseName,jdbcType=VARCHAR}");
        }
        if (record.getTestUseCaseId() != null) {
            sql.VALUES("test_use_case_id", "#{testUseCaseId,jdbcType=VARCHAR}");
        }
        
        return sql.toString();
    }

    public String updateByPrimaryKeySelective(TestUseCaseRecord record) {
        SQL sql = new SQL();
        sql.UPDATE("tb_ssgp_csgj_test_use_case_record");
        
        if (record.getItirId() != null) {
            sql.SET("itir_id = #{itirId,jdbcType=VARCHAR}");
        }
        
        if (record.getStatus() != null) {
            sql.SET("status = #{status,jdbcType=VARCHAR}");
        }
        
        if (record.getStartTime() != null) {
            sql.SET("start_time = #{startTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getEndTime() != null) {
            sql.SET("end_time = #{endTime,jdbcType=TIMESTAMP}");
        }
        
        if (record.getTotal() != null) {
            sql.SET("total = #{total,jdbcType=INTEGER}");
        }
        
        if (record.getSuccessTotal() != null) {
            sql.SET("success_total = #{successTotal,jdbcType=INTEGER}");
        }
        if (record.getTestUseCaseName() != null) {
            sql.SET("test_use_case_name = #{testUseCaseName,jdbcType=VARCHAR}");
        }
        if (record.getTestUseCaseId() != null) {
            sql.SET("test_use_case_id = #{testUseCaseId,jdbcType=VARCHAR}");
        }
        
        sql.WHERE("id = #{id,jdbcType=VARCHAR}");
        
        return sql.toString();
    }
}