package test.serverframe.armc.server.manager.dao.mapper;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.jdbc.SQL;
import org.ho.yaml.Yaml;
import test.serverframe.armc.server.manager.controller.ConfigManagerController;
import test.serverframe.armc.server.manager.controller.params.ConfigManager;
import test.serverframe.armc.server.manager.domain.IntegrationTestRecord;
import test.serverframe.armc.server.util.DateUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.Date;
import java.util.Map;

public class IntegrationTestRecordSqlProvider {

    public String insertSelective(IntegrationTestRecord record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("tb_ssgp_csgj_integration_test_record");

        if (record.getId() != null) {
            sql.VALUES("id", "#{id,jdbcType=VARCHAR}");
        }

        if (record.getItId() != null) {
            sql.VALUES("it_id", "#{itId,jdbcType=VARCHAR}");
        }

        if (record.getStatus() != null) {
            sql.VALUES("status", "#{status,jdbcType=VARCHAR}");
        }

        if (record.getStartTime() != null) {
            sql.VALUES("start_time", "#{startTime,jdbcType=TIMESTAMP}");
        }

        if (record.getEndTime() != null) {
            sql.VALUES("end_time", "#{endTime,jdbcType=TIMESTAMP}");
        }

        if (record.getTotal() != null) {
            sql.VALUES("total", "#{total,jdbcType=INTEGER}");
        }

        if (record.getSuccessTotal() != null) {
            sql.VALUES("success_total", "#{successTotal,jdbcType=INTEGER}");
        }

        if (record.getConcurrentCount() != null) {
            sql.VALUES("concurrent_count", "#{concurrentCount,jdbcType=INTEGER}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(IntegrationTestRecord record) {
        SQL sql = new SQL();
        sql.UPDATE("tb_ssgp_csgj_integration_test_record");

        if (record.getItId() != null) {
            sql.SET("it_id = #{itId,jdbcType=VARCHAR}");
        }

        if (record.getStatus() != null) {
            sql.SET("status = #{status,jdbcType=VARCHAR}");
        }

        if (record.getStartTime() != null) {
            sql.SET("start_time = #{startTime,jdbcType=TIMESTAMP}");
        }

        if (record.getEndTime() != null) {
            sql.SET("end_time = #{endTime,jdbcType=TIMESTAMP}");
        }

        if (record.getTotal() != null) {
            sql.SET("total = #{total,jdbcType=INTEGER}");
        }

        if (record.getSuccessTotal() != null) {
            sql.SET("success_total = #{successTotal,jdbcType=INTEGER}");
        }

        if (record.getConcurrentCount() != null) {
            sql.SET("concurrent_count = #{concurrentCount,jdbcType=INTEGER}");
        }

        sql.WHERE("id = #{id,jdbcType=VARCHAR}");

        return sql.toString();
    }


    public String selectByPage(Map<String, Object> params) {
        StringBuilder sql = new StringBuilder();
        //获取配置文件的信息
        /*URL resource = ConfigManagerController.class.getClassLoader().getResource("config-manager.yml");
        File dumpFile = new File(resource.getPath());
        ConfigManager configManager = null;
        Date startTime = null;
        Date endTime = null;
        try {
            configManager = (ConfigManager) Yaml.loadType(dumpFile, ConfigManager.class);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (configManager != null) {
            Integer day = configManager.getTime();
            startTime = DateUtil.getDateBefore(new Date(), day);
            endTime = new Date();
        }*/
        Date startTime = null;
        Date endTime = null;
        if (params.get("recordDay") != null) {
            Object day = params.get("recordDay");
            startTime = DateUtil.getDateBefore(new Date(), (Integer) day);
            endTime = new Date();
        }
        sql.append("select * from tb_ssgp_csgj_integration_test_record where 1 = 1");
        if (params.get("startTime") != null && params.get("endTime") != null) {
            sql.append(" and start_time>='" + DateUtil.formatDate((Date) params.get("startTime")) + "'"
                    + " and start_time<='" + DateUtil.formatDate((Date) params.get("endTime")) + "'");
        } else {
            if (startTime != null && endTime != null) {
                sql.append(" and start_time>='" + DateUtil.formatDate(startTime) + "'"
                        + " and start_time<='" + DateUtil.formatDate(endTime) + "'");
            }
        }
        Object flag = params.get("flag");
        if (flag != null) {
            if (((boolean) flag)) {
                sql.append(" and date(start_time) = curdate()");
            } else {
                sql.append(" and DATE_FORMAT( start_time,'%Y-%m-%d') < DATE_FORMAT(NOW(), '%Y-%m-%d')");
            }
        }
        Object name = params.get("itId");
        if (name != null && StringUtils.isNotBlank(name.toString())) {
            sql.append(" and it_id='" + name.toString() + "'");
        }
        sql.append(" order by ");
        sql.append(" start_time DESC");
        return sql.toString();
    }
}
