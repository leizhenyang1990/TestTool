package test.serverframe.armc.server.manager.controller.vo;

import com.microcore.entity.AssertCondition;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;


/**
 * @author LeiZhenYang
 * @date 2018.12.02
 */
@ApiModel("接口测试模型")
public class TestUseCaseCallInterface {

    @ApiModelProperty("接口ID")
    private String interfaceId;

    @ApiModelProperty("接口名称")
    private String interfaceName;

    @ApiModelProperty("请求类型")
    private String requestMethod;

    @ApiModelProperty("请求地址")
    private String requestUrl;

    @ApiModelProperty("Header入参")
    private String inHeader;

    @ApiModelProperty("Body入参")
    private String inBody;

    @ApiModelProperty("Query入参")
    private String inQuery;

    @ApiModelProperty(value = "断言条件")
    private List<AssertCondition> assertConditions;

    public List<AssertCondition> getAssertConditions() {
        return assertConditions;
    }

    public void setAssertConditions(List<AssertCondition> assertConditions) {
        this.assertConditions = assertConditions;
    }

    public String getInterfaceId() {
        return interfaceId;
    }

    public void setInterfaceId(String interfaceId) {
        this.interfaceId = interfaceId;
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getRequestMethod() {
        return requestMethod;
    }

    public void setRequestMethod(String requestMethod) {
        this.requestMethod = requestMethod;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getInHeader() {
        return inHeader;
    }

    public void setInHeader(String inHeader) {
        this.inHeader = inHeader;
    }

    public String getInBody() {
        return inBody;
    }

    public void setInBody(String inBody) {
        this.inBody = inBody;
    }

    public String getInQuery() {
        return inQuery;
    }

    public void setInQuery(String inQuery) {
        this.inQuery = inQuery;
    }

}
