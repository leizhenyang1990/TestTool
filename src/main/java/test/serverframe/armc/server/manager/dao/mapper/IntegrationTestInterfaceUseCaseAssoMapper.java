package test.serverframe.armc.server.manager.dao.mapper;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import test.serverframe.armc.server.manager.domain.IntegrationTestInterfaceUseCaseAsso;

public interface IntegrationTestInterfaceUseCaseAssoMapper {
    @Insert({
            "insert into tb_ssgp_csgj_integration_test_interface_use_case_asso (iti_id, tuc_id)",
            "values (#{itiId,jdbcType=VARCHAR}, #{tucId,jdbcType=VARCHAR})"
    })
    int insert(IntegrationTestInterfaceUseCaseAsso record);

    @InsertProvider(type = IntegrationTestInterfaceUseCaseAssoSqlProvider.class, method = "insertSelective")
    int insertSelective(IntegrationTestInterfaceUseCaseAsso record);

    @Delete({
            "delete from tb_ssgp_csgj_integration_test_interface_use_case_asso where iti_id = #{itiId,jdbcType=VARCHAR}"
    })
    int deleteByItiId(String idiId);

    @Select({
            "select * from tb_ssgp_csgj_integration_test_interface_use_case_asso",
            "where iti_id = #{itiId,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column = "iti_id", property = "itiId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "tuc_id", property = "tucId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "tuc_id", property = "testUseCase", one = @One(
                    select = "test.serverframe.armc.server.manager.dao.mapper.TestUseCaseMapper.selectByPrimaryKey"
            ))
    })
    IntegrationTestInterfaceUseCaseAsso selectByItiId(String itiId);
}
