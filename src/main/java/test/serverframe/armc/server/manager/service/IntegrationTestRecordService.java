package test.serverframe.armc.server.manager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import test.serverframe.armc.server.manager.dao.mapper.IntegrationTestRecordMapper;
import test.serverframe.armc.server.manager.domain.IntegrationTestRecord;

import java.util.HashMap;
import java.util.Map;

/**
 * 集成测试
 *
 * @author LeiZhenYang
 * @date 2018.11.29
 */
@Service
public class IntegrationTestRecordService extends BaseService<IntegrationTestRecord, String> {

    @Autowired
    private IntegrationTestRecordMapper mapper;

    @Override
    protected String getMapperName() {
        return IntegrationTestRecordMapper.class.getName();
    }

    public Integer countByItId(String itId) {
        return mapper.countByItId(itId);
    }

    /**
     * 获取集成测试状态列表
     *
     * @return
     */
    public Map<Integer, String> getStatus() {
        Map<Integer, String> status = new HashMap<>(3);
        //status.put(null, "请选择");
        status.put(0, "执行中");
        status.put(1, "成功");
        status.put(2, "失败");
        return status;
    }
}
