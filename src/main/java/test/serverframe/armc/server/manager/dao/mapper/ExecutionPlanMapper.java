package test.serverframe.armc.server.manager.dao.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.apache.ibatis.annotations.UpdateProvider;
import org.apache.ibatis.type.JdbcType;
import test.serverframe.armc.server.manager.domain.ExecutionPlan;

public interface ExecutionPlanMapper {
    @Delete({
        "delete from tb_ssgp_execution_plan",
        "where id = #{id,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String id);

    @Insert({
        "insert into tb_ssgp_execution_plan (id, trigger_id, ",
        "execute_count, concurrent_count)",
        "values (#{id,jdbcType=VARCHAR}, #{triggerId,jdbcType=VARCHAR}, ",
        "#{executeCount,jdbcType=INTEGER}, #{concurrentCount,jdbcType=INTEGER})"
    })
    int insert(ExecutionPlan record);

    @InsertProvider(type=ExecutionPlanSqlProvider.class, method="insertSelective")
    int insertSelective(ExecutionPlan record);

    @Select({
        "select",
        "id, trigger_id, execute_count, concurrent_count",
        "from tb_ssgp_execution_plan",
        "where id = #{id,jdbcType=VARCHAR}"
    })
    @Results({
        @Result(column="id", property="id", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="trigger_id", property="triggerId", jdbcType=JdbcType.VARCHAR),
        @Result(column="execute_count", property="executeCount", jdbcType=JdbcType.INTEGER),
        @Result(column="concurrent_count", property="concurrentCount", jdbcType=JdbcType.INTEGER)
    })
    ExecutionPlan selectByPrimaryKey(String id);

    @UpdateProvider(type=ExecutionPlanSqlProvider.class, method="updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(ExecutionPlan record);

    @Update({
        "update tb_ssgp_execution_plan",
        "set trigger_id = #{triggerId,jdbcType=VARCHAR},",
          "execute_count = #{executeCount,jdbcType=INTEGER},",
          "concurrent_count = #{concurrentCount,jdbcType=INTEGER}",
        "where id = #{id,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(ExecutionPlan record);
}