package test.serverframe.armc.server.manager.controller;

import com.microcore.entity.AssertCondition;
import com.microcore.service.AssertService;
import com.microcore.service.asserts.condition.CompareCondition;
import com.microcore.service.asserts.condition.LinkCondition;
import com.microcore.service.asserts.impl.Assertor;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import test.serverframe.armc.server.manager.common.ResultDtoUtil;
import test.serverframe.armc.server.manager.dto.ResultDto;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;

/**
 * @author LeiZhenYang
 * @date 2019.01.07
 */
@RestController
@RequestMapping("assert")
@CrossOrigin(origins = "*")
@Api("断言服务")
public class AssertController {

    @Autowired
    private AssertService assertService;

    @GetMapping("compare/conditions/group")
    @ApiOperation(notes = "获取比较运算符", value = "获取比较运算符")
    public ResultDto<Map<String, List<Assertor>>> getConditionGroup() {
        return ResultDtoUtil.success(CompareCondition.getConditionGroup());
    }

    @GetMapping("link/conditions")
    @ApiOperation(notes = "获取关系运算符", value = "获取关系运算符")
    public ResultDto<List<Map<String, String>>> getConditions() {
        return ResultDtoUtil.success(LinkCondition.getConditions());
    }

    @GetMapping("{fkId}")
    @ApiOperation(notes = "通过外键ID查找断言实体", value = "通过外键ID查找断言实体")
    public ResultDto<List<AssertCondition>> getAssertCondition(@ApiParam("外键ID") @PathVariable String fkId) {
        try {
            return ResultDtoUtil.success(assertService.getAssert(fkId));
        } catch (Exception e) {
            return ResultDtoUtil.error(e.getMessage());
        }
    }

    @DeleteMapping("{fkId}")
    @ApiOperation(notes = "通过外键ID删除断言实体", value = "通过外键ID删除断言实体")
    public ResultDto<Integer> deleteAssertCondition(@ApiParam("外键ID") @PathVariable String fkId) {
        try {
            return ResultDtoUtil.success(assertService.deleteAssert(fkId));
        } catch (Exception e) {
            return ResultDtoUtil.error(e.getMessage());
        }
    }

    @PutMapping
    @ApiOperation(notes = "修改断言", value = "修改断言")
    public ResultDto<Integer> updataAssertCondition(@ApiParam("断言实体") @RequestBody @Valid AssertCondition assertCondition) {
        try {
            if (assertService.isJsonArray(assertCondition.getConditions())) {
                return ResultDtoUtil.success(assertService.updataAssert(assertCondition));
            }
        } catch (Exception e) {
            return ResultDtoUtil.error(e.getMessage());
        }
        return ResultDtoUtil.error("修改失败");
    }

    @PostMapping
    @ApiOperation(notes = "增加断言", value = "增加断言")
    public ResultDto<Integer> addAssertCondition(@ApiParam("断言实体") @RequestBody @Valid AssertCondition assertCondition) {
        try {
            if (assertService.isJsonArray(assertCondition.getConditions())) {
                return ResultDtoUtil.success(assertService.addAssert(assertCondition));
            }
        } catch (Exception e) {
            return ResultDtoUtil.error(e.getMessage());
        }
        return ResultDtoUtil.error("新增失败");
    }
}
