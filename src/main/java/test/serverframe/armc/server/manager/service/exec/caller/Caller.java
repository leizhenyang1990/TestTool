package test.serverframe.armc.server.manager.service.exec.caller;

/**
 * @author LeiZhenYang
 * @date 2019.01.03
 */
public interface Caller {

    void asyncCall() throws RuntimeException;

    Object syncCall() throws RuntimeException;
}
