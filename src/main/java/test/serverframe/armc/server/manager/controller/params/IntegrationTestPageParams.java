package test.serverframe.armc.server.manager.controller.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author LeiZhenYang
 * @date 2018.11.29
 */
@ApiModel("集成测试分页查询条件")
public class IntegrationTestPageParams extends PageParams{

    @ApiModelProperty("集成测试名称")
    private String name;

    @ApiModelProperty("测试状态")
    private Integer status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
