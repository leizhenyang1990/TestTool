package test.serverframe.armc.server.manager.service.exec.params;

import test.serverframe.armc.server.manager.domain.IntegrationTestInterface;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 集成测试接口执行参数
 */
public class InterfaceTestParams extends IntegrationTestParams {

    /**
     * 集成测试接口
     */
    private IntegrationTestInterface testInterface;


    /**
     * 接口测试总数
     */
    private int interfaceTestTotalCount;

    /**
     * 接口测试并发总数
     */
    private AtomicInteger concurrentInterfaceTestCallbackTotalCounter;

    /**
     * 接口测试并发总数
     */
    private AtomicInteger concurrentInterfaceTestSuccessTotalCounter;

    public InterfaceTestParams(IntegrationTestParams params) {
        setExecuteCount(params.getExecuteCount());
        setInterfaces(params.getInterfaces());
        setItrId(params.getItrId());
        setConcurrentCount(params.getConcurrentCount());
        setTotalCount(params.getTotalCount());
        // 一次集成测试相关计数
        setIntegrationTestTotalCount(params.getIntegrationTestTotalCount());
        setConcurrentIntegrationTestCallbackTotalCounter(params.getConcurrentIntegrationTestCallbackTotalCounter());
        setConcurrentIntegrationTestSuccessTotalCounter(params.getConcurrentIntegrationTestSuccessTotalCounter());
        // 注入后续使用的服务
        setInterfaceTestService(params.getInterfaceTestService());
        setIntegrationTestRecordService(params.getIntegrationTestRecordService());
        setIntegrationTestInterfaceUseCaseRecordAssoService(params.getIntegrationTestInterfaceUseCaseRecordAssoService());
        setInterfaceRecordService(params.getInterfaceRecordService());
        setTestUseCaseRecordService(params.getTestUseCaseRecordService());
        setAssertService(params.getAssertService());
    }

    public IntegrationTestInterface getTestInterface() {
        return testInterface;
    }

    public void setTestInterface(IntegrationTestInterface testInterface) {
        this.testInterface = testInterface;
    }

    public int getInterfaceTestTotalCount() {
        return interfaceTestTotalCount;
    }

    public void setInterfaceTestTotalCount(int interfaceTestTotalCount) {
        this.interfaceTestTotalCount = interfaceTestTotalCount;
    }

    public AtomicInteger getConcurrentInterfaceTestCallbackTotalCounter() {
        return concurrentInterfaceTestCallbackTotalCounter;
    }

    public void setConcurrentInterfaceTestCallbackTotalCounter(AtomicInteger concurrentInterfaceTestCallbackTotalCounter) {
        this.concurrentInterfaceTestCallbackTotalCounter = concurrentInterfaceTestCallbackTotalCounter;
    }

    public AtomicInteger getConcurrentInterfaceTestSuccessTotalCounter() {
        return concurrentInterfaceTestSuccessTotalCounter;
    }

    public void setConcurrentInterfaceTestSuccessTotalCounter(AtomicInteger concurrentInterfaceTestSuccessTotalCounter) {
        this.concurrentInterfaceTestSuccessTotalCounter = concurrentInterfaceTestSuccessTotalCounter;
    }

}
