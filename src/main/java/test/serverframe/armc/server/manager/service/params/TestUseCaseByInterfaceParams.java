package test.serverframe.armc.server.manager.service.params;

/**
 * @author LeiZhenYang
 * @date 2018.12.02
 */
public class TestUseCaseByInterfaceParams {

    private String interfaceId;

    public String getInterfaceId() {
        return interfaceId;
    }

    public void setInterfaceId(String interfaceId) {
        this.interfaceId = interfaceId;
    }

    public TestUseCaseByInterfaceParams() {
    }

    public TestUseCaseByInterfaceParams(String interfaceId) {
        this.interfaceId = interfaceId;
    }
}
