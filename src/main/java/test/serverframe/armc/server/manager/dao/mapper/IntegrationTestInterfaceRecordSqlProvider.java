package test.serverframe.armc.server.manager.dao.mapper;

import org.apache.ibatis.jdbc.SQL;
import test.serverframe.armc.server.manager.domain.IntegrationTestInterfaceRecord;

public class IntegrationTestInterfaceRecordSqlProvider {

    public String insertSelective(IntegrationTestInterfaceRecord record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("tb_ssgp_csgj_integration_test_interface_record");

        if (record.getId() != null) {
            sql.VALUES("id", "#{id,jdbcType=VARCHAR}");
        }

        if (record.getItrId() != null) {
            sql.VALUES("itr_id", "#{itrId,jdbcType=VARCHAR}");
        }

        if (record.getInterfaceId() != null) {
            sql.VALUES("interface_id", "#{interfaceId,jdbcType=VARCHAR}");
        }

        if (record.getInterfaceName() != null) {
            sql.VALUES("interface_name", "#{interfaceName,jdbcType=VARCHAR}");
        }

        if (record.getStatus() != null) {
            sql.VALUES("status", "#{status,jdbcType=VARCHAR}");
        }

        if (record.getTotal() != null) {
            sql.VALUES("total", "#{total,jdbcType=INTEGER}");
        }

        if (record.getSuccessTotal() != null) {
            sql.VALUES("success_total", "#{successTotal,jdbcType=INTEGER}");
        }

        if (record.getStartTime() != null) {
            sql.VALUES("start_time", "#{startTime,jdbcType=TIMESTAMP}");
        }

        if (record.getEndTime() != null) {
            sql.VALUES("end_time", "#{endTime,jdbcType=TIMESTAMP}");
        }

        return sql.toString();
    }

    public String updateByPrimaryKeySelective(IntegrationTestInterfaceRecord record) {
        SQL sql = new SQL();
        sql.UPDATE("tb_ssgp_csgj_integration_test_interface_record");

        if (record.getItrId() != null) {
            sql.SET("itr_id = #{itrId,jdbcType=VARCHAR}");
        }

        if (record.getInterfaceId() != null) {
            sql.SET("interface_id = #{interfaceId,jdbcType=VARCHAR}");
        }

        if (record.getInterfaceName() != null) {
            sql.SET("interface_name = #{interfaceName,jdbcType=VARCHAR}");
        }

        if (record.getStatus() != null) {
            sql.SET("status = #{status,jdbcType=VARCHAR}");
        }

        if (record.getTotal() != null) {
            sql.SET("total = #{total,jdbcType=INTEGER}");
        }

        if (record.getSuccessTotal() != null) {
            sql.SET("success_total = #{successTotal,jdbcType=INTEGER}");
        }

        if (record.getStartTime() != null) {
            sql.SET("start_time = #{startTime,jdbcType=TIMESTAMP}");
        }

        if (record.getEndTime() != null) {
            sql.SET("end_time = #{endTime,jdbcType=TIMESTAMP}");
        }

        sql.WHERE("id = #{id,jdbcType=VARCHAR}");

        return sql.toString();
    }
}
