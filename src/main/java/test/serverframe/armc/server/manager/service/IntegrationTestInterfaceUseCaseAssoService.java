package test.serverframe.armc.server.manager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import test.serverframe.armc.server.manager.controller.vo.IntegrationTestEnterUseCaseAssoAdd;
import test.serverframe.armc.server.manager.dao.mapper.IntegrationTestInterfaceUseCaseAssoMapper;
import test.serverframe.armc.server.manager.domain.IntegrationTestInterfaceUseCaseAsso;

/**
 * @author LeiZhenYang
 * @date 2018.12.05
 */
@Service
public class IntegrationTestInterfaceUseCaseAssoService {

    @Autowired
    private IntegrationTestInterfaceUseCaseAssoMapper mapper;

    /**
     * 新增集成测试接口与测试用例的关联
     *
     * @param record
     * @return
     */
    @Transactional
    public boolean addIntegrationTestEnterUseCaseAsso(IntegrationTestEnterUseCaseAssoAdd record) {
        IntegrationTestInterfaceUseCaseAsso iteuca = new IntegrationTestInterfaceUseCaseAsso(record);
        mapper.insertSelective(iteuca);
        return true;
    }

    /**
     * 根据集成测试接口ID删除集成测试接口与测试用例的关联
     *
     * @param itiId
     */
    public void deleteIntegrationTestEnterUseCaseAssoByItiId(String itiId) {
        mapper.deleteByItiId(itiId);
    }

}
