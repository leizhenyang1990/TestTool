package test.serverframe.armc.server.manager.model;

import java.io.Serializable;

/**
 * 注册中心接口
 *
 * @author LeiZhenYang
 * @date 2018.11.24
 */
public class CenterInterface implements Serializable {
    /**
     * 接口id
     */
    private String siaId;

    /**
     * 实例id
     */
    private String siId;

    /**
     * 接口名称
     */
    private String siaName;

    /**
     * 接口地址
     */
    private String siaPath;

    /**
     * 请求方式(GET\POST\PUT\DELETE)
     */
    private String siaHttpMethod;

    /**
     * 连接方式（REST）
     */
    private String siaConnectMethod;

    private String siaCreateTime;

    private String siaCreateTimeStr;

    private Integer siaCount;

    private String siaVersion;

    private Boolean siaIsCross;

    /**
     * 接口状态
     */
    private String siaStatus;

    /**
     * 接口说明
     */
    private String siaDesc;

    /**
     * 是否删除
     */
    private Boolean siaIsDelete;

    /**
     * 接口签名
     */
    private String siaSign;

    /**
     * 请求数据描述
     */
    private String siaRequestData;

    /**
     * 响应数据描述
     */
    private String siaResponseData;

    /**
     * 请求数据json格式
     */
    private String siaRequestDataJson;

    /**
     * 相应数据json格式
     */
    private String siaResponseDataJson;

    /**
     * 接口ip地址
     */
    private String siIpAddress;

    /**
     * 接口端口
     */
    private String siPort;

    private String siaOpenStatus;

    private String siaCodeName;

    private String siaCodeType;

    private Boolean siaIsOpen;

    private String siaIsOpenStr;

    public String getSiaId() {
        return siaId;
    }

    public void setSiaId(String siaId) {
        this.siaId = siaId;
    }

    public String getSiId() {
        return siId;
    }

    public void setSiId(String siId) {
        this.siId = siId;
    }

    public String getSiaName() {
        return siaName;
    }

    public void setSiaName(String siaName) {
        this.siaName = siaName;
    }

    public String getSiaPath() {
        return siaPath;
    }

    public void setSiaPath(String siaPath) {
        this.siaPath = siaPath;
    }

    public String getSiaHttpMethod() {
        return siaHttpMethod;
    }

    public void setSiaHttpMethod(String siaHttpMethod) {
        this.siaHttpMethod = siaHttpMethod;
    }

    public String getSiaConnectMethod() {
        return siaConnectMethod;
    }

    public void setSiaConnectMethod(String siaConnectMethod) {
        this.siaConnectMethod = siaConnectMethod;
    }

    public String getSiaCreateTime() {
        return siaCreateTime;
    }

    public void setSiaCreateTime(String siaCreateTime) {
        this.siaCreateTime = siaCreateTime;
    }

    public String getSiaCreateTimeStr() {
        return siaCreateTimeStr;
    }

    public void setSiaCreateTimeStr(String siaCreateTimeStr) {
        this.siaCreateTimeStr = siaCreateTimeStr;
    }

    public Integer getSiaCount() {
        return siaCount;
    }

    public void setSiaCount(Integer siaCount) {
        this.siaCount = siaCount;
    }

    public String getSiaVersion() {
        return siaVersion;
    }

    public void setSiaVersion(String siaVersion) {
        this.siaVersion = siaVersion;
    }

    public Boolean getSiaIsCross() {
        return siaIsCross;
    }

    public void setSiaIsCross(Boolean siaIsCross) {
        this.siaIsCross = siaIsCross;
    }

    public String getSiaStatus() {
        return siaStatus;
    }

    public void setSiaStatus(String siaStatus) {
        this.siaStatus = siaStatus;
    }

    public String getSiaDesc() {
        return siaDesc;
    }

    public void setSiaDesc(String siaDesc) {
        this.siaDesc = siaDesc;
    }

    public Boolean getSiaIsDelete() {
        return siaIsDelete;
    }

    public void setSiaIsDelete(Boolean siaIsDelete) {
        this.siaIsDelete = siaIsDelete;
    }

    public String getSiaSign() {
        return siaSign;
    }

    public void setSiaSign(String siaSign) {
        this.siaSign = siaSign;
    }

    public String getSiaRequestData() {
        return siaRequestData;
    }

    public void setSiaRequestData(String siaRequestData) {
        this.siaRequestData = siaRequestData;
    }

    public String getSiaResponseData() {
        return siaResponseData;
    }

    public void setSiaResponseData(String siaResponseData) {
        this.siaResponseData = siaResponseData;
    }

    public String getSiaRequestDataJson() {
        return siaRequestDataJson;
    }

    public void setSiaRequestDataJson(String siaRequestDataJson) {
        this.siaRequestDataJson = siaRequestDataJson;
    }

    public String getSiaResponseDataJson() {
        return siaResponseDataJson;
    }

    public void setSiaResponseDataJson(String siaResponseDataJson) {
        this.siaResponseDataJson = siaResponseDataJson;
    }

    public String getSiIpAddress() {
        return siIpAddress;
    }

    public void setSiIpAddress(String siIpAddress) {
        this.siIpAddress = siIpAddress;
    }

    public String getSiPort() {
        return siPort;
    }

    public void setSiPort(String siPort) {
        this.siPort = siPort;
    }

    public String getSiaOpenStatus() {
        return siaOpenStatus;
    }

    public void setSiaOpenStatus(String siaOpenStatus) {
        this.siaOpenStatus = siaOpenStatus;
    }

    public String getSiaCodeName() {
        return siaCodeName;
    }

    public void setSiaCodeName(String siaCodeName) {
        this.siaCodeName = siaCodeName;
    }

    public String getSiaCodeType() {
        return siaCodeType;
    }

    public void setSiaCodeType(String siaCodeType) {
        this.siaCodeType = siaCodeType;
    }

    public Boolean getSiaIsOpen() {
        return siaIsOpen;
    }

    public void setSiaIsOpen(Boolean siaIsOpen) {
        this.siaIsOpen = siaIsOpen;
    }

    public String getSiaIsOpenStr() {
        return siaIsOpenStr;
    }

    public void setSiaIsOpenStr(String siaIsOpenStr) {
        this.siaIsOpenStr = siaIsOpenStr;
    }

    @Override
    public String toString() {
        return "CenterInterface{" +
                "siaId=" + siaId +
                ", siId=" + siId +
                ", siaName='" + siaName + '\'' +
                ", siaPath='" + siaPath + '\'' +
                ", siaHttpMethod='" + siaHttpMethod + '\'' +
                ", siaConnectMethod='" + siaConnectMethod + '\'' +
                ", siaCreateTime='" + siaCreateTime + '\'' +
                ", siaCreateTimeStr='" + siaCreateTimeStr + '\'' +
                ", siaCount=" + siaCount +
                ", siaVersion='" + siaVersion + '\'' +
                ", siaIsCross=" + siaIsCross +
                ", siaStatus='" + siaStatus + '\'' +
                ", siaDesc='" + siaDesc + '\'' +
                ", siaIsDelete=" + siaIsDelete +
                ", siaSign='" + siaSign + '\'' +
                ", siaRequestData='" + siaRequestData + '\'' +
                ", siaResponseData='" + siaResponseData + '\'' +
                ", siaRequestDataJson='" + siaRequestDataJson + '\'' +
                ", siaResponseDataJson='" + siaResponseDataJson + '\'' +
                ", siIpAddress='" + siIpAddress + '\'' +
                ", siPort='" + siPort + '\'' +
                ", siaOpenStatus='" + siaOpenStatus + '\'' +
                ", siaCodeName='" + siaCodeName + '\'' +
                ", siaCodeType='" + siaCodeType + '\'' +
                ", siaIsOpen=" + siaIsOpen +
                ", siaIsOpenStr='" + siaIsOpenStr + '\'' +
                '}';
    }
}
