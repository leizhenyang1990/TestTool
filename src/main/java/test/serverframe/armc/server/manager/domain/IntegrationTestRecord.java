package test.serverframe.armc.server.manager.domain;

import java.util.Date;

public class IntegrationTestRecord {
    private String id;

    private String itId;

    private String status;

    private Date startTime;

    private Date endTime;

    private Integer total;

    private Integer successTotal;

    private Integer concurrentCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getItId() {
        return itId;
    }

    public void setItId(String itId) {
        this.itId = itId == null ? null : itId.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getSuccessTotal() {
        return successTotal;
    }

    public void setSuccessTotal(Integer successTotal) {
        this.successTotal = successTotal;
    }

    public Integer getConcurrentCount() {
        return concurrentCount;
    }

    public void setConcurrentCount(Integer concurrentCount) {
        this.concurrentCount = concurrentCount;
    }

    public static class Status {
        /**
         * 执行中
         */
        public static final String Working = "0";

        /**
         * 成功
         */
        public static final String Success = "1";

        /**
         * 失败
         */
        public static final String Failure = "2";

    }

}
