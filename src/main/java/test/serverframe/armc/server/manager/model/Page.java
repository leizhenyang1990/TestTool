package test.serverframe.armc.server.manager.model;

import java.util.List;

/**
 * @author LeiZhenYang
 * @date 2018.11.26
 */
public class Page<T>{

    private int total;

    private List<T> list;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
