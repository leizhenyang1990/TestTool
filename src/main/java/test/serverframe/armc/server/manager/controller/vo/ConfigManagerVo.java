package test.serverframe.armc.server.manager.controller.vo;

import io.swagger.annotations.ApiModelProperty;

/**
 * 配置参数
 * 
 * @author yegui  
 * @date 2019年2月27日
 */
public class ConfigManagerVo {
	
    @ApiModelProperty(value = "id,默认为1")
    private String id;
    
    @ApiModelProperty(value = "历史记录保留天数")
    private Integer recordTime;
    
    @ApiModelProperty(value = "http连接超时时间")
    private Integer httpConnectTimeout;
    
    @ApiModelProperty(value = "读取数据超时时间")
    private Integer httpReadTimeout;
    
    @ApiModelProperty(value = "本系统名称")
    private String systemName;
    
    @ApiModelProperty(value = "连接不够用的超时时间")
    private Integer httpConnectionRequestTimeout;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName == null ? null : systemName.trim();
    }

    public Integer getHttpConnectTimeout() {
        return httpConnectTimeout;
    }

    public void setHttpConnectTimeout(Integer httpConnectTimeout) {
        this.httpConnectTimeout = httpConnectTimeout;
    }

    public Integer getHttpReadTimeout() {
        return httpReadTimeout;
    }

    public void setHttpReadTimeout(Integer httpReadTimeout) {
        this.httpReadTimeout = httpReadTimeout;
    }

    public Integer getRecordTime() {
        return recordTime;
    }

    public void setRecordTime(Integer recordTime) {
        this.recordTime = recordTime;
    }
    
    public Integer getHttpConnectionRequestTimeout() {
		return httpConnectionRequestTimeout;
	}

	public void setHttpConnectionRequestTimeout(Integer httpConnectionRequestTimeout) {
		this.httpConnectionRequestTimeout = httpConnectionRequestTimeout;
	}
}
