package test.serverframe.armc.server.manager.dao.mapper;

import org.apache.ibatis.annotations.*;
import org.apache.ibatis.type.JdbcType;
import test.serverframe.armc.server.manager.domain.InterfaceTestRecord;

public interface InterfaceTestRecordMapper {
    @Delete({
            "delete from tb_ssgp_csgj_interface_test_record",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    int deleteByPrimaryKey(String id);

    @Insert({
            "insert into tb_ssgp_csgj_interface_test_record (id, uc_id, ",
            "out_value, start_time, ",
            "end_time, status, assert_value)",
            "values (#{id,jdbcType=VARCHAR}, #{ucId,jdbcType=VARCHAR}, ",
            "#{outValue,jdbcType=VARCHAR}, #{startTime,jdbcType=TIMESTAMP}, ",
            "#{endTime,jdbcType=TIMESTAMP}, #{status,jdbcType=VARCHAR}, #{assertValue,jdbcType=VARCHAR})"
    })
    int insert(InterfaceTestRecord record);

    @InsertProvider(type = InterfaceTestRecordSqlProvider.class, method = "insertSelective")
    int insertSelective(InterfaceTestRecord record);

    @Select({
            "select",
            "id, uc_id, out_value, start_time, end_time, status, assert_value",
            "from tb_ssgp_csgj_interface_test_record",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    @Results({
            @Result(column = "id", property = "id", jdbcType = JdbcType.VARCHAR, id = true),
            @Result(column = "uc_id", property = "ucId", jdbcType = JdbcType.VARCHAR),
            @Result(column = "out_value", property = "outValue", jdbcType = JdbcType.VARCHAR),
            @Result(column = "start_time", property = "startTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "end_time", property = "endTime", jdbcType = JdbcType.TIMESTAMP),
            @Result(column = "status", property = "status", jdbcType = JdbcType.VARCHAR),
            @Result(column = "assert_value", property = "assertValue", jdbcType = JdbcType.VARCHAR)
    })
    InterfaceTestRecord selectByPrimaryKey(String id);

    @UpdateProvider(type = InterfaceTestRecordSqlProvider.class, method = "updateByPrimaryKeySelective")
    int updateByPrimaryKeySelective(InterfaceTestRecord record);

    @Update({
            "update tb_ssgp_csgj_interface_test_record",
            "set uc_id = #{ucId,jdbcType=VARCHAR},",
            "out_value = #{outValue,jdbcType=VARCHAR},",
            "start_time = #{startTime,jdbcType=TIMESTAMP},",
            "end_time = #{endTime,jdbcType=TIMESTAMP},",
            "status = #{status,jdbcType=VARCHAR},",
            "assert_value = #{assertValue,jdbcType=VARCHAR}",
            "where id = #{id,jdbcType=VARCHAR}"
    })
    int updateByPrimaryKey(InterfaceTestRecord record);
}