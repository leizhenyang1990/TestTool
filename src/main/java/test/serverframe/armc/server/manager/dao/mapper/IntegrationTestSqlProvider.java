package test.serverframe.armc.server.manager.dao.mapper;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.jdbc.SQL;
import test.serverframe.armc.server.manager.domain.IntegrationTest;

import java.util.Map;

public class IntegrationTestSqlProvider {

    public String insertSelective(IntegrationTest record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("tb_ssgp_csgj_integration_test");

        if (record.getId() != null) {
            sql.VALUES("id", "#{id,jdbcType=VARCHAR}");
        }

        if (record.getName() != null) {
            sql.VALUES("name", "#{name,jdbcType=VARCHAR}");
        }

        if (record.getRemark() != null) {
            sql.VALUES("remark", "#{remark,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.VALUES("update_time", "#{updateTime,jdbcType=TIMESTAMP}");
        }
        if (record.getClassName() != null) {
            sql.VALUES("class_name", "#{className,jdbcType=VARCHAR}");
        }

        if (record.getMethodName() != null) {
            sql.VALUES("method_name", "#{methodName,jdbcType=VARCHAR}");
        }

        if (record.getParams() != null) {
            sql.VALUES("params", "#{params,jdbcType=VARCHAR}");
        }
        if (record.getStatus() != null) {
            sql.VALUES("status", "#{status,jdbcType=INTEGER}");
        }
        if (record.getConcurrent() != null) {
            sql.VALUES("is_concurrent", "#{isConcurrent,jdbcType=BIT}");
        }
        if (record.getStartTime() != null) {
            sql.VALUES("start_time", "#{startTime,jdbcType=TIMESTAMP}");
        }

        if (record.getEndTime() != null) {
            sql.VALUES("end_time", "#{endTime,jdbcType=TIMESTAMP}");
        }
        if (record.getExecutePlanData() != null) {
            sql.VALUES("execute_plan_data", "#{executePlanData,jdbcType=VARCHAR}");
        }
        if (record.getUsed() != null) {
            sql.VALUES("is_used", "#{isUsed,jdbcType=VARCHAR}");
        }


        return sql.toString();
    }

    public String updateByPrimaryKeySelective(IntegrationTest record) {
        SQL sql = new SQL();
        sql.UPDATE("tb_ssgp_csgj_integration_test");

        if (record.getName() != null) {
            sql.SET("name = #{name,jdbcType=VARCHAR}");
        }

        if (record.getRemark() != null) {
            sql.SET("remark = #{remark,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.SET("update_time = #{updateTime,jdbcType=TIMESTAMP}");
        }
        if (record.getClassName() != null) {
            sql.SET("class_name = #{className,jdbcType=VARCHAR}");
        }

        if (record.getMethodName() != null) {
            sql.SET("method_name = #{methodName,jdbcType=VARCHAR}");
        }

        if (record.getParams() != null) {
            sql.SET("params = #{params,jdbcType=VARCHAR}");
        }
        if (record.getStatus() != null) {
            sql.SET("status = #{status,jdbcType=INTEGER}");
        }
        if (record.getConcurrent() != null) {
            sql.SET("is_concurrent = #{isConcurrent,jdbcType=BIT}");
        }
        if (record.getStartTime() != null) {
            sql.SET("start_time = #{startTime,jdbcType=TIMESTAMP}");
        }

        if (record.getEndTime() != null) {
            sql.SET("end_time = #{endTime,jdbcType=TIMESTAMP}");
        }
        if (record.getExecutePlanData() != null) {
            sql.SET("execute_plan_data = #{executePlanData,jdbcType=VARCHAR}");
        }
        if (record.getUsed() != null) {
            sql.SET("is_used = #{isUsed,jdbcType=VARCHAR}");
        }

        sql.WHERE("id = #{id,jdbcType=VARCHAR}");

        return sql.toString();
    }

    public String selectByPage(Map<String, Object> params) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT " +
                " t.id, " +
                " t.NAME, " +
                " t.remark, " +
                " t.create_time, " +
                " t.update_time, " +
                " t.status, " +
                " record.STATUS AS last_test_status, " +
                " record.start_time AS last_test_time, " +
                " record.total AS last_test_interface_total, " +
                " record.success_total AS last_test_success_interface_total, " +
                " record.id AS last_test_record_id  " +
                "FROM " +
                " tb_ssgp_csgj_integration_test t " +
                " left JOIN ( " +
                "SELECT " +
                " max( b1.id ) AS id, " +
                " b1.it_id " +
                "FROM " +
                " tb_ssgp_csgj_integration_test_record b1 " +
                " INNER JOIN ( SELECT it_id, max( start_time ) AS lastest_time FROM tb_ssgp_csgj_integration_test_record GROUP BY it_id ) b2 ON b1.it_id = b2.it_id  " +
                " AND b1.start_time = b2.lastest_time  " +
                " GROUP BY " +
                " b1.it_id  " +
                " ) last ON t.id = last.it_id " +
                " left JOIN tb_ssgp_csgj_integration_test_record record ON record.id = last.id  " +
                "WHERE " +
                " 1 = 1  and is_used=1 ");
        if (params.get("status") != null) {
            sql.append(" and record.status = " + params.get("status"));
        }
        Object name = params.get("name");
        if (name != null && StringUtils.isNotBlank(name.toString())) {
            sql.append(" and t.name like '%" + name.toString() + "%'");
        }
        sql.append(" ORDER BY ");
        sql.append(" t.update_time DESC");
        return sql.toString();
    }
}
