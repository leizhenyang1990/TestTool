package test.serverframe.armc.server.manager.controller.vo;

import com.microcore.entity.param.InvokeParams;
import com.microcore.entity.param.TriggerParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author LeiZhenYang
 * @date 2018.11.30
 */
@ApiModel("集成测试新增模型")
public class IntegrationTestAdd extends InvokeParams {

    @ApiModelProperty(value = "集成测试名称", required = true)
    private String name;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("状态（启用、禁用）")
    private Integer status;

    @ApiModelProperty("是否并发")
    private Boolean isConcurrent;

    @ApiModelProperty("开始时间")
    private Date startTime;

    @ApiModelProperty("结束时间")
    private Date endTime;

    @ApiModelProperty("集成测试接口集合")
    private List<IntegrationTestInterfaceAdd> interfaces;

    @ApiModelProperty("集成测试的调度的trigger集合（不用填trigger属性）")
    private List<IntegrationTestTriggerParams> integrationTestTriggerParams;

    @ApiModelProperty("集成测试执行计划数据")
    private String executePlanData;

    @ApiModelProperty(hidden = true)
    private String isUsed;

    public String getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(String isUsed) {
        this.isUsed = isUsed;
    }

    public String getExecutePlanData() {
        return executePlanData;
    }

    public void setExecutePlanData(String executePlanData) {
        this.executePlanData = executePlanData;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public List<IntegrationTestInterfaceAdd> getInterfaces() {
        return interfaces;
    }

    public void setInterfaces(List<IntegrationTestInterfaceAdd> interfaces) {
        this.interfaces = interfaces;
    }

    public List<IntegrationTestTriggerParams> getIntegrationTestTriggerParams() {
        return integrationTestTriggerParams;
    }

    public void setIntegrationTestTriggerParams(List<IntegrationTestTriggerParams> integrationTestTriggerParams) {
        List<TriggerParams> list = new ArrayList<>();
        integrationTestTriggerParams.forEach(ele -> list.add(ele));
        super.setTriggers(list);
        this.integrationTestTriggerParams = integrationTestTriggerParams;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean getConcurrent() {
        return isConcurrent;
    }

    public void setConcurrent(Boolean concurrent) {
        isConcurrent = concurrent;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
