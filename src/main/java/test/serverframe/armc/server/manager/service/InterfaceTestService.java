package test.serverframe.armc.server.manager.service;

import com.microcore.entity.AssertCondition;
import com.microcore.service.AssertService;
import com.microcore.service.asserts.model.AssertResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import test.serverframe.armc.server.manager.dao.mapper.InterfaceTestRecordMapper;
import test.serverframe.armc.server.manager.domain.InterfaceTestRecord;
import test.serverframe.armc.server.manager.domain.TestUseCase;
import test.serverframe.armc.server.manager.service.exec.caller.TestUseCaseHTTPCaller;
import test.serverframe.armc.server.util.RestTemplateUtil;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 接口用例
 *
 * @author LeiZhenYang
 * @date 2018.11.29
 */
@Service
public class InterfaceTestService extends BaseService<InterfaceTestRecord, String> {

    @Autowired
    private InterfaceTestRecordMapper mapper;

    @Autowired
    private AssertService assertService;

    @Override
    protected String getMapperName() {
        return InterfaceTestRecordMapper.class.getName();
    }

    /**
     * 执行测试用例，并记录返回结果
     *
     * @param testUseCase 测试用例
     * @return
     */
    @Transactional
    public InterfaceTestRecord callAndRecord(TestUseCase testUseCase) {
        InterfaceTestRecord record = new InterfaceTestRecord();
        // 开始时间
        record.setStartTime(new Date());
        record.setId(UUID.randomUUID().toString());
        record.setUcId(testUseCase.getId());
        try {
            // 成功
            String resultJson = TestUseCaseHTTPCaller.newSyncInstance(testUseCase).syncCall();
            List<AssertCondition> assertList = testUseCase.getAssertConditions();
            if (assertList != null) {
                for (AssertCondition assertCondition : assertList) {
                    AssertResult assertResult = AssertService.toAssert(resultJson, assertCondition.getConditions());
                    // 断言判断的错误信息
                    record.setAssertValue(assertResult.toString());
                }
            }
            // 请求成功
            record.setStatus(InterfaceTestRecord.Status.Success);
            record.setOutValue(resultJson);
            record.setCurl(RestTemplateUtil.getCurl(HttpMethod.valueOf(testUseCase.getRequestMethod()),testUseCase.getRequestUrl(),testUseCase.getInQuery(),testUseCase.getInBody(),testUseCase.getInHeader()));
            record.setUrl(RestTemplateUtil.getUrl(testUseCase.getRequestUrl(),testUseCase.getInQuery()));
        } catch (Exception e) {
            // 请求失败
            e.printStackTrace();
            record.setStatus(InterfaceTestRecord.Status.Failure);
            record.setOutValue(e.getMessage());
        } finally {
            // 结束时间
            record.setEndTime(new Date());
            insertSelective(record);
        }
        return record;
    }

}
