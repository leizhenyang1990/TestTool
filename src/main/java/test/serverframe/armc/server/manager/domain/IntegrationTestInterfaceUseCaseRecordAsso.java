package test.serverframe.armc.server.manager.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 集成测试 接口与用例记录的关联表
 */
@ApiModel()
public class IntegrationTestInterfaceUseCaseRecordAsso {

    @ApiModelProperty(value = "接口记录ID")
    private String itirId;

    @ApiModelProperty(value = "测试用例ID")
    private String tucId;

    @ApiModelProperty(value = "集成测试ID")
    private String itrId;

    public String getItirId() {
        return itirId;
    }

    public void setItirId(String itirId) {
        this.itirId = itirId == null ? null : itirId.trim();
    }

    public String getTucId() {
        return tucId;
    }

    public void setTucId(String tucId) {
        this.tucId = tucId == null ? null : tucId.trim();
    }

    public String getItrId() {
        return itrId;
    }

    public void setItrId(String itrId) {
        this.itrId = itrId == null ? null : itrId.trim();
    }
}
