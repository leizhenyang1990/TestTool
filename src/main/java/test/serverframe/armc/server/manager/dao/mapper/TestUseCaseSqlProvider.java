package test.serverframe.armc.server.manager.dao.mapper;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.jdbc.SQL;
import test.serverframe.armc.server.manager.domain.TestUseCase;

import java.util.Map;

public class TestUseCaseSqlProvider {

    public String insertSelective(TestUseCase record) {
        SQL sql = new SQL();
        sql.INSERT_INTO("tb_ssgp_csgj_test_use_case");

        if (record.getId() != null) {
            sql.VALUES("id", "#{id,jdbcType=VARCHAR}");
        }

        if (record.getInterfaceId() != null) {
            sql.VALUES("interface_id", "#{interfaceId,jdbcType=VARCHAR}");
        }

        if (record.getInterfaceName() != null) {
            sql.VALUES("interface_name", "#{interfaceName,jdbcType=VARCHAR}");
        }

        if (record.getRequestMethod() != null) {
            sql.VALUES("request_method", "#{requestMethod,jdbcType=VARCHAR}");
        }

        if (record.getRequestUrl() != null) {
            sql.VALUES("request_url", "#{requestUrl,jdbcType=VARCHAR}");
        }

        if (record.getUseCaseName() != null) {
            sql.VALUES("use_case_name", "#{useCaseName,jdbcType=VARCHAR}");
        }

        if (record.getRemark() != null) {
            sql.VALUES("remark", "#{remark,jdbcType=VARCHAR}");
        }

        if (record.getSource() != null) {
            sql.VALUES("source", "#{source,jdbcType=VARCHAR}");
        }

        if (record.getDeleteFlag() != null) {
            sql.VALUES("delete_flag", "#{deleteFlag,jdbcType=VARCHAR}");
        }

        if (record.getInHeader() != null) {
            sql.VALUES("in_header", "#{inHeader,jdbcType=VARCHAR}");
        }

        if (record.getInBody() != null) {
            sql.VALUES("in_body", "#{inBody,jdbcType=VARCHAR}");
        }

        if (record.getInQuery() != null) {
            sql.VALUES("in_query", "#{inQuery,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.VALUES("create_time", "#{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.VALUES("update_time", "#{updateTime,jdbcType=TIMESTAMP}");
        }
        return sql.toString();
    }

    public String updateByPrimaryKeySelective(TestUseCase record) {
        SQL sql = new SQL();
        sql.UPDATE("tb_ssgp_csgj_test_use_case");

        if (record.getInterfaceId() != null) {
            sql.SET("interface_id = #{interfaceId,jdbcType=VARCHAR}");
        }

        if (record.getInterfaceName() != null) {
            sql.SET("interface_name = #{interfaceName,jdbcType=VARCHAR}");
        }

        if (record.getRequestMethod() != null) {
            sql.SET("request_method = #{requestMethod,jdbcType=VARCHAR}");
        }

        if (record.getRequestUrl() != null) {
            sql.SET("request_url = #{requestUrl,jdbcType=VARCHAR}");
        }

        if (record.getUseCaseName() != null) {
            sql.SET("use_case_name = #{useCaseName,jdbcType=VARCHAR}");
        }

        if (record.getRemark() != null) {
            sql.SET("remark = #{remark,jdbcType=VARCHAR}");
        }

        if (record.getSource() != null) {
            sql.SET("source = #{source,jdbcType=VARCHAR}");
        }

        if (record.getDeleteFlag() != null) {
            sql.SET("delete_flag = #{deleteFlag,jdbcType=VARCHAR}");
        }

        if (record.getInHeader() != null) {
            sql.SET("in_header = #{inHeader,jdbcType=VARCHAR}");
        }

        if (record.getInBody() != null) {
            sql.SET("in_body = #{inBody,jdbcType=VARCHAR}");
        }

        if (record.getInQuery() != null) {
            sql.SET("in_query = #{inQuery,jdbcType=VARCHAR}");
        }

        if (record.getCreateTime() != null) {
            sql.SET("create_time = #{createTime,jdbcType=TIMESTAMP}");
        }

        if (record.getUpdateTime() != null) {
            sql.SET("update_time = #{updateTime,jdbcType=TIMESTAMP}");
        }

        sql.WHERE("id = #{id,jdbcType=VARCHAR}");

        return sql.toString();
    }

    /**
     * 分页查询
     *
     * @param params
     * @return
     */
    public String selectByPage(Map<String, Object> params) {
        StringBuilder sql = new StringBuilder();
        sql.append(selectByInterfaceId(params));
        Object source = params.get("source");
        if (source != null && StringUtils.isNotBlank(source.toString())) {
            sql.append("and t.source = '" + source.toString() + "'");
        } else {
            sql.append("and t.source = '" + TestUseCase.Source.TestUseCase + "'");
        }
        Object useCaseName = params.get("useCaseName");
        if (useCaseName != null && StringUtils.isNotBlank(useCaseName.toString())) {
            sql.append(" and t.use_case_name like '%" + useCaseName + "%'");
        }
        sql.append(" order by t.update_time desc");
        return sql.toString();
    }

    public String selectByInterfaceId(Map<String, Object> params) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT t.* from tb_ssgp_csgj_test_use_case t where 1=1 and t.delete_flag='" + TestUseCase.DeleteFlag.False + "'");
        Object interfaceId = params.get("interfaceId");
        if (interfaceId != null && StringUtils.isNotBlank(interfaceId.toString())) {
            sql.append(" and t.interface_id = '" + interfaceId + "'");
        }
        return sql.toString();
    }
}
