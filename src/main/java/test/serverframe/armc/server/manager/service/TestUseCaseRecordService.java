package test.serverframe.armc.server.manager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import test.serverframe.armc.server.manager.dao.mapper.InterfaceTestRecordMapper;
import test.serverframe.armc.server.manager.dao.mapper.TestUseCaseRecordMapper;
import test.serverframe.armc.server.manager.domain.TestUseCaseRecord;

import java.util.List;

/**
 * 测试用例记录服务
 *
 * @author LiuChunfu
 * @date 2019.01.13
 */
@Service
public class TestUseCaseRecordService extends BaseService<TestUseCaseRecord, String> {

    @Autowired
    private TestUseCaseRecordMapper mapper;

    @Override
    protected String getMapperName() {
        return InterfaceTestRecordMapper.class.getName();
    }

    public List<TestUseCaseRecord> getRecordByItirId(String itirId) {
        return mapper.selectByItirId(itirId);
    }
}
