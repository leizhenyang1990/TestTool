package test.serverframe.armc.server.manager.controller.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author LeiZhenYang
 * @date 2018.11.29
 */
@ApiModel("测试用例分页查询条件")
public class TestUseCasePageParams extends PageParams {

    @ApiModelProperty("接口ID")
    private String interfaceId;
    @ApiModelProperty("用例测试名称")
    private String useCaseName;

    public String getInterfaceId() {
        return interfaceId;
    }

    public void setInterfaceId(String interfaceId) {
        this.interfaceId = interfaceId;
    }

    public String getUseCaseName() {
        return useCaseName;
    }

    public void setUseCaseName(String useCaseName) {
        this.useCaseName = useCaseName;
    }
}
