package test.serverframe.armc.server.manager.service.exec.params;

import com.microcore.service.AssertService;
import test.serverframe.armc.server.manager.domain.IntegrationTestInterface;
import test.serverframe.armc.server.manager.service.*;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 集成测试接口执行参数
 */
public class IntegrationTestParams {

    /**
     * 执行次数
     */
    private int executeCount;

    /**
     * 并发数
     */
    private int concurrentCount;

    /**
     * 接口集合
     */
    private List<IntegrationTestInterface> interfaces;

    /**
     * 集成测试ID
     */
    private String itId;

    /**
     * 集成测试记录ID
     */
    private String itrId;

    /**
     * 总数
     */
    private int totalCount;

    /**
     * 集成测试总数
     */
    private int integrationTestTotalCount;

    /**
     * 集成测试并发总数
     */
    private AtomicInteger concurrentIntegrationTestCallbackTotalCounter;

    /**
     * 集成测试成功并发总数
     */
    private AtomicInteger concurrentIntegrationTestSuccessTotalCounter;

    private InterfaceTestService interfaceTestService;

    private IntegrationTestRecordService integrationTestRecordService;

    private IntegrationTestInterfaceUseCaseRecordAssoService integrationTestInterfaceUseCaseRecordAssoService;

    private IntegrationTestInterfaceRecordService interfaceRecordService;

    private TestUseCaseRecordService testUseCaseRecordService;

    private AssertService assertService;

    public int getExecuteCount() {
        return executeCount;
    }

    public void setExecuteCount(int executeCount) {
        this.executeCount = executeCount;
    }

    public int getConcurrentCount() {
        return concurrentCount;
    }

    public void setConcurrentCount(int concurrentCount) {
        this.concurrentCount = concurrentCount;
    }

    public List<IntegrationTestInterface> getInterfaces() {
        return interfaces;
    }

    public void setInterfaces(List<IntegrationTestInterface> interfaces) {
        this.interfaces = interfaces;
    }

    public String getItrId() {
        return itrId;
    }

    public void setItrId(String itrId) {
        this.itrId = itrId;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public InterfaceTestService getInterfaceTestService() {
        return interfaceTestService;
    }

    public void setInterfaceTestService(InterfaceTestService interfaceTestService) {
        this.interfaceTestService = interfaceTestService;
    }

    public IntegrationTestRecordService getIntegrationTestRecordService() {
        return integrationTestRecordService;
    }

    public void setIntegrationTestRecordService(IntegrationTestRecordService integrationTestRecordService) {
        this.integrationTestRecordService = integrationTestRecordService;
    }

    public IntegrationTestInterfaceUseCaseRecordAssoService getIntegrationTestInterfaceUseCaseRecordAssoService() {
        return integrationTestInterfaceUseCaseRecordAssoService;
    }

    public void setIntegrationTestInterfaceUseCaseRecordAssoService(IntegrationTestInterfaceUseCaseRecordAssoService integrationTestInterfaceUseCaseRecordAssoService) {
        this.integrationTestInterfaceUseCaseRecordAssoService = integrationTestInterfaceUseCaseRecordAssoService;
    }

    public IntegrationTestInterfaceRecordService getInterfaceRecordService() {
        return interfaceRecordService;
    }

    public void setInterfaceRecordService(IntegrationTestInterfaceRecordService interfaceRecordService) {
        this.interfaceRecordService = interfaceRecordService;
    }

    public int getIntegrationTestTotalCount() {
        return integrationTestTotalCount;
    }

    public void setIntegrationTestTotalCount(int integrationTestTotalCount) {
        this.integrationTestTotalCount = integrationTestTotalCount;
    }

    public AtomicInteger getConcurrentIntegrationTestCallbackTotalCounter() {
        return concurrentIntegrationTestCallbackTotalCounter;
    }

    public void setConcurrentIntegrationTestCallbackTotalCounter(AtomicInteger concurrentIntegrationTestCallbackTotalCounter) {
        this.concurrentIntegrationTestCallbackTotalCounter = concurrentIntegrationTestCallbackTotalCounter;
    }

    public AtomicInteger getConcurrentIntegrationTestSuccessTotalCounter() {
        return concurrentIntegrationTestSuccessTotalCounter;
    }

    public void setConcurrentIntegrationTestSuccessTotalCounter(AtomicInteger concurrentIntegrationTestSuccessTotalCounter) {
        this.concurrentIntegrationTestSuccessTotalCounter = concurrentIntegrationTestSuccessTotalCounter;
    }

    public AssertService getAssertService() {
        return assertService;
    }

    public void setAssertService(AssertService assertService) {
        this.assertService = assertService;
    }

    public TestUseCaseRecordService getTestUseCaseRecordService() {
        return testUseCaseRecordService;
    }

    public void setTestUseCaseRecordService(TestUseCaseRecordService testUseCaseRecordService) {
        this.testUseCaseRecordService = testUseCaseRecordService;
    }

    public String getItId() {
        return itId;
    }

    public void setItId(String itId) {
        this.itId = itId;
    }

}
