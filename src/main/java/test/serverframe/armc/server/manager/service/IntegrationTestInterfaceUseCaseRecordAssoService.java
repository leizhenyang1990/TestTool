package test.serverframe.armc.server.manager.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import test.serverframe.armc.server.manager.dao.mapper.IntegrationTestInterfaceUseCaseRecordAssoMapper;
import test.serverframe.armc.server.manager.domain.IntegrationTestInterfaceUseCaseRecordAsso;

/**
 * @author LeiZhenYang
 * @date 2018.12.05
 */
@Service
public class IntegrationTestInterfaceUseCaseRecordAssoService extends BaseService<IntegrationTestInterfaceUseCaseRecordAsso, String> {

    @Autowired
    private IntegrationTestInterfaceUseCaseRecordAssoMapper mapper;

    @Override
    protected String getMapperName() {
        return IntegrationTestInterfaceUseCaseRecordAssoMapper.class.getName();
    }
}
