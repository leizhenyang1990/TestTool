package test.serverframe.armc.server.manager.controller.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import test.serverframe.armc.server.manager.domain.extend.RecordCounter;

import java.util.Date;

@ApiModel("集成测试记录分页模型")
public class IntegrationTestRecordPage {

    @ApiModelProperty("集成测试记录ID")
    private String id;

    @ApiModelProperty("集成测试ID")
    private String itId;

    @ApiModelProperty("状态")
    private String status;

    @ApiModelProperty("开始时间")
    private Date startTime;

    @ApiModelProperty("结束时间")
    private Date endTime;

    @ApiModelProperty("接口总数")
    private Integer total;

    @ApiModelProperty("接口成功")
    private Integer successTotal;

    @ApiModelProperty("并发数")
    private Integer concurrentCount;

    @ApiModelProperty("记录数量统计")
    private RecordCounter counter;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getItId() {
        return itId;
    }

    public void setItId(String itId) {
        this.itId = itId == null ? null : itId.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getSuccessTotal() {
        return successTotal;
    }

    public void setSuccessTotal(Integer successTotal) {
        this.successTotal = successTotal;
    }

    public Integer getConcurrentCount() {
        return concurrentCount;
    }

    public void setConcurrentCount(Integer concurrentCount) {
        this.concurrentCount = concurrentCount;
    }

    public RecordCounter getCounter() {
        return counter;
    }

    public void setCounter(RecordCounter counter) {
        this.counter = counter;
    }
}
