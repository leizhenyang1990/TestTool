package test.serverframe.armc.server.manager.controller.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author LeiZhenYang
 * @date 2018.12.02
 */
@ApiModel("测试用例新增模型")
public class TestUseCaseAdd extends TestUseCaseCallInterface {

    @ApiModelProperty("测试用例名称")
    private String useCaseName;

    @ApiModelProperty("备注")
    private String remark;

    public String getUseCaseName() {
        return useCaseName;
    }

    public void setUseCaseName(String useCaseName) {
        this.useCaseName = useCaseName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
