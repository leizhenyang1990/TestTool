package test.serverframe.armc.server.manager.model;

import java.io.Serializable;

/**
 * 注册中心类别（类似于Controller？）
 *
 * @author LeiZhenYang
 * @date 2018.11.24
 */
public class CenterOwner implements Serializable {

    /**
     * 实例id
     */
    private String siId;

    private String siaoId;

    /**
     * 实例名称
     */
    private String siaoName;
    /**
     * 实例代码名称
     */
    private String siaoCodeName;
    /**
     * 实例路径
     */
    private String siaoPath;
    /**
     * 实例类型
     */
    private String siaoType;

    public String getSiId() {
        return siId;
    }

    public void setSiId(String siId) {
        this.siId = siId;
    }

    public String getSiaoId() {
        return siaoId;
    }

    public void setSiaoId(String siaoId) {
        this.siaoId = siaoId;
    }

    public String getSiaoName() {
        return siaoName;
    }

    public void setSiaoName(String siaoName) {
        this.siaoName = siaoName;
    }

    public String getSiaoCodeName() {
        return siaoCodeName;
    }

    public void setSiaoCodeName(String siaoCodeName) {
        this.siaoCodeName = siaoCodeName;
    }

    public String getSiaoPath() {
        return siaoPath;
    }

    public void setSiaoPath(String siaoPath) {
        this.siaoPath = siaoPath;
    }

    public String getSiaoType() {
        return siaoType;
    }

    public void setSiaoType(String siaoType) {
        this.siaoType = siaoType;
    }
}
