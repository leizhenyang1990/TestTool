package test.serverframe.armc.server.manager.domain;

import java.util.Date;

public class TestUseCaseRecord {
    private String id;

    private String itirId;

    private String status;

    private Date startTime;

    private Date endTime;

    private Integer total;

    private Integer successTotal;

    private String testUseCaseName;

    private String testUseCaseId;

    public String getTestUseCaseName() {
        return testUseCaseName;
    }

    public void setTestUseCaseName(String testUseCaseName) {
        this.testUseCaseName = testUseCaseName;
    }

    public String getTestUseCaseId() {
        return testUseCaseId;
    }

    public void setTestUseCaseId(String testUseCaseId) {
        this.testUseCaseId = testUseCaseId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getItirId() {
        return itirId;
    }

    public void setItirId(String itirId) {
        this.itirId = itirId == null ? null : itirId.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getSuccessTotal() {
        return successTotal;
    }

    public void setSuccessTotal(Integer successTotal) {
        this.successTotal = successTotal;
    }

}