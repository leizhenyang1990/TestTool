package test.serverframe.armc.server.manager.dao.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.type.JdbcType;

import test.serverframe.armc.server.manager.controller.vo.ConfigManagerVo;
import test.serverframe.armc.server.manager.domain.ConfigManager;

public interface ConfigManagerMapper {
    @Insert({
        "insert into tb_ssgp_config (id,system_name, http_connect_timeout, ",
        "http_read_timeout, record_time)",
        "values (#{id,jdbcType=VARCHAR}, #{systemName,jdbcType=VARCHAR}, #{httpConnectTimeout,jdbcType=INTEGER}, ",
        "#{httpReadTimeout,jdbcType=INTEGER}, #{recordTime,jdbcType=INTEGER})"
    })
    int insert(ConfigManager record);

    @InsertProvider(type=ConfigManagerSqlProvider.class, method="insertSelective")
    int insertSelective(ConfigManager record);
    
    @InsertProvider(type=ConfigManagerSqlProvider.class, method="updateSelective")
    int updateSelective(ConfigManagerVo record);
    
    @Select({
        "select id, system_name, http_connect_timeout, http_read_timeout, record_time, http_connection_request_timeout",
        "from tb_ssgp_config"
    })
    @Results({
    	@Result(column="id", property="id", jdbcType=JdbcType.VARCHAR, id=true),
        @Result(column="system_name", property="systemName", jdbcType=JdbcType.VARCHAR),
        @Result(column="http_connect_timeout", property="httpConnectTimeout", jdbcType=JdbcType.INTEGER),
        @Result(column="http_read_timeout", property="httpReadTimeout", jdbcType=JdbcType.INTEGER),
        @Result(column="record_time", property="recordTime", jdbcType=JdbcType.INTEGER),
        @Result(column="http_connection_request_timeout", property="httpConnectionRequestTimeout", jdbcType=JdbcType.INTEGER),
    })
    ConfigManager selectConfig();
}