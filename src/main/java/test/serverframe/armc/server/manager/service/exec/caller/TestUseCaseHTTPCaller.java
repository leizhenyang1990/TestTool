package test.serverframe.armc.server.manager.service.exec.caller;

import com.alibaba.fastjson.JSON;
import com.microcore.entity.AssertCondition;
import com.microcore.service.AssertService;
import com.microcore.service.asserts.model.AssertResult;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.concurrent.ListenableFutureCallback;
import test.serverframe.armc.server.manager.domain.*;
import test.serverframe.armc.server.manager.service.exec.params.TestUseCaseTestParams;
import test.serverframe.armc.server.util.RestTemplateUtil;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * 测试用例HttpCaller
 *
 * @author LeiZhenYang
 * @date 2019.01.03
 */
public class TestUseCaseHTTPCaller implements ListenableFutureCallback<ResponseEntity<Object>>, Caller {

    private static final Logger logger = LoggerFactory.getLogger(TestUseCaseHTTPCaller.class);

    private Date startTime;



    /**
     * 测试用例
     */
    private TestUseCase testUseCase;

    /**
     * 测试用例请求模型
     */
    private TestUseCaseTestParams params;

    private String testUseCaseRecordId = UUID.randomUUID().toString();

    /**
     * 测试用例
     *
     * @param testUseCase
     */
    private TestUseCaseHTTPCaller(TestUseCase testUseCase) {
        this.testUseCase = testUseCase;
    }

    /**
     * 测试用例请求参数
     *
     * @param params
     */
    private TestUseCaseHTTPCaller(TestUseCase testUseCase, TestUseCaseTestParams params) {
        this.testUseCase = testUseCase;
        this.params = params;
    }

    /**
     * 实例化同步Caller
     *
     * @param testUseCase
     * @return
     */
    public static TestUseCaseHTTPCaller newSyncInstance(TestUseCase testUseCase) {
        return new TestUseCaseHTTPCaller(testUseCase);
    }

    /**
     * 实例化异步Caller
     *
     * @param testUseCase
     * @param params
     * @return
     */
    public static TestUseCaseHTTPCaller newAsyncInstance(TestUseCase testUseCase, TestUseCaseTestParams params) {
        return new TestUseCaseHTTPCaller(testUseCase, params);
    }

    @Override
    public void onFailure(Throwable throwable) {
        callback(throwable);
    }

    @Override
    public void onSuccess(ResponseEntity<Object> object) {
        callback(object);
    }

    /**
     * 同步callback计数
     */
    public synchronized void incrementCallback() {
        // 记录各维度的响应次数 begin
        Integer i1, i2, i3;
        // 集成测试维度计数
        i1 = params.getConcurrentIntegrationTestCallbackTotalCounter().incrementAndGet();
        // 接口维度计数
        i2 = params.getConcurrentInterfaceTestCallbackTotalCounter().incrementAndGet();
        // 用例维度的计数
        i3 = params.getConcurrentTestUseCaseCallbackTotalCounter().incrementAndGet();
        // 记录各维度的响应次数 end
    }

    /**
     * 处理callback的数据
     *
     * @param data
     * @return
     */
    public boolean handle(InterfaceTestRecord record, Object data) {
        if (data instanceof ResponseEntity) {
            // 成功
            String resultJson = JSON.toJSONString(((ResponseEntity<Object>) data).getBody());
            List<AssertCondition> assertList = params.getAssertService().getAssert(params.getUcId());
            for (AssertCondition assertCondition : assertList) {
                AssertResult assertResult = AssertService.toAssert(resultJson, assertCondition.getConditions());
                if (!assertResult.isSuccess()) {
                    // 保存断言判断的错误信息
                    record.setAssertValue(assertResult.toString());
                    return false;
                }
            }
            record.setOutValue(resultJson);
        } else {
            // 异常的错误信息
            record.setOutValue(((Throwable) data).getMessage());
            return false;
        }
        return true;
    }

    public void callback(Object data) {
        Date callbackDate = new Date();
        incrementCallback();
        try {
            InterfaceTestRecord interfaceTestRecord = new InterfaceTestRecord();
            boolean flag = handle(interfaceTestRecord, data);
            if (!flag) {
                String interTestId = UUID.randomUUID().toString();
                interfaceTestRecord.setId(interTestId);
                interfaceTestRecord.setUcId(params.getUcId());
                interfaceTestRecord.setStartTime(startTime);
                interfaceTestRecord.setEndTime(new Date());
                // HTTP请求异常
                interfaceTestRecord.setStatus(InterfaceTestRecord.Status.Failure);
                // 错误信息和返回数据都在该字段
                // 保存接口调用结果
                params.getInterfaceTestService().insertSelective(interfaceTestRecord);
                // 保存接口记录、用例用例、测试结果记录的关联关系
                IntegrationTestInterfaceUseCaseRecordAsso recordAsso = new IntegrationTestInterfaceUseCaseRecordAsso();
                recordAsso.setItirId(params.getItirId());
                recordAsso.setItrId(interTestId); // 注意，该字段是interface test record主键
                recordAsso.setTucId(params.getUcId());
                params.getIntegrationTestInterfaceUseCaseRecordAssoService().insertSelective(recordAsso);
            } else {
                incrementSuccess();
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("异步请求处理响应时，发生异常：" + e.getMessage());
        } finally {
            // 当前用例的所有并发请求发送完成 更新测试用例成功总数
            updateRecordStatus(callbackDate);
        }
    }

    private synchronized void incrementSuccess() {
        // 递增成功数
        // 集成测试
        int i1 = params.getConcurrentIntegrationTestSuccessTotalCounter().incrementAndGet();
        // 接口
        int i2 = params.getConcurrentInterfaceTestSuccessTotalCounter().incrementAndGet();
        // 测试用例
        int i3 = params.getConcurrentTestUseCaseSuccessTotalCounter().incrementAndGet();

        //System.out.println("Success:" + i1 + " " + i2 + " " + i3);
    }

    /**
     * 修改记录状态
     */
    private synchronized void updateRecordStatus(Date callbackDate) {
        // 集成测试
        int integrationSuccessCount = params.getConcurrentIntegrationTestSuccessTotalCounter().get();
        int integrationCallbackCount = params.getConcurrentIntegrationTestCallbackTotalCounter().get();
        // 接口测试
        int interfaceCallbackCount = params.getConcurrentInterfaceTestCallbackTotalCounter().get();
        int interfaceSuccessCount = params.getConcurrentInterfaceTestSuccessTotalCounter().get();
        // 测试用例
        int testUseCaseCallbackCount = params.getConcurrentTestUseCaseCallbackTotalCounter().get();
        int testUseCaseSuccessCount = params.getConcurrentTestUseCaseSuccessTotalCounter().get();

        if (testUseCaseCallbackCount == params.getConcurrentCount()) {
            IntegrationTestRecord record = new IntegrationTestRecord();
            record.setId(params.getItrId());
            record.setSuccessTotal(integrationSuccessCount);
            record.setStatus(isSuccess(integrationCallbackCount, integrationSuccessCount));
            record.setEndTime(callbackDate);
            params.getIntegrationTestRecordService().updateByPrimaryKeySelective(record);

            IntegrationTestInterfaceRecord interfaceRecord = new IntegrationTestInterfaceRecord();
            interfaceRecord.setId(params.getItirId());
            interfaceRecord.setSuccessTotal(interfaceSuccessCount);
            interfaceRecord.setStatus(isSuccess(interfaceCallbackCount, interfaceSuccessCount));
            interfaceRecord.setEndTime(callbackDate);
            params.getInterfaceRecordService().updateByPrimaryKeySelective(interfaceRecord);

            TestUseCaseRecord testUseCaseRecord = new TestUseCaseRecord();
            testUseCaseRecord.setId(testUseCaseRecordId);
            testUseCaseRecord.setSuccessTotal(testUseCaseSuccessCount);
            testUseCaseRecord.setStatus(isSuccess(testUseCaseCallbackCount, testUseCaseSuccessCount));
            testUseCaseRecord.setEndTime(callbackDate);
            params.getTestUseCaseRecordService().updateByPrimaryKeySelective(testUseCaseRecord);
        }
    }

    /**
     * 是否成功
     *
     * @param total
     * @param success
     * @return
     */
    public String isSuccess(int total, int success) {
        if (total != 0 && total == success) {
            return IntegrationTestRecord.Status.Success;
        }
        return IntegrationTestRecord.Status.Failure;
    }

    /**
     * Http异步请求
     */
    @Override
    public void asyncCall() throws RuntimeException {
        TestUseCaseRecord testUseCaseRecord = new TestUseCaseRecord();
        try {
            startTime = new Date();
            testUseCaseRecord.setId(testUseCaseRecordId);
            testUseCaseRecord.setItirId(params.getItirId());
            testUseCaseRecord.setStartTime(startTime);
            testUseCaseRecord.setTestUseCaseId(testUseCase.getId());
            testUseCaseRecord.setTestUseCaseName(testUseCase.getUseCaseName());
            testUseCaseRecord.setTotal(params.getConcurrentCount());
            testUseCaseRecord.setSuccessTotal(0);
            for (int i = 0; i < params.getConcurrentCount(); i++) {
                RestTemplateUtil.getAsyncResponseEntity(HttpMethod.resolve(testUseCase.getRequestMethod().toUpperCase()), testUseCase.getRequestUrl(), testUseCase.getInQuery(), testUseCase.getInBody(), testUseCase.getInHeader(), this);
            }
            // 保存测试用例测试记录
            testUseCaseRecord.setStatus(IntegrationTestRecord.Status.Working);
        } catch (Exception e) {
            e.printStackTrace();
            testUseCaseRecord.setStatus(IntegrationTestRecord.Status.Failure);
            throw new RuntimeException(e);
        } finally {
            params.getTestUseCaseRecordService().insertSelective(testUseCaseRecord);
        }
    }

    /**
     * Http同步请求
     *
     * @return
     */
    @Override
    public String syncCall() throws RuntimeException {
        String json;
        try {
            json = RestTemplateUtil.getResponseEntity(HttpMethod.resolve(testUseCase.getRequestMethod().toUpperCase()), testUseCase.getRequestUrl(), testUseCase.getInQuery(), testUseCase.getInBody(), testUseCase.getInHeader());
        } catch (JSONException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return json;
    }
}
