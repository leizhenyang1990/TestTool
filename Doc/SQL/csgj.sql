DROP TABLE IF EXISTS job_entity;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE job_entity (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL,
  `group` varchar(255) DEFAULT NULL,
  cron varchar(255) DEFAULT NULL,
  parameter varchar(255) NOT NULL,
  description varchar(255) DEFAULT NULL,
  vm_param varchar(255) DEFAULT NULL,
  jar_path varchar(255) DEFAULT NULL,
  status varchar(255) DEFAULT NULL,
  PRIMARY KEY (id) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table qrtz_blob_triggers
--

DROP TABLE IF EXISTS qrtz_blob_triggers;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE qrtz_blob_triggers (
  SCHED_NAME varchar(120) NOT NULL,
  TRIGGER_NAME varchar(200) NOT NULL,
  TRIGGER_GROUP varchar(200) NOT NULL,
  BLOB_DATA blob,
  PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) USING BTREE,
  KEY SCHED_NAME (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table qrtz_calendars
--

DROP TABLE IF EXISTS qrtz_calendars;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE qrtz_calendars (
  SCHED_NAME varchar(120) NOT NULL,
  CALENDAR_NAME varchar(200) NOT NULL,
  CALENDAR blob NOT NULL,
  PRIMARY KEY (SCHED_NAME,CALENDAR_NAME) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table qrtz_cron_triggers
--

DROP TABLE IF EXISTS qrtz_cron_triggers;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE qrtz_cron_triggers (
  SCHED_NAME varchar(120) NOT NULL,
  TRIGGER_NAME varchar(200) NOT NULL,
  TRIGGER_GROUP varchar(200) NOT NULL,
  CRON_EXPRESSION varchar(120) NOT NULL,
  TIME_ZONE_ID varchar(80) DEFAULT NULL,
  PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table qrtz_fired_triggers
--

DROP TABLE IF EXISTS qrtz_fired_triggers;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE qrtz_fired_triggers (
  SCHED_NAME varchar(120) NOT NULL,
  ENTRY_ID varchar(95) NOT NULL,
  TRIGGER_NAME varchar(200) NOT NULL,
  TRIGGER_GROUP varchar(200) NOT NULL,
  INSTANCE_NAME varchar(200) NOT NULL,
  FIRED_TIME bigint(13) NOT NULL,
  SCHED_TIME bigint(13) NOT NULL,
  PRIORITY int(11) NOT NULL,
  STATE varchar(16) NOT NULL,
  JOB_NAME varchar(200) DEFAULT NULL,
  JOB_GROUP varchar(200) DEFAULT NULL,
  IS_NONCONCURRENT varchar(1) DEFAULT NULL,
  REQUESTS_RECOVERY varchar(1) DEFAULT NULL,
  PRIMARY KEY (SCHED_NAME,ENTRY_ID) USING BTREE,
  KEY IDX_QRTZ_FT_TRIG_INST_NAME (SCHED_NAME,INSTANCE_NAME) USING BTREE,
  KEY IDX_QRTZ_FT_INST_JOB_REQ_RCVRY (SCHED_NAME,INSTANCE_NAME,REQUESTS_RECOVERY) USING BTREE,
  KEY IDX_QRTZ_FT_J_G (SCHED_NAME,JOB_NAME,JOB_GROUP) USING BTREE,
  KEY IDX_QRTZ_FT_JG (SCHED_NAME,JOB_GROUP) USING BTREE,
  KEY IDX_QRTZ_FT_T_G (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) USING BTREE,
  KEY IDX_QRTZ_FT_TG (SCHED_NAME,TRIGGER_GROUP) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table qrtz_job_details
--

DROP TABLE IF EXISTS qrtz_job_details;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE qrtz_job_details (
  SCHED_NAME varchar(120) NOT NULL,
  JOB_NAME varchar(200) NOT NULL,
  JOB_GROUP varchar(200) NOT NULL,
  DESCRIPTION varchar(250) DEFAULT NULL,
  JOB_CLASS_NAME varchar(250) NOT NULL,
  IS_DURABLE varchar(1) NOT NULL,
  IS_NONCONCURRENT varchar(1) NOT NULL,
  IS_UPDATE_DATA varchar(1) NOT NULL,
  REQUESTS_RECOVERY varchar(1) NOT NULL,
  JOB_DATA blob,
  PRIMARY KEY (SCHED_NAME,JOB_NAME,JOB_GROUP) USING BTREE,
  KEY IDX_QRTZ_J_REQ_RECOVERY (SCHED_NAME,REQUESTS_RECOVERY) USING BTREE,
  KEY IDX_QRTZ_J_GRP (SCHED_NAME,JOB_GROUP) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table qrtz_locks
--

DROP TABLE IF EXISTS qrtz_locks;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE qrtz_locks (
  SCHED_NAME varchar(120) NOT NULL,
  LOCK_NAME varchar(40) NOT NULL,
  PRIMARY KEY (SCHED_NAME,LOCK_NAME) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table qrtz_paused_trigger_grps
--

DROP TABLE IF EXISTS qrtz_paused_trigger_grps;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE qrtz_paused_trigger_grps (
  SCHED_NAME varchar(120) NOT NULL,
  TRIGGER_GROUP varchar(200) NOT NULL,
  PRIMARY KEY (SCHED_NAME,TRIGGER_GROUP) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table qrtz_scheduler_state
--

DROP TABLE IF EXISTS qrtz_scheduler_state;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE qrtz_scheduler_state (
  SCHED_NAME varchar(120) NOT NULL,
  INSTANCE_NAME varchar(200) NOT NULL,
  LAST_CHECKIN_TIME bigint(13) NOT NULL,
  CHECKIN_INTERVAL bigint(13) NOT NULL,
  PRIMARY KEY (SCHED_NAME,INSTANCE_NAME) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table qrtz_simple_triggers
--

DROP TABLE IF EXISTS qrtz_simple_triggers;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE qrtz_simple_triggers (
  SCHED_NAME varchar(120) NOT NULL,
  TRIGGER_NAME varchar(200) NOT NULL,
  TRIGGER_GROUP varchar(200) NOT NULL,
  REPEAT_COUNT bigint(7) NOT NULL,
  REPEAT_INTERVAL bigint(12) NOT NULL,
  TIMES_TRIGGERED bigint(10) NOT NULL,
  PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table qrtz_simprop_triggers
--

DROP TABLE IF EXISTS qrtz_simprop_triggers;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE qrtz_simprop_triggers (
  SCHED_NAME varchar(120) NOT NULL,
  TRIGGER_NAME varchar(200) NOT NULL,
  TRIGGER_GROUP varchar(200) NOT NULL,
  STR_PROP_1 varchar(512) DEFAULT NULL,
  STR_PROP_2 varchar(512) DEFAULT NULL,
  STR_PROP_3 varchar(512) DEFAULT NULL,
  INT_PROP_1 int(11) DEFAULT NULL,
  INT_PROP_2 int(11) DEFAULT NULL,
  LONG_PROP_1 bigint(20) DEFAULT NULL,
  LONG_PROP_2 bigint(20) DEFAULT NULL,
  DEC_PROP_1 decimal(13,4) DEFAULT NULL,
  DEC_PROP_2 decimal(13,4) DEFAULT NULL,
  BOOL_PROP_1 varchar(1) DEFAULT NULL,
  BOOL_PROP_2 varchar(1) DEFAULT NULL,
  PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table qrtz_triggers
--

DROP TABLE IF EXISTS qrtz_triggers;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE qrtz_triggers (
  SCHED_NAME varchar(120) NOT NULL,
  TRIGGER_NAME varchar(200) NOT NULL,
  TRIGGER_GROUP varchar(200) NOT NULL,
  JOB_NAME varchar(200) NOT NULL,
  JOB_GROUP varchar(200) NOT NULL,
  DESCRIPTION varchar(250) DEFAULT NULL,
  NEXT_FIRE_TIME bigint(13) DEFAULT NULL,
  PREV_FIRE_TIME bigint(13) DEFAULT NULL,
  PRIORITY int(11) DEFAULT NULL,
  TRIGGER_STATE varchar(16) NOT NULL,
  TRIGGER_TYPE varchar(8) NOT NULL,
  START_TIME bigint(13) NOT NULL,
  END_TIME bigint(13) DEFAULT NULL,
  CALENDAR_NAME varchar(200) DEFAULT NULL,
  MISFIRE_INSTR smallint(2) DEFAULT NULL,
  JOB_DATA blob,
  PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP) USING BTREE,
  KEY IDX_QRTZ_T_J (SCHED_NAME,JOB_NAME,JOB_GROUP) USING BTREE,
  KEY IDX_QRTZ_T_JG (SCHED_NAME,JOB_GROUP) USING BTREE,
  KEY IDX_QRTZ_T_C (SCHED_NAME,CALENDAR_NAME) USING BTREE,
  KEY IDX_QRTZ_T_G (SCHED_NAME,TRIGGER_GROUP) USING BTREE,
  KEY IDX_QRTZ_T_STATE (SCHED_NAME,TRIGGER_STATE) USING BTREE,
  KEY IDX_QRTZ_T_N_STATE (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP,TRIGGER_STATE) USING BTREE,
  KEY IDX_QRTZ_T_N_G_STATE (SCHED_NAME,TRIGGER_GROUP,TRIGGER_STATE) USING BTREE,
  KEY IDX_QRTZ_T_NEXT_FIRE_TIME (SCHED_NAME,NEXT_FIRE_TIME) USING BTREE,
  KEY IDX_QRTZ_T_NFT_ST (SCHED_NAME,TRIGGER_STATE,NEXT_FIRE_TIME) USING BTREE,
  KEY IDX_QRTZ_T_NFT_MISFIRE (SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME) USING BTREE,
  KEY IDX_QRTZ_T_NFT_ST_MISFIRE (SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME,TRIGGER_STATE) USING BTREE,
  KEY IDX_QRTZ_T_NFT_ST_MISFIRE_GRP (SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME,TRIGGER_GROUP,TRIGGER_STATE) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS tb_ssgp_assert_condition;

CREATE TABLE tb_ssgp_assert_condition (
  assert_id varchar(36) NOT NULL COMMENT '断言ID',
  fk_id varchar(36) DEFAULT NULL COMMENT '测试用例主键',
  conditions text COMMENT '断言条件',
  PRIMARY KEY (assert_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='断言条件表';


DROP TABLE IF EXISTS tb_ssgp_csgj_integration_test;

CREATE TABLE tb_ssgp_csgj_integration_test (
  id varchar(36) NOT NULL,
  name varchar(255) DEFAULT NULL,
  remark varchar(255) DEFAULT NULL,
  create_time datetime(3) DEFAULT NULL,
  update_time datetime(3) DEFAULT NULL,
  class_name varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '类名',
  method_name varchar(30) DEFAULT NULL COMMENT '方法名',
  params varchar(255) DEFAULT NULL COMMENT '参数',
  status int(5) DEFAULT NULL COMMENT '启用状态',
  is_concurrent tinyint(1) DEFAULT NULL COMMENT '是否并发',
  start_time datetime(3) DEFAULT NULL COMMENT '开始时间',
  end_time datetime(3) DEFAULT NULL COMMENT '结束时间',
  execute_plan_data text COMMENT '执行计划数据',
  is_used tinyint(4) DEFAULT '1',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='集成测试表';

DROP TABLE IF EXISTS tb_ssgp_csgj_integration_test_interface;

CREATE TABLE tb_ssgp_csgj_integration_test_interface (
  id varchar(36) CHARACTER SET utf8 NOT NULL,
  it_id varchar(36) CHARACTER SET utf8 DEFAULT NULL COMMENT '集成测试外键',
  interface_id varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  interface_name varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS tb_ssgp_csgj_integration_test_interface_record;

CREATE TABLE tb_ssgp_csgj_integration_test_interface_record (
  id varchar(36) CHARACTER SET utf8 NOT NULL,
  itr_id varchar(36) CHARACTER SET utf8 DEFAULT NULL,
  interface_id varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  interface_name varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  status varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '状态0 未执行 1 执行成功 2执行失败',
  total int(255) DEFAULT NULL,
  success_total int(255) DEFAULT NULL,
  start_time datetime(3) DEFAULT NULL,
  end_time datetime(3) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS tb_ssgp_csgj_integration_test_interface_use_case_asso;

CREATE TABLE tb_ssgp_csgj_integration_test_interface_use_case_asso (
  iti_id varchar(36) CHARACTER SET utf8 DEFAULT NULL,
  tuc_id varchar(36) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS tb_ssgp_csgj_integration_test_interface_use_case_record_asso;

CREATE TABLE tb_ssgp_csgj_integration_test_interface_use_case_record_asso (
  itir_id varchar(36) CHARACTER SET utf8 DEFAULT NULL,
  tuc_id varchar(36) CHARACTER SET utf8 DEFAULT NULL,
  itr_id varchar(36) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS tb_ssgp_csgj_integration_test_record;

CREATE TABLE tb_ssgp_csgj_integration_test_record (
  id varchar(36) CHARACTER SET utf8 NOT NULL,
  it_id varchar(36) CHARACTER SET utf8 DEFAULT NULL COMMENT '集成测试外键',
  status varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '状态0 未执行 1 执行成功 2执行失败',
  start_time datetime(3) DEFAULT NULL COMMENT '开始时间',
  end_time datetime(3) DEFAULT NULL COMMENT '结束时间',
  total int(11) DEFAULT NULL COMMENT '接口总数',
  success_total int(11) DEFAULT NULL COMMENT '成功接口总数',
  concurrent_count int(11) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='集成测试记录表';


DROP TABLE IF EXISTS tb_ssgp_csgj_interface_test_record;

CREATE TABLE tb_ssgp_csgj_interface_test_record (
  id varchar(36) CHARACTER SET utf8 NOT NULL,
  uc_id varchar(36) CHARACTER SET utf8 DEFAULT NULL COMMENT '测试用例ID',
  out_value longtext CHARACTER SET utf8 DEFAULT NULL COMMENT '返回值',
  start_time datetime(3) DEFAULT NULL,
  end_time datetime(3) DEFAULT NULL,
  status varchar(20) CHARACTER SET utf8 DEFAULT NULL COMMENT '0失败 1成功',
  assert_value text,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='接口测试记录';


DROP TABLE IF EXISTS tb_ssgp_csgj_test_use_case;

CREATE TABLE tb_ssgp_csgj_test_use_case (
  id varchar(36) CHARACTER SET utf8 NOT NULL,
  interface_id varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '接口ID',
  interface_name varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '接口名称',
  request_method varchar(255) DEFAULT NULL,
  request_url varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  use_case_name varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '用例测试名称',
  remark varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '说明',
  create_time datetime(3) DEFAULT NULL COMMENT '创建时间',
  update_time datetime(3) DEFAULT NULL COMMENT '修改时间',
  in_header text CHARACTER SET utf8 COMMENT 'Header入参',
  in_body text CHARACTER SET utf8 COMMENT 'Body入参',
  in_query text CHARACTER SET utf8 COMMENT 'Query入参',
  source varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '来源 0 用例测试 1集成测试（似乎不需要） 2接口测试',
  delete_flag varchar(255) CHARACTER SET utf8 DEFAULT NULL COMMENT '删除标记',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='测试用例表';


DROP TABLE IF EXISTS tb_ssgp_csgj_test_use_case_record;

CREATE TABLE tb_ssgp_csgj_test_use_case_record (
  id varchar(36) NOT NULL,
  itir_id varchar(36) DEFAULT NULL COMMENT '集成测试接口历史主键',
  status varchar(255) DEFAULT NULL COMMENT '状态0 未执行 1 执行成功 2执行失败',
  start_time datetime(3) DEFAULT NULL COMMENT '开始时间',
  end_time datetime(3) DEFAULT NULL COMMENT '结束时间',
  total int(11) DEFAULT NULL COMMENT '总数',
  success_total int(11) DEFAULT NULL COMMENT '成功接口总数',
  test_use_case_id varchar(36) DEFAULT NULL,
  test_use_case_name varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='集成测试记录表';

DROP TABLE IF EXISTS tb_ssgp_execution_plan;

CREATE TABLE tb_ssgp_execution_plan (
  id varchar(36) NOT NULL COMMENT '执行计划主键ID',
  trigger_id varchar(36) DEFAULT NULL COMMENT '调度触发器id',
  execute_count int(11) DEFAULT NULL COMMENT '执行次数',
  concurrent_count int(11) DEFAULT NULL COMMENT '每次并发次数',
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS tb_ssgp_invoke_trigger;

CREATE TABLE tb_ssgp_invoke_trigger (
  trigger_id varchar(36) NOT NULL COMMENT '调度触发器id',
  foreign_key_id varchar(36) DEFAULT NULL COMMENT '调度id',
  start_datetime datetime(3) DEFAULT NULL COMMENT '调度开始日期时间',
  end_datetime datetime(3) DEFAULT NULL COMMENT '调度结束日期时间',
  count int(11) DEFAULT NULL COMMENT '调度执行次数',
  trigger_interval int(11) DEFAULT NULL COMMENT '调度执行间隔（秒）',
  cron_expression varchar(255) DEFAULT NULL COMMENT 'cron表达式',
  trigger_name varchar(255) DEFAULT NULL COMMENT 'trigger唯一标识',
  start_time time(3) DEFAULT NULL COMMENT '调度开始时间',
  end_time time(3) DEFAULT NULL COMMENT '调度结束时间',
  params varchar(300) DEFAULT NULL,
  PRIMARY KEY (trigger_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='调度计划表';

