drop database if exists csgj;
create database csgj;
use csgj;

CREATE TABLE job_entity
(
  id          INT AUTO_INCREMENT
    PRIMARY KEY,
  name        VARCHAR(255) NULL,
  `group`     VARCHAR(255) NULL,
  cron        VARCHAR(255) NULL,
  parameter   VARCHAR(255) NOT NULL,
  description VARCHAR(255) NULL,
  vm_param    VARCHAR(255) NULL,
  jar_path    VARCHAR(255) NULL,
  status      VARCHAR(255) NULL
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE qrtz_blob_triggers
(
  SCHED_NAME    VARCHAR(120) NOT NULL,
  TRIGGER_NAME  VARCHAR(200) NOT NULL,
  TRIGGER_GROUP VARCHAR(200) NOT NULL,
  BLOB_DATA     BLOB         NULL,
  PRIMARY KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE INDEX SCHED_NAME
  ON qrtz_blob_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);

CREATE TABLE qrtz_calendars
(
  SCHED_NAME    VARCHAR(120) NOT NULL,
  CALENDAR_NAME VARCHAR(200) NOT NULL,
  CALENDAR      BLOB         NOT NULL,
  PRIMARY KEY (SCHED_NAME, CALENDAR_NAME)
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE qrtz_cron_triggers
(
  SCHED_NAME      VARCHAR(120) NOT NULL,
  TRIGGER_NAME    VARCHAR(200) NOT NULL,
  TRIGGER_GROUP   VARCHAR(200) NOT NULL,
  CRON_EXPRESSION VARCHAR(120) NOT NULL,
  TIME_ZONE_ID    VARCHAR(80)  NULL,
  PRIMARY KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE qrtz_fired_triggers
(
  SCHED_NAME        VARCHAR(120) NOT NULL,
  ENTRY_ID          VARCHAR(95)  NOT NULL,
  TRIGGER_NAME      VARCHAR(200) NOT NULL,
  TRIGGER_GROUP     VARCHAR(200) NOT NULL,
  INSTANCE_NAME     VARCHAR(200) NOT NULL,
  FIRED_TIME        BIGINT(13)   NOT NULL,
  SCHED_TIME        BIGINT(13)   NOT NULL,
  PRIORITY          INT          NOT NULL,
  STATE             VARCHAR(16)  NOT NULL,
  JOB_NAME          VARCHAR(200) NULL,
  JOB_GROUP         VARCHAR(200) NULL,
  IS_NONCONCURRENT  VARCHAR(1)   NULL,
  REQUESTS_RECOVERY VARCHAR(1)   NULL,
  PRIMARY KEY (SCHED_NAME, ENTRY_ID)
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE INDEX IDX_QRTZ_FT_T_G
  ON qrtz_fired_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);

CREATE INDEX IDX_QRTZ_FT_TG
  ON qrtz_fired_triggers (SCHED_NAME, TRIGGER_GROUP);

CREATE INDEX IDX_QRTZ_FT_INST_JOB_REQ_RCVRY
  ON qrtz_fired_triggers (SCHED_NAME, INSTANCE_NAME, REQUESTS_RECOVERY);

CREATE INDEX IDX_QRTZ_FT_TRIG_INST_NAME
  ON qrtz_fired_triggers (SCHED_NAME, INSTANCE_NAME);

CREATE INDEX IDX_QRTZ_FT_J_G
  ON qrtz_fired_triggers (SCHED_NAME, JOB_NAME, JOB_GROUP);

CREATE INDEX IDX_QRTZ_FT_JG
  ON qrtz_fired_triggers (SCHED_NAME, JOB_GROUP);

CREATE TABLE qrtz_job_details
(
  SCHED_NAME        VARCHAR(120) NOT NULL,
  JOB_NAME          VARCHAR(200) NOT NULL,
  JOB_GROUP         VARCHAR(200) NOT NULL,
  DESCRIPTION       VARCHAR(250) NULL,
  JOB_CLASS_NAME    VARCHAR(250) NOT NULL,
  IS_DURABLE        VARCHAR(1)   NOT NULL,
  IS_NONCONCURRENT  VARCHAR(1)   NOT NULL,
  IS_UPDATE_DATA    VARCHAR(1)   NOT NULL,
  REQUESTS_RECOVERY VARCHAR(1)   NOT NULL,
  JOB_DATA          BLOB         NULL,
  PRIMARY KEY (SCHED_NAME, JOB_NAME, JOB_GROUP)
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE INDEX IDX_QRTZ_J_GRP
  ON qrtz_job_details (SCHED_NAME, JOB_GROUP);

CREATE INDEX IDX_QRTZ_J_REQ_RECOVERY
  ON qrtz_job_details (SCHED_NAME, REQUESTS_RECOVERY);

CREATE TABLE qrtz_locks
(
  SCHED_NAME VARCHAR(120) NOT NULL,
  LOCK_NAME  VARCHAR(40)  NOT NULL,
  PRIMARY KEY (SCHED_NAME, LOCK_NAME)
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE qrtz_paused_trigger_grps
(
  SCHED_NAME    VARCHAR(120) NOT NULL,
  TRIGGER_GROUP VARCHAR(200) NOT NULL,
  PRIMARY KEY (SCHED_NAME, TRIGGER_GROUP)
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE qrtz_scheduler_state
(
  SCHED_NAME        VARCHAR(120) NOT NULL,
  INSTANCE_NAME     VARCHAR(200) NOT NULL,
  LAST_CHECKIN_TIME BIGINT(13)   NOT NULL,
  CHECKIN_INTERVAL  BIGINT(13)   NOT NULL,
  PRIMARY KEY (SCHED_NAME, INSTANCE_NAME)
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE qrtz_simple_triggers
(
  SCHED_NAME      VARCHAR(120) NOT NULL,
  TRIGGER_NAME    VARCHAR(200) NOT NULL,
  TRIGGER_GROUP   VARCHAR(200) NOT NULL,
  REPEAT_COUNT    BIGINT(7)    NOT NULL,
  REPEAT_INTERVAL BIGINT(12)   NOT NULL,
  TIMES_TRIGGERED BIGINT(10)   NOT NULL,
  PRIMARY KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE qrtz_simprop_triggers
(
  SCHED_NAME    VARCHAR(120)   NOT NULL,
  TRIGGER_NAME  VARCHAR(200)   NOT NULL,
  TRIGGER_GROUP VARCHAR(200)   NOT NULL,
  STR_PROP_1    VARCHAR(512)   NULL,
  STR_PROP_2    VARCHAR(512)   NULL,
  STR_PROP_3    VARCHAR(512)   NULL,
  INT_PROP_1    INT            NULL,
  INT_PROP_2    INT            NULL,
  LONG_PROP_1   BIGINT         NULL,
  LONG_PROP_2   BIGINT         NULL,
  DEC_PROP_1    DECIMAL(13, 4) NULL,
  DEC_PROP_2    DECIMAL(13, 4) NULL,
  BOOL_PROP_1   VARCHAR(1)     NULL,
  BOOL_PROP_2   VARCHAR(1)     NULL,
  PRIMARY KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE qrtz_triggers
(
  SCHED_NAME     VARCHAR(120) NOT NULL,
  TRIGGER_NAME   VARCHAR(200) NOT NULL,
  TRIGGER_GROUP  VARCHAR(200) NOT NULL,
  JOB_NAME       VARCHAR(200) NOT NULL,
  JOB_GROUP      VARCHAR(200) NOT NULL,
  DESCRIPTION    VARCHAR(250) NULL,
  NEXT_FIRE_TIME BIGINT(13)   NULL,
  PREV_FIRE_TIME BIGINT(13)   NULL,
  PRIORITY       INT          NULL,
  TRIGGER_STATE  VARCHAR(16)  NOT NULL,
  TRIGGER_TYPE   VARCHAR(8)   NOT NULL,
  START_TIME     BIGINT(13)   NOT NULL,
  END_TIME       BIGINT(13)   NULL,
  CALENDAR_NAME  VARCHAR(200) NULL,
  MISFIRE_INSTR  SMALLINT(2)  NULL,
  JOB_DATA       BLOB         NULL,
  PRIMARY KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP),
  CONSTRAINT qrtz_triggers_ibfk_1
  FOREIGN KEY (SCHED_NAME, JOB_NAME, JOB_GROUP) REFERENCES qrtz_job_details (SCHED_NAME, JOB_NAME, JOB_GROUP)
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE INDEX IDX_QRTZ_T_N_STATE
  ON qrtz_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, TRIGGER_STATE);

CREATE INDEX IDX_QRTZ_T_N_G_STATE
  ON qrtz_triggers (SCHED_NAME, TRIGGER_GROUP, TRIGGER_STATE);

CREATE INDEX IDX_QRTZ_T_G
  ON qrtz_triggers (SCHED_NAME, TRIGGER_GROUP);

CREATE INDEX IDX_QRTZ_T_J
  ON qrtz_triggers (SCHED_NAME, JOB_NAME, JOB_GROUP);

CREATE INDEX IDX_QRTZ_T_JG
  ON qrtz_triggers (SCHED_NAME, JOB_GROUP);

CREATE INDEX IDX_QRTZ_T_NEXT_FIRE_TIME
  ON qrtz_triggers (SCHED_NAME, NEXT_FIRE_TIME);

CREATE INDEX IDX_QRTZ_T_NFT_ST
  ON qrtz_triggers (SCHED_NAME, TRIGGER_STATE, NEXT_FIRE_TIME);

CREATE INDEX IDX_QRTZ_T_STATE
  ON qrtz_triggers (SCHED_NAME, TRIGGER_STATE);

CREATE INDEX IDX_QRTZ_T_C
  ON qrtz_triggers (SCHED_NAME, CALENDAR_NAME);

CREATE INDEX IDX_QRTZ_T_NFT_ST_MISFIRE_GRP
  ON qrtz_triggers (SCHED_NAME, MISFIRE_INSTR, NEXT_FIRE_TIME, TRIGGER_GROUP, TRIGGER_STATE);

CREATE INDEX IDX_QRTZ_T_NFT_ST_MISFIRE
  ON qrtz_triggers (SCHED_NAME, MISFIRE_INSTR, NEXT_FIRE_TIME, TRIGGER_STATE);

CREATE INDEX IDX_QRTZ_T_NFT_MISFIRE
  ON qrtz_triggers (SCHED_NAME, MISFIRE_INSTR, NEXT_FIRE_TIME);

ALTER TABLE qrtz_blob_triggers
  ADD CONSTRAINT qrtz_blob_triggers_ibfk_1
FOREIGN KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP) REFERENCES qrtz_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);

ALTER TABLE qrtz_cron_triggers
  ADD CONSTRAINT qrtz_cron_triggers_ibfk_1
FOREIGN KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP) REFERENCES qrtz_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);

ALTER TABLE qrtz_simple_triggers
  ADD CONSTRAINT qrtz_simple_triggers_ibfk_1
FOREIGN KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP) REFERENCES qrtz_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);

ALTER TABLE qrtz_simprop_triggers
  ADD CONSTRAINT qrtz_simprop_triggers_ibfk_1
FOREIGN KEY (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP) REFERENCES qrtz_triggers (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);

CREATE TABLE tb_ssgp_assert_condition
(
  assert_id  VARCHAR(36) NOT NULL
  COMMENT '断言ID'
    PRIMARY KEY,
  fk_id      VARCHAR(36) NULL
  COMMENT '测试用例主键',
  conditions TEXT        NULL
  COMMENT '断言条件'
)
  COMMENT '断言条件表'
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE tb_ssgp_csgj_integration_test
(
  id                VARCHAR(36)  NOT NULL
    PRIMARY KEY,
  name              VARCHAR(255) NULL,
  remark            VARCHAR(255) NULL,
  create_time       DATETIME     NULL,
  update_time       DATETIME     NULL,
  class_name        VARCHAR(255) NULL
  COMMENT '类名',
  method_name       VARCHAR(30)  NULL
  COMMENT '方法名',
  params            VARCHAR(255) NULL
  COMMENT '参数',
  status            TINYINT(1)   NULL
  COMMENT '启用状态',
  is_concurrent     TINYINT(1)   NULL
  COMMENT '是否并发',
  start_time        DATETIME     NULL
  COMMENT '开始时间',
  end_time          DATETIME     NULL
  COMMENT '结束时间',
  execute_plan_data TEXT         NULL
  COMMENT '执行计划数据'
)
  COMMENT '集成测试表'
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE tb_ssgp_csgj_integration_test_interface
(
  id             VARCHAR(36)  NOT NULL
  PRIMARY KEY,
  it_id          VARCHAR(36)  NULL  COMMENT '集成测试外键',
  interface_id   VARCHAR(255) NULL ,
  interface_name VARCHAR(255) NULL
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE tb_ssgp_csgj_integration_test_interface_record
(
  id             VARCHAR(36)  NOT NULL
  PRIMARY KEY,
  itr_id         VARCHAR(36)  NULL ,
  interface_id   VARCHAR(255) NULL ,
  interface_name VARCHAR(255) NULL ,
  status         VARCHAR(255) NULL  COMMENT '状态0 未执行 1 执行成功 2执行失败',
  total          INT(255)     NULL,
  success_total  INT(255)     NULL,
  start_time     DATETIME     NULL,
  end_time       DATETIME     NULL
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE tb_ssgp_csgj_integration_test_interface_use_case_asso
(
  iti_id VARCHAR(36) NULL ,
  tuc_id VARCHAR(36) NULL
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE tb_ssgp_csgj_integration_test_interface_use_case_record_asso
(
  itir_id VARCHAR(36) NULL ,
  tuc_id  VARCHAR(36) NULL ,
  itr_id  VARCHAR(36) NULL
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE tb_ssgp_csgj_integration_test_record
(
  id               VARCHAR(36)  NOT NULL
  PRIMARY KEY,
  it_id            VARCHAR(36)  NULL  COMMENT '集成测试外键',
  status           VARCHAR(255) NULL  COMMENT '状态0 未执行 1 执行成功 2执行失败',
  start_time       DATETIME     NULL
  COMMENT '开始时间',
  end_time         DATETIME     NULL
  COMMENT '结束时间',
  total            INT          NULL
  COMMENT '接口总数',
  success_total    INT          NULL
  COMMENT '成功接口总数',
  concurrent_count INT          NULL
)
  COMMENT '集成测试记录表'
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE tb_ssgp_csgj_interface_test_record
(
  id         VARCHAR(36)  NOT NULL
  PRIMARY KEY,
  uc_id      VARCHAR(36)  NULL  COMMENT '测试用例ID',
  out_value  VARCHAR(255) NULL  COMMENT '返回值',
  start_time DATETIME     NULL,
  end_time   DATETIME     NULL,
  status     VARCHAR(20)  NULL  COMMENT '0失败 1成功'
)
  COMMENT '接口测试记录'
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE tb_ssgp_csgj_invoke_trigger
(
  trigger_id      VARCHAR(36)  NOT NULL
  COMMENT '调度触发器id'
    PRIMARY KEY,
  foreign_key_id  VARCHAR(36)  NULL
  COMMENT '集成测试ID',
  start_datetime  DATETIME     NULL
  COMMENT '开始日期',
  end_datetime    DATETIME     NULL
  COMMENT '结束日期',
  start_time      DATETIME     NULL
  COMMENT '开始时间',
  end_time        DATETIME     NULL
  COMMENT '结束时间',
  count           INT          NULL
  COMMENT '执行次数',
  `interval`      INT          NULL
  COMMENT '执行间隔(秒)',
  cron_expression VARCHAR(255) NULL
  COMMENT 'cron表达式',
  trigger_name    VARCHAR(255) NULL
  COMMENT 'trigger唯一标识'
)
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE tb_ssgp_csgj_test_use_case
(
  id             VARCHAR(36)  NOT NULL
  PRIMARY KEY,
  interface_id   VARCHAR(255) NULL  COMMENT '接口ID',
  interface_name VARCHAR(255) NULL  COMMENT '接口名称',
  request_method VARCHAR(255) NULL,
  request_url    VARCHAR(500) NULL ,
  use_case_name  VARCHAR(255) NULL  COMMENT '用例测试名称',
  remark         VARCHAR(255) NULL  COMMENT '说明',
  create_time    DATETIME     NULL
  COMMENT '创建时间',
  update_time    DATETIME     NULL
  COMMENT '修改时间',
  in_header      TEXT         NULL  COMMENT 'Header入参',
  in_body        TEXT         NULL  COMMENT 'Body入参',
  in_query       TEXT         NULL  COMMENT 'Query入参',
  source         VARCHAR(255) NULL  COMMENT '来源 0 用例测试 1集成测试（似乎不需要） 2接口测试',
  delete_flag    VARCHAR(255) NULL  COMMENT '删除标记'
)
  COMMENT '测试用例表'
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE tb_ssgp_csgj_test_use_case_record
(
  id            VARCHAR(36)  NOT NULL
    PRIMARY KEY,
  itir_id       VARCHAR(36)  NULL
  COMMENT '集成测试接口历史主键',
  status        VARCHAR(255) NULL
  COMMENT '状态0 未执行 1 执行成功 2执行失败',
  start_time    DATETIME     NULL
  COMMENT '开始时间',
  end_time      DATETIME     NULL
  COMMENT '结束时间',
  total         INT          NULL
  COMMENT '总数',
  success_total INT          NULL
  COMMENT '成功接口总数'
)
  COMMENT '集成测试记录表'
  ENGINE = InnoDB
  CHARSET = utf8;

CREATE TABLE tb_ssgp_execution_plan
(
  id                VARCHAR(36) NOT NULL
  COMMENT '执行计划主键ID'
    PRIMARY KEY,
  trigger_id        VARCHAR(36) NULL
  COMMENT '调度触发器id',
  execute_count   INT         NULL
  COMMENT '执行次数',
  concurrent_count INT         NULL
  COMMENT '每次并发次数'
)

  ENGINE = InnoDB
  CHARSET = utf8;


CREATE TABLE tb_ssgp_invoke_trigger
(
  trigger_id       VARCHAR(36)  NOT NULL
  COMMENT '调度触发器id'
    PRIMARY KEY,
  foreign_key_id   VARCHAR(36)  NULL
  COMMENT '调度id',
  start_datetime   DATETIME     NULL
  COMMENT '调度开始日期时间',
  end_datetime     DATETIME     NULL
  COMMENT '调度结束日期时间',
  count            INT          NULL
  COMMENT '调度执行次数',
  trigger_interval INT          NULL
  COMMENT '调度执行间隔（秒）',
  cron_expression  VARCHAR(255) NULL
  COMMENT 'cron表达式',
  trigger_name     VARCHAR(255) NULL
  COMMENT 'trigger唯一标识',
  start_time       TIME         NULL
  COMMENT '调度开始时间',
  end_time         TIME         NULL
  COMMENT '调度结束时间'
)
  COMMENT '调度计划表'
  ENGINE = InnoDB
  CHARSET = utf8;



